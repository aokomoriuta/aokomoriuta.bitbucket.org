Grailbird.data.tweets_2014_08 = 
 [ {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 117, 139 ],
      "url" : "http:\/\/t.co\/dhLM4iFMQE",
      "expanded_url" : "http:\/\/www.ipa.go.jp\/osc\/license1.html",
      "display_url" : "ipa.go.jp\/osc\/license1.h\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "506067010715799552",
  "text" : "CC-BY-*NC-ND*\u306EGPL\u89E3\u8AAC\u66F8\u5192\u982D\u306B\u300C\u672C\u66F8\u304C\u3001\u6211\u304C\u56FD\u306E\u30BD\u30D5\u30C8\u30A6\u30A7\u30A2\u958B\u767A\u306B\u304A\u3051\u308B\u30D5\u30EA\u30FC\uFF0F\u30AA\u30FC\u30D7\u30F3\u30BD\u30FC\u30B9\u30BD\u30D5\u30C8\u30A6\u30A7\u30A2\u306E\u5229\u7528\u63A8\u9032\u306B\u5C11\u3057\u3067\u3082\u5F79\u7ACB\u3064\u3082\u306E\u306B\u306A\u308B\u306E\u3067\u3042\u308C\u3070\u3001\u59D4\u54E1\u4E00\u540C\u3001\u671B\u5916\u306E\u559C\u3073\u3067\u3042\u308B\u300D\u3068\u304B\u3001\uFF08\u82E6\uFF09\u7B11\u3044\u3092\u3053\u3089\u3048\u308B\u306E\u306B\u5FC5\u6B7B http:\/\/t.co\/dhLM4iFMQE",
  "id" : 506067010715799552,
  "created_at" : "2014-08-31 13:12:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 44, 66 ],
      "url" : "http:\/\/t.co\/dhLM4iFMQE",
      "expanded_url" : "http:\/\/www.ipa.go.jp\/osc\/license1.html",
      "display_url" : "ipa.go.jp\/osc\/license1.h\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "506066248350695424",
  "text" : "GPLv3\u306E\u89E3\u8AAC\u304CCC-BY-*NC-ND*\u3068\u304B\u3001\u3068\u3066\u3082\u30B9\u30D1\u30A4\u30B9\u306E\u805E\u3044\u305F\u30B8\u30E7\u30FC\u30AF\u3067\u3059\u306D http:\/\/t.co\/dhLM4iFMQE IPA\u304C\u3053\u3093\u306A\u3093\u3060\u304B\u3089\u65E5\u672C\u306B\u30AA\u30FC\u30D7\u30F3\u30BD\u30FC\u30B9\u306E\u7CBE\u795E\u304C\u6839\u4ED8\u304B\u306A\u3044\u308F\u3051\u3067\u3059",
  "id" : 506066248350695424,
  "created_at" : "2014-08-31 13:09:30 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u725B\u5C71\u7D20\u884C",
      "screen_name" : "disaster_i",
      "indices" : [ 0, 11 ],
      "id_str" : "177125714",
      "id" : 177125714
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "506060745268686848",
  "geo" : { },
  "id_str" : "506064749939806208",
  "in_reply_to_user_id" : 177125714,
  "text" : "@disaster_i \u30CF\u30B6\u30FC\u30C9\u3068\u7247\u4EEE\u540D\u306B\u3057\u3066\u3057\u307E\u3046\u3068\u304A\u5E74\u5BC4\u308A\u306B\u4F1D\u308F\u308A\u306B\u304F\u3044\u306E\u3067\u3001\u4F55\u304B\u9069\u5207\u306A\u8A33\u8A9E\u306F\u306A\u3044\u3067\u3057\u3087\u3046\u304B\u3002",
  "id" : 506064749939806208,
  "in_reply_to_status_id" : 506060745268686848,
  "created_at" : "2014-08-31 13:03:33 +0000",
  "in_reply_to_screen_name" : "disaster_i",
  "in_reply_to_user_id_str" : "177125714",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "506063899435618304",
  "text" : "\u6163\u308C\u305F\u3068\u3044\u3046\u304B\u3001\u305D\u3082\u305D\u3082\u9055\u3046\u8A71\u3068\u601D\u3063\u3066\u898B\u3089\u308C\u3066\u308B\u304B\u3089\u306A\u3002",
  "id" : 506063899435618304,
  "created_at" : "2014-08-31 13:00:10 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "506063738646966272",
  "text" : "\u30B7\u30E3\u30FC\u30ED\u30C3\u30AF2nd\u30B7\u30FC\u30BA\u30F3\u307E\u3067\u898B\u3066\u3001\u305D\u308D\u305D\u308D\u30B7\u30E3\u30FC\u30ED\u30C3\u30AF&lt;-&gt;\u30B8\u30E7\u30F3\u547C\u3073\u306B\u3082\u6163\u308C\u305F\u3002",
  "id" : 506063738646966272,
  "created_at" : "2014-08-31 12:59:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30BB\u30CE\u30DA\u30F3",
      "screen_name" : "senopen",
      "indices" : [ 0, 8 ],
      "id_str" : "240197690",
      "id" : 240197690
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "506057338583666688",
  "geo" : { },
  "id_str" : "506063553371979776",
  "in_reply_to_user_id" : 240197690,
  "text" : "@senopen \u3048\u3048\u3001\u3042\u306E\u65E5\u306F\u3061\u3087\u3063\u3068\u6240\u7528\u304C\u3042\u3063\u3066\u65E9\u3081\u306B\u629C\u3051\u308B\u3064\u3082\u308A\u3060\u3063\u305F\u3093\u3067\u3059\u304C\u3001PyVTK\u3068\u805E\u3044\u3066\u30BB\u30CE\u30DA\u30F3\u3055\u3093\u306E\u767A\u8868\u3060\u3051\u805E\u3044\u3066\u5E30\u308A\u307E\u3057\u305F\uFF57\u3002PythonFilter\u306F\u60C5\u5831\u307B\u3068\u3093\u3069\u306A\u3044\u3067\u3059\u306D\u3047\u30FB\u30FB\u30FB\u3042\u3063\u3066\u3082\u53E4\u3044\u3068\u304B\u3002\u3084\u3063\u3071\u308A\u30BD\u30FC\u30B9\u8AAD\u3093\u3060\u308A\u3057\u3066\u89E3\u8AAD\u3057\u307E\u3057\u305F\u3001\u79C1\u3082\u3002",
  "id" : 506063553371979776,
  "in_reply_to_status_id" : 506057338583666688,
  "created_at" : "2014-08-31 12:58:47 +0000",
  "in_reply_to_screen_name" : "senopen",
  "in_reply_to_user_id_str" : "240197690",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505989362245312512",
  "geo" : { },
  "id_str" : "506056967849115648",
  "in_reply_to_user_id" : 445958218,
  "text" : "@hiochuu \u306F\u3044\uFF01\u3044\u3044\u3068\u601D\u3044\u307E\u3059\uFF01",
  "id" : 506056967849115648,
  "in_reply_to_status_id" : 505989362245312512,
  "created_at" : "2014-08-31 12:32:37 +0000",
  "in_reply_to_screen_name" : "hio87bi",
  "in_reply_to_user_id_str" : "445958218",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30BB\u30CE\u30DA\u30F3",
      "screen_name" : "senopen",
      "indices" : [ 5, 13 ],
      "id_str" : "240197690",
      "id" : 240197690
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "506055909957595136",
  "text" : "\u571F\u66DC\u65E5\u306E @senopen \u3055\u3093\u306E\u767A\u8868\u805E\u3044\u3066\u4E45\u3057\u3076\u308A\u306BParaView\u306EPythonFilter\u4F7F\u3063\u3066\u307F\u305F\u3089\u4F7F\u3048\u306A\u304F\u306A\u3063\u3066\u305F\u306E\u3067\u4F55\u304B\u8A2D\u5B9A\u3057\u5FD8\u308C\u3066\u308B\u3002\u305D\u308C\u306F\u305D\u308C\u3068\u3057\u3066\u3001\u3069\u3053\u304B\u3067\u805E\u3044\u305F\u540D\u524D\u3060\u3068\u601D\u3063\u305F\u3089\u65E2\u306Blist\u306B\u5165\u308C\u3066\u305F\u3002",
  "id" : 506055909957595136,
  "created_at" : "2014-08-31 12:28:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "506052806071951361",
  "text" : "\u7FFB\u8A33\u96E3\u3057\u3044\u306E\u306F\u5206\u304B\u308B\u3051\u3069\"the woman\"\u3092\u300C\u6BD4\u985E\u306A\u304D\u5973\u300D\u3063\u3066\u8A33\u3059\u306E\u306F\u3061\u3087\u3063\u3068\u3069\u3046\u304B\u3068\u601D\u3063\u305F\u3002",
  "id" : 506052806071951361,
  "created_at" : "2014-08-31 12:16:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505990265694203904",
  "geo" : { },
  "id_str" : "505990343506927617",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3042\u30FC",
  "id" : 505990343506927617,
  "in_reply_to_status_id" : 505990265694203904,
  "created_at" : "2014-08-31 08:07:53 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505989771903000577",
  "geo" : { },
  "id_str" : "505989867898028032",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u305D\u3046\u306D\u30FC\u3001\u5927\u5B66\u751F\u307B\u3069\u6642\u9593\u3082\u77E5\u8B58\u3082\u3042\u3063\u3066\u30A6\u30A3\u30AD\u30DA\u30C7\u30A3\u30A2\u30F3\u306B\u6700\u9069\u306A\u4EBA\u305F\u3061\u3044\u306A\u3044\u306E\u306B\u306D",
  "id" : 505989867898028032,
  "in_reply_to_status_id" : 505989771903000577,
  "created_at" : "2014-08-31 08:06:00 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505989775472340992",
  "text" : "\u3061\u306A\u307F\u306B\uFF08\u307E\u3060\uFF09\u5C31\u6D3B\u3057\u305F\u3053\u3068\u306A\u3044\u52E2\u306A\u306E\u3067\u5206\u304B\u308A\u307E\u305B\u3093",
  "id" : 505989775472340992,
  "created_at" : "2014-08-31 08:05:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 55, 60 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jawp",
      "indices" : [ 46, 51 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505986322998128641",
  "geo" : { },
  "id_str" : "505989635462295553",
  "in_reply_to_user_id" : 11484132,
  "text" : "\u300C\u30A6\u30A3\u30AD\u30DA\u30C7\u30A3\u30A2\u306E\u7BA1\u7406\u8005\u3084\u3063\u3066\u307E\u3057\u305F\uFF01\u300D\u306F\u5C31\u6D3B\u306B\u6709\u5229\u304B\u3069\u3046\u304B\u3063\u3066\u30CD\u30BF\u306F\u5272\u3068\u6614\u304B\u3089\u9244\u677F\u3067\u3059\u306D #jawp QT @kzhr: \u300C\u30A6\u30A3\u30AD\u30DA\u30C7\u30A3\u30A2\u306B\u57F7\u7B46\u3057\u3066\u793E\u4F1A\u8CA2\u732E\u3092\uFF01\uFF01\u300D\u30AD\u30E3\u30F3\u30DA\u30FC\u30F3\u3068\u304B\u3069\u3046\u3067\u3059\u304B\u306D",
  "id" : 505989635462295553,
  "in_reply_to_status_id" : 505986322998128641,
  "created_at" : "2014-08-31 08:05:04 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505988947994898432",
  "text" : "\u30B7\u30E3\u30FC\u30ED\u30C3\u30AF\u306E2nd\u30B7\u30FC\u30BA\u30F3\u307F\u308B",
  "id" : 505988947994898432,
  "created_at" : "2014-08-31 08:02:20 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505972653190959104",
  "geo" : { },
  "id_str" : "505986615726985216",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u3055\u304F\u306A\u3093\u3068\u304B\u3055\u3093\u306E\u30B5\u30FC\u30D0\u30FC\u30EB\u30FC\u30E0\u306B\u5FCD\u3073\u8FBC\u3081\u3070\u89E6\u308C\u308B\u306E\u3067\u306F\uFF01\u71B1\u3044\u304B\u3082\u3042\u3044\u308C\u306A\u3044\u3051\u3069\u3002",
  "id" : 505986615726985216,
  "in_reply_to_status_id" : 505972653190959104,
  "created_at" : "2014-08-31 07:53:04 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u307F",
      "screen_name" : "yu_minpg",
      "indices" : [ 0, 9 ],
      "id_str" : "40223746",
      "id" : 40223746
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505972079309516800",
  "geo" : { },
  "id_str" : "505972469077798912",
  "in_reply_to_user_id" : 40223746,
  "text" : "@yu_minpg \u305D\u308C\u3044\u3044\uFF01",
  "id" : 505972469077798912,
  "in_reply_to_status_id" : 505972079309516800,
  "created_at" : "2014-08-31 06:56:51 +0000",
  "in_reply_to_screen_name" : "yu_minpg",
  "in_reply_to_user_id_str" : "40223746",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505970321346686976",
  "text" : "\u6D17\u6FEF\u3057\u305F\u3051\u3069\u3082\u3046\u3059\u3050\u96E8\u3089\u3057\u304F\u3066\u60B2\u3057\u3093\u3067\u308B",
  "id" : 505970321346686976,
  "created_at" : "2014-08-31 06:48:19 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u307F",
      "screen_name" : "yu_minpg",
      "indices" : [ 0, 9 ],
      "id_str" : "40223746",
      "id" : 40223746
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505963144921890816",
  "geo" : { },
  "id_str" : "505964891442663424",
  "in_reply_to_user_id" : 40223746,
  "text" : "@yu_minpg \u307E\u3060\u967D\u306F\u9AD8\u3044\u3067\u3059\u3088\uFF1F",
  "id" : 505964891442663424,
  "in_reply_to_status_id" : 505963144921890816,
  "created_at" : "2014-08-31 06:26:45 +0000",
  "in_reply_to_screen_name" : "yu_minpg",
  "in_reply_to_user_id_str" : "40223746",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u307F",
      "screen_name" : "yu_minpg",
      "indices" : [ 0, 9 ],
      "id_str" : "40223746",
      "id" : 40223746
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505961916569628672",
  "geo" : { },
  "id_str" : "505962040632950784",
  "in_reply_to_user_id" : 40223746,
  "text" : "@yu_minpg \u4ECA\u304B\u3089\u3067\u3082\uFF01",
  "id" : 505962040632950784,
  "in_reply_to_status_id" : 505961916569628672,
  "created_at" : "2014-08-31 06:15:25 +0000",
  "in_reply_to_screen_name" : "yu_minpg",
  "in_reply_to_user_id_str" : "40223746",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304D\u308A\u304F\u305A\u3069\u3046\uFF1A\u7565\u79F0\u30AF\u30BA\u3055\u3093",
      "screen_name" : "kirikuzudo",
      "indices" : [ 0, 11 ],
      "id_str" : "93370967",
      "id" : 93370967
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505821685388283904",
  "geo" : { },
  "id_str" : "505927053258915840",
  "in_reply_to_user_id" : 93370967,
  "text" : "@kirikuzudo \u305D\u306E\u672C\u306E\u901A\u308A\u306B\u4F5C\u3063\u3066\u307F\u3066\u7C92\u5B50\u6CD5\u306B\u5E7B\u6EC5\u3057\u306A\u3044\u3067\u304F\u3060\u3055\u3044\u306D\u30FB\u30FB\u30FB",
  "id" : 505927053258915840,
  "in_reply_to_status_id" : 505821685388283904,
  "created_at" : "2014-08-31 03:56:23 +0000",
  "in_reply_to_screen_name" : "kirikuzudo",
  "in_reply_to_user_id_str" : "93370967",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505924791954460672",
  "text" : "ingress\u3001\u6700\u521D\u306B\u767A\u8868\u3055\u308C\u3066\u65E5\u672C\u3067\u3067\u304D\u308B\u3088\u3046\u306B\u306A\u3063\u305F\u6642\u306B\u3061\u3087\u3053\u3063\u3068\u3084\u3063\u305F\u3051\u3069\u3001\u8AB0\u3082\u30D7\u30EC\u30A4\u3057\u3066\u306A\u304F\u3066\u3064\u307E\u3089\u306A\u304B\u3063\u305F\u3053\u3068\u3057\u304B\u899A\u3048\u3066\u306A\u3044\u3051\u3069\u3001\u4ECA\u767D\u71B1\u3057\u3066\u308B\u306A\u3089\u3001\u518D\u53C2\u52A0\u3057\u3066\u3082\u3088\u3044\u6C17\u304C\u3059\u308B",
  "id" : 505924791954460672,
  "created_at" : "2014-08-31 03:47:24 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3076\u3093\u3061\u3087\u3046",
      "screen_name" : "yutopp",
      "indices" : [ 0, 7 ],
      "id_str" : "51032597",
      "id" : 51032597
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 90, 112 ],
      "url" : "http:\/\/t.co\/steyP3n3QR",
      "expanded_url" : "http:\/\/appllio.com\/20140830-5622-ingress-is-real-war",
      "display_url" : "appllio.com\/20140830-5622-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "505924409995960320",
  "in_reply_to_user_id" : 51032597,
  "text" : "@yutopp \u304C\u5EC3\u4EBA\u3068\u3057\u3066\u7D39\u4ECB\u3055\u308C\u3066\u308B\u305E\uFF01\uFF01 \/ Google\u306E\u30EA\u30A2\u30EB\u9663\u53D6\u308A\u30B2\u30FC\u30E0\u300CIngress\u300D\u304C\u30D7\u30EC\u30A4\u30E4\u30FC\u306E\u4EBA\u7684\u30FB\u7269\u7684\u8CC7\u6E90\u3092\u6295\u5165\u3059\u308B\u300C\u6226\u4E89\u300D\u306E\u821E\u53F0\u3068\u306A\u308B\u904E\u7A0B | \u30A2\u30D7\u30EA\u30AA http:\/\/t.co\/steyP3n3QR",
  "id" : 505924409995960320,
  "created_at" : "2014-08-31 03:45:53 +0000",
  "in_reply_to_screen_name" : "yutopp",
  "in_reply_to_user_id_str" : "51032597",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505717908056453120",
  "text" : "\u81EA\u5206\u3067\u3054\u98EF\u4F5C\u308B\u3068\u3053\u3046\u3044\u3046\u306E\u304C\u3044\u3044\u3088\u306D\u3001\u597D\u304D\u306A\u3082\u306E\u3092\u597D\u304D\u306A\u3060\u3051\u98DF\u3079\u3089\u308C\u308B",
  "id" : 505717908056453120,
  "created_at" : "2014-08-30 14:05:19 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505717521610055683",
  "text" : "\u6700\u9AD8\u3060\u3063\u305F\u3001\u5E78\u305B\u3060\u3063\u305F\u3001\u304A\u8179\u3044\u3063\u3071\u3044",
  "id" : 505717521610055683,
  "created_at" : "2014-08-30 14:03:47 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/505714855580405761\/photo\/1",
      "indices" : [ 10, 32 ],
      "url" : "http:\/\/t.co\/nb8gz9nOGV",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BwSo-zqCUAAzL1H.jpg",
      "id_str" : "505714855144214528",
      "id" : 505714855144214528,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BwSo-zqCUAAzL1H.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 191,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1920
      }, {
        "h" : 337,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      } ],
      "display_url" : "pic.twitter.com\/nb8gz9nOGV"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505714855580405761",
  "text" : "\u539A\u5207\u308A\u30D9\u30FC\u30B3\u30F3\u6700\u9AD8 http:\/\/t.co\/nb8gz9nOGV",
  "id" : 505714855580405761,
  "created_at" : "2014-08-30 13:53:11 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505705921780006912",
  "geo" : { },
  "id_str" : "505707485643018240",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u304A\u306F\u3088\u3046\uFF01",
  "id" : 505707485643018240,
  "in_reply_to_status_id" : 505705921780006912,
  "created_at" : "2014-08-30 13:23:54 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505707399391375360",
  "text" : "\u6700\u8FD1\u30C0\u30FC\u30C4\u3044\u3063\u3066\u306A\u3044\u306A",
  "id" : 505707399391375360,
  "created_at" : "2014-08-30 13:23:34 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505706314970845184",
  "text" : "\u4ECA\u65E5\u306F\u6DBC\u3057\u3044\u304B\u3089\u30AF\u30FC\u30E9\u30FC\u306A\u3057\u3067\u5BDD\u308C\u308B\u306D",
  "id" : 505706314970845184,
  "created_at" : "2014-08-30 13:19:15 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505705865278537728",
  "text" : "\u5BDD\u3066\u305F",
  "id" : 505705865278537728,
  "created_at" : "2014-08-30 13:17:28 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "t_masuda",
      "screen_name" : "tmasda",
      "indices" : [ 0, 7 ],
      "id_str" : "91782180",
      "id" : 91782180
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505639961580687360",
  "geo" : { },
  "id_str" : "505648692481634304",
  "in_reply_to_user_id" : 91782180,
  "text" : "@tmasda \u306A\u308B\u307B\u3069\u3001\u4F55\u304B\u4ED6\u306B\u3082\u5206\u304B\u3063\u305F\u3089\u6559\u3048\u3066\u4E0B\u3055\u3044\uFF01",
  "id" : 505648692481634304,
  "in_reply_to_status_id" : 505639961580687360,
  "created_at" : "2014-08-30 09:30:17 +0000",
  "in_reply_to_screen_name" : "tmasda",
  "in_reply_to_user_id_str" : "91782180",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "t_masuda",
      "screen_name" : "tmasda",
      "indices" : [ 0, 7 ],
      "id_str" : "91782180",
      "id" : 91782180
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505636922538991617",
  "geo" : { },
  "id_str" : "505637689991782400",
  "in_reply_to_user_id" : 91782180,
  "text" : "@tmasda \u3069\u3093\u306A\u306E\u304C\u3042\u308A\u307E\u3059\uFF1F",
  "id" : 505637689991782400,
  "in_reply_to_status_id" : 505636922538991617,
  "created_at" : "2014-08-30 08:46:34 +0000",
  "in_reply_to_screen_name" : "tmasda",
  "in_reply_to_user_id_str" : "91782180",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505614892385046528",
  "text" : "\u30BD\u30FC\u30B9\u30B3\u30FC\u30C9C\u3084C++\u306E\u4FDD\u5B88\u6027\u3068\u304B\u3092\u8A55\u4FA1\u3057\u3066\u304F\u308C\u308B\u8A3A\u65AD\u30C4\u30FC\u30EB\u307F\u305F\u3044\u306A\u306E\u306A\u3044\u3067\u3059\u304B\u306D\u3002\u914D\u5217\u30AA\u30FC\u30D0\u30FC\u30D5\u30ED\u30FC\u3068\u304B\u306E\u9759\u7684\u89E3\u6790\u30C4\u30FC\u30EB\u3058\u3083\u306A\u304F\u3066\u3001\u4F8B\u3048\u30701\u95A2\u6570\u304C\u7121\u99C4\u306B\u9577\u3044\u3068\u304B\u30B3\u30D4\u30DA\u7B87\u6240\u304C\u6563\u898B\u3055\u308C\u308B\u3068\u304B\u3092\u6307\u6A19\u5316\u3057\u3066\u304F\u308C\u308B\u3084\u3064\u3002",
  "id" : 505614892385046528,
  "created_at" : "2014-08-30 07:15:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505582242509250560",
  "text" : "\u5727\u529B\u3060\u3044\u3058\u3067\u3059\u3088\uFF01\u306A\u3093\u305F\u3063\u3066\u5727\u529B\u3053\u305D\u304C\u6D41\u4F53\u306E\u99C6\u52D5\u529B\u3067\u3059\u304B\u3089\u306D\uFF01",
  "id" : 505582242509250560,
  "created_at" : "2014-08-30 05:06:14 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u306F\u3088\u3084\u308C",
      "indices" : [ 31, 36 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505582039077113856",
  "text" : "\u8A08\u7B97\u30B7\u30DF\u30E5\u30EC\u30FC\u30B7\u30E7\u30F3\u52C9\u5F37\u4F1A\u3044\u3064\u3067\u3059\u304B\u30FC\uFF1F\u3063\u3066\u5727\u529B\u3092\u53D7\u3051\u307E\u3057\u305F #\u306F\u3088\u3084\u308C",
  "id" : 505582039077113856,
  "created_at" : "2014-08-30 05:05:26 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505577707661307904",
  "geo" : { },
  "id_str" : "505580341801652225",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3066\u3078\u307A\u308D",
  "id" : 505580341801652225,
  "in_reply_to_status_id" : 505577707661307904,
  "created_at" : "2014-08-30 04:58:41 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505580105381318656",
  "text" : "C\u8A00\u8A9E\u3067\u52E4\u6020\u7BA1\u7406\u30B7\u30B9\u30C6\u30E0\u4F5C\u308B\u3068\u304B\u95C7\u3059\u304E\u308B\u30FB\u30FB\u30FB\u707D\u5BB3\u30EC\u30D9\u30EB\u3084\u3002",
  "id" : 505580105381318656,
  "created_at" : "2014-08-30 04:57:45 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505577092977680384",
  "geo" : { },
  "id_str" : "505577626430210048",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3054\u3081\u3093\u3061\u3083",
  "id" : 505577626430210048,
  "in_reply_to_status_id" : 505577092977680384,
  "created_at" : "2014-08-30 04:47:53 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505577592288579584",
  "text" : "\u767A\u8868\u3057\u306A\u3044\u3067\u8074\u304F\u3060\u3051\u306E\u52C9\u5F37\u4F1A\u53C2\u52A0\u306B\u610F\u5473\u306F\u306A\u3044\u3068\u601D\u3063\u3066\u308B\u3051\u3069\u3001\u4ECA\u65E5\u306F\u52C9\u5F37\u4F1A\u306F\u3064\u3044\u3067\u307F\u305F\u3044\u306A\u3082\u3093\u3060\u304B\u3089\u3002",
  "id" : 505577592288579584,
  "created_at" : "2014-08-30 04:47:45 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505577222866890752",
  "text" : "\u767A\u8868\u3057\u306A\u3044\u304B\u3089\u304B",
  "id" : 505577222866890752,
  "created_at" : "2014-08-30 04:46:17 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505576987885174784",
  "text" : "\u52C9\u5F37\u4F1A15\u5206\u524D\u306B\u7740\u304F\u3068\u304B\u5947\u8DE1",
  "id" : 505576987885174784,
  "created_at" : "2014-08-30 04:45:21 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505574992054980608",
  "text" : "\u5272\u308A\u306B\u3086\u3063\u304F\u308A\u6563\u6B69\u3057\u306A\u304C\u3089\u5411\u304B\u3048\u3066\u308B\u3002\u3044\u3064\u3082\u52C9\u5F37\u4F1A\u524D\u3063\u3066\u3042\u308F\u3066\u3066\u3066\u3053\u306E\u8FBA\u304F\u308B\u3068\u8D70\u3063\u3066\u308B\u304B\u3089\u306A\u3041\u3002",
  "id" : 505574992054980608,
  "created_at" : "2014-08-30 04:37:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505573604025573377",
  "geo" : { },
  "id_str" : "505574714798923776",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u98DF\u3079\u308C\u308B\u304B\u308C\u306A\u3044\u304B\u306E\u554F\u984C\u3058\u3083\u306A\u3044\uFF01\u98DF\u3079\u308B\u304B\u306A\u3044\u306E\u304B\u304C\u554F\u984C\u306A\u3093\u3060\uFF01",
  "id" : 505574714798923776,
  "in_reply_to_status_id" : 505573604025573377,
  "created_at" : "2014-08-30 04:36:19 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505571691653636097",
  "geo" : { },
  "id_str" : "505574046222647296",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u6B21\u306E\u4E16\u4EE3\u304C\u305D\u3046\u306A\u308C\u308B\u3088\u3046\u3001\u52AA\u529B\u3057\u3066\u9032\u6B69\u3055\u305B\u306A\u3051\u308C\u3070\u306A\u308A\u307E\u305B\u3093\u306D",
  "id" : 505574046222647296,
  "in_reply_to_status_id" : 505571691653636097,
  "created_at" : "2014-08-30 04:33:40 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505550731756068864",
  "geo" : { },
  "id_str" : "505573018886631424",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3044\u307E\u306F\u3084\u3089\u306A\u3044\u306E\u30FC\uFF1F",
  "id" : 505573018886631424,
  "in_reply_to_status_id" : 505550731756068864,
  "created_at" : "2014-08-30 04:29:35 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505568071008808960",
  "geo" : { },
  "id_str" : "505569417506193408",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u3053\u308C\u3060\u3051\u793E\u4F1A\u304C\u767A\u9054\u3057\u305F\u306E\u3060\u304B\u3089\u3001\u4E0A\u3092\u671B\u307E\u306A\u3044\u4EBA\u306F\u50CD\u304B\u306A\u304F\u3066\u3082\u3044\u3044\u4ED5\u7D44\u307F\u306B\u306A\u3063\u3066\u3082\u3044\u3044\u6C17\u304C\u3059\u308B\u3093\u3067\u3059\u3051\u3069\u306D\u3002",
  "id" : 505569417506193408,
  "in_reply_to_status_id" : 505568071008808960,
  "created_at" : "2014-08-30 04:15:16 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3048\u3093\u305D",
      "screen_name" : "ens_o",
      "indices" : [ 0, 6 ],
      "id_str" : "127528231",
      "id" : 127528231
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "505017708862111745",
  "geo" : { },
  "id_str" : "505032207224692736",
  "in_reply_to_user_id" : 127528231,
  "text" : "@ens_o \u3084\u3063\u3071\u308A\u6B73\u3092\u53D6\u308B\u3068\u30FB\u30FB\u30FB\uFF1F",
  "id" : 505032207224692736,
  "in_reply_to_status_id" : 505017708862111745,
  "created_at" : "2014-08-28 16:40:35 +0000",
  "in_reply_to_screen_name" : "ens_o",
  "in_reply_to_user_id_str" : "127528231",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 104, 126 ],
      "url" : "http:\/\/t.co\/GeRMArR8Si",
      "expanded_url" : "http:\/\/sdtimes.com\/amd-announces-heterogeneous-c-amp-language-developers\/",
      "display_url" : "sdtimes.com\/amd-announces-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "505031794240917505",
  "text" : "C++AMP\u304C\u3044\u3064\u306E\u9593\u306B\u304BLinux(clang\/LLVM)\u3067\u4F7F\u3048\u308B\u3088\u3046\u306B\u306A\u3063\u3066\u305F \/ AMD announces heterogeneous C++ AMP language for developers http:\/\/t.co\/GeRMArR8Si",
  "id" : 505031794240917505,
  "created_at" : "2014-08-28 16:38:57 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "505028395776761857",
  "text" : "\u30B7\u30E3\u30FC\u30ED\u30C3\u30AF\u89B3\u3066\u308B\u30D2\u30DE\u304C\u306A\u3044",
  "id" : 505028395776761857,
  "created_at" : "2014-08-28 16:25:27 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3057\u3057\u3083\u3082\uFF20\u30A6\u30ED\u30B3\u306E\u5973",
      "screen_name" : "shamo_6",
      "indices" : [ 0, 8 ],
      "id_str" : "1727272765",
      "id" : 1727272765
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504660217045331968",
  "geo" : { },
  "id_str" : "504660756176982016",
  "in_reply_to_user_id" : 1727272765,
  "text" : "@shamo_6 \u3044\u3044\u3093\u3067\u306F",
  "id" : 504660756176982016,
  "in_reply_to_status_id" : 504660217045331968,
  "created_at" : "2014-08-27 16:04:35 +0000",
  "in_reply_to_screen_name" : "shamo_6",
  "in_reply_to_user_id_str" : "1727272765",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504628862932639746",
  "text" : "10\u6708\u4E2D\u9803\u306B\u3059\u308B\u304B\u306A\u3041",
  "id" : 504628862932639746,
  "created_at" : "2014-08-27 13:57:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504628721920131073",
  "text" : "\u8A08\u7B97\u30B7\u30DF\u30E5\u30EC\u30FC\u30B7\u30E7\u30F3\u52C9\u5F37\u4F1A\u3092\u6765\u6708\u672B\u306B\u3059\u308B\u3063\u3066\u3044\u3044\u3064\u3064\u4F55\u306E\u6E96\u5099\u3082\u3057\u3066\u306A\u304B\u3063\u305F\u3053\u3068\u306B\u6C17\u3065\u304D\u3073\u3063\u304F\u308A\u3057\u3066\u3044\u308B\u3002",
  "id" : 504628721920131073,
  "created_at" : "2014-08-27 13:57:17 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504570050888495104",
  "text" : "\u307F\u3064\u3051\u305F",
  "id" : 504570050888495104,
  "created_at" : "2014-08-27 10:04:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504568624212750336",
  "text" : "\u6E2F\u5357\u53E3\u304D\u305F\u3051\u3069\u3001\u3055\u3066\uFF1F",
  "id" : 504568624212750336,
  "created_at" : "2014-08-27 09:58:29 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504565396683235328",
  "text" : "\u4ECA\u65E5\u306E\u4F1A\u306B\u547C\u3070\u308C\u305F\u306F\u3044\u3044\u3051\u3069\u3001\u96C6\u5408\u5834\u6240\u304C\u3075\u308F\u3063\u3068\u3057\u3066\u308B\u3057\u305D\u3082\u305D\u3082\u547C\u3093\u3060\u4EBA\u306F\u9045\u308C\u305D\u3046\u3068\u304B\u306E\u3067\u5272\u308A\u3068\u5371\u6A5F\u3067\u3042\u308B\u3053\u3068\u306B\u6C17\u3065\u3044\u305F\u96C6\u540815\u5206\u524D",
  "id" : 504565396683235328,
  "created_at" : "2014-08-27 09:45:39 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504564694988771328",
  "text" : "\u300C\u884C\u5217\u5272\u308A\u8FBC\u307F\u300D\u304C\u30D7\u30ED\u30B0\u30E9\u30DF\u30F3\u30B0\u306E\u8A71\u306B\u3057\u304B\u601D\u3048\u306A\u3044\u52E2",
  "id" : 504564694988771328,
  "created_at" : "2014-08-27 09:42:52 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504560291175084033",
  "text" : "\u6708\u6C34\u91D1\u3068\u5916\u98DF\u3067\u30EA\u30C3\u30C1\u307E\u30FC\u3093",
  "id" : 504560291175084033,
  "created_at" : "2014-08-27 09:25:22 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504290101296263168",
  "text" : "\u3055\u3066\u3001\u5BDD\u306A\u3051\u308C\u3070",
  "id" : 504290101296263168,
  "created_at" : "2014-08-26 15:31:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u30C0\u30A4\u30A4\u30F3\u30B0\u30E1\u30C3\u30BB\u30FC\u30B8",
      "indices" : [ 21, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504289844281880579",
  "text" : "C\u8A00\u8A9E\u5206\u304B\u3089\u3093\u3002C++\u306E\u65B9\u304C\u307E\u3060\u5206\u304B\u308B\u3002 #\u30C0\u30A4\u30A4\u30F3\u30B0\u30E1\u30C3\u30BB\u30FC\u30B8",
  "id" : 504289844281880579,
  "created_at" : "2014-08-26 15:30:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504287401502457856",
  "text" : "\u3073\u3063\u304F\u308A\u3057\u3059\u304E\u3066\u8AA4\u5909\u63DB\uFF01",
  "id" : 504287401502457856,
  "created_at" : "2014-08-26 15:21:00 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504287366375165954",
  "text" : "\u4E45\u3057\u3076\u308A\u306B\u96C6\u4E2D\u3057\u3066\u6642\u9593\u6728\u306B\u3057\u3066\u306A\u304B\u3063\u305F",
  "id" : 504287366375165954,
  "created_at" : "2014-08-26 15:20:52 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504287324746698753",
  "text" : "\u3073\u3063\u304F\u308A\u3057\u305F",
  "id" : 504287324746698753,
  "created_at" : "2014-08-26 15:20:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504287284921790464",
  "text" : "\u304A\u3046\u308F\u3001\u3042\u304B\u3093\u3001\u3082\u3046\u3053\u3093\u306A\u6642\u9593\u3060\u304C\u306D\uFF01",
  "id" : 504287284921790464,
  "created_at" : "2014-08-26 15:20:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504262955039354881",
  "text" : "\u30BB\u30ED\u30EA\u3063\u3066\u306A\u3093\u304B\u6D88\u6BD2\u6DB2\u306E\u5473\u304C\u3059\u308B\u304B\u3089\u597D\u304D\u3058\u3083\u306A\u3044",
  "id" : 504262955039354881,
  "created_at" : "2014-08-26 13:43:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504225764099624960",
  "text" : "\u4ECA\u65E5\u306F\u8C46\u8150\u3054\u98EF\u3067\u3059",
  "id" : 504225764099624960,
  "created_at" : "2014-08-26 11:16:04 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504219863250788353",
  "geo" : { },
  "id_str" : "504219978136961024",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u6301\u3063\u3066\u305F\u3093\u3067\u3059\u304B",
  "id" : 504219978136961024,
  "in_reply_to_status_id" : 504219863250788353,
  "created_at" : "2014-08-26 10:53:05 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504219925729124354",
  "text" : "\u9BD6\u306E4.5\u5DFB\u3001\u3059\u3067\u306B\u843D\u66F8\u304D\u3067\u524D\u306B\u307F\u305F\u3053\u3068\u3042\u308B\u3084\u3064\u306E\u63CF\u304D\u76F4\u3057\u3060\u3063\u305F\u306E\u304B\u3002\u30C7\u30B8\u30E3\u30D6\u306B\u3057\u3066\u306F\u5909\u3060\u3068\u601D\u3063\u305F\u3002",
  "id" : 504219925729124354,
  "created_at" : "2014-08-26 10:52:52 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504203202359611392",
  "text" : "4\u793E\u304C\u5404\u793E1\u6642\u9593\u305A\u3064\u4F5C\u3063\u305F4\u6642\u9593\u756A\u7D44\u30921\u6642\u9593\u3054\u3068\u306B\u5206\u5272\u3057\u30664\u30B8\u30E7\u30D6\u3067\u8A08\u7B97\u3057\u305F\u5834\u5408\u3001\u305D\u308C\u306F\u4E26\u5217\u304B\u4E26\u884C\u304B\u3063\u3066\u8A71\uFF1F",
  "id" : 504203202359611392,
  "created_at" : "2014-08-26 09:46:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504200553614700544",
  "text" : "\u3068\u3053\u308D\u3067VisualStudio\u3067constexpr\u306F\u3044\u3064\u304B\u3089\u4F7F\u3048\u308B\u3088\u3046\u306B\u306A\u308B\u3093\u3067\u3057\u3087\u3046\u304B",
  "id" : 504200553614700544,
  "created_at" : "2014-08-26 09:35:54 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504194027495751680",
  "geo" : { },
  "id_str" : "504195712687411200",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u3058\u3083\u3042\u660E\u65E5\u3082\u65E9\u304F\u5E30\u308B\u3053\u3068\u306B\u3057\u307E\u3059",
  "id" : 504195712687411200,
  "in_reply_to_status_id" : 504194027495751680,
  "created_at" : "2014-08-26 09:16:40 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504195258721116160",
  "text" : "\u5E30\u3063\u3066\u30C7\u30FC\u30BF\u3068\u30C7\u30FC\u30BF\u69CB\u9020\u3092\u5206\u96E2\u3059\u308B\u3060\u3051\u306E\u7C21\u5358\u306A\u304A\u4ED5\u4E8B\u3092\u3059\u308B",
  "id" : 504195258721116160,
  "created_at" : "2014-08-26 09:14:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504193753452511232",
  "text" : "\u305D\u3046\u3044\u3084\u660E\u65E5\u306E\u591C\u3054\u98EF\u306F\u3069\u3046\u306A\u3063\u305F\u3093\u3060\u308D\u3046\u304B\u3002\u884C\u3063\u3066\u3044\u3044\u306E\u3093\uFF1F",
  "id" : 504193753452511232,
  "created_at" : "2014-08-26 09:08:52 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504073426537943040",
  "geo" : { },
  "id_str" : "504098070989004800",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u3084\u3093\u3067\u308B\u3002\u3002",
  "id" : 504098070989004800,
  "in_reply_to_status_id" : 504073426537943040,
  "created_at" : "2014-08-26 02:48:40 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30CF\u30EB",
      "screen_name" : "_haru_",
      "indices" : [ 0, 7 ],
      "id_str" : "15616888",
      "id" : 15616888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504094752296284160",
  "geo" : { },
  "id_str" : "504098063950962689",
  "in_reply_to_user_id" : 15616888,
  "text" : "@_haru_ \u30A8\u30BE\u30B7\u30DE\u30EA\u30B9\u306B\u30AF\u30C3\u30AD\u30FC\u3042\u3052\u3066\u611B\u3067\u308B\u4ED5\u4E8B\u3057\u3066\u66AE\u3089\u3057\u305F\u3044",
  "id" : 504098063950962689,
  "in_reply_to_status_id" : 504094752296284160,
  "created_at" : "2014-08-26 02:48:38 +0000",
  "in_reply_to_screen_name" : "_haru_",
  "in_reply_to_user_id_str" : "15616888",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504058543612243968",
  "geo" : { },
  "id_str" : "504067924655480832",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u3055\u3059\u304C\uFF01\uFF01",
  "id" : 504067924655480832,
  "in_reply_to_status_id" : 504058543612243968,
  "created_at" : "2014-08-26 00:48:53 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30CF\u30EB",
      "screen_name" : "_haru_",
      "indices" : [ 0, 7 ],
      "id_str" : "15616888",
      "id" : 15616888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504051559655669760",
  "geo" : { },
  "id_str" : "504067855218769921",
  "in_reply_to_user_id" : 15616888,
  "text" : "@_haru_ \u3044\u3044\u306A\u3041",
  "id" : 504067855218769921,
  "in_reply_to_status_id" : 504051559655669760,
  "created_at" : "2014-08-26 00:48:36 +0000",
  "in_reply_to_screen_name" : "_haru_",
  "in_reply_to_user_id_str" : "15616888",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504059918249234432",
  "text" : "\u3059\u305A\u3057\u3044",
  "id" : 504059918249234432,
  "created_at" : "2014-08-26 00:17:04 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "504058218373328896",
  "text" : "\u300C\u306A\u308B\u307F\u300D\u3068\u805E\u3044\u3066\u300C\u8AB0\u305D\u308C\uFF1F\u300D\u3068\u304B\u8A00\u3063\u3061\u3083\u3046\u4EBA\u306B\u300E\u540D\u63A2\u5075\u30B3\u30CA\u30F3\u300F\u30D5\u30A1\u30F3\u3092\u540D\u4E57\u3063\u3066\u307B\u3057\u304F\u306A\u3044\u3067\u3059\u306D",
  "id" : 504058218373328896,
  "created_at" : "2014-08-26 00:10:18 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "MAX-WING",
      "screen_name" : "sento_daisuki",
      "indices" : [ 0, 14 ],
      "id_str" : "634427637",
      "id" : 634427637
    } ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jawp",
      "indices" : [ 116, 121 ]
    } ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "504048282469797888",
  "geo" : { },
  "id_str" : "504049714505850880",
  "in_reply_to_user_id" : 634427637,
  "text" : "@sento_daisuki \u81EA\u524D\u3068\u3044\u3046\u3068\u8CA1\u56E3\u5B50\u98FC\u3044\u307F\u305F\u3044\u306A\u5370\u8C61\u3092\u53D7\u3051\u308B\u5358\u8A9E\u3067\u3059\u304C\u3001\u3042\u306E\u63D0\u6848\u306F\u9006\u3067\u3059\u3002\u6280\u8853\u7684\u306A\u90E8\u5206\u3082\u3001\u4F55\u304C\u30A6\u30A3\u30AD\u30E1\u30C7\u30A3\u30A2\u904B\u52D5\u306B\u6C42\u3081\u3089\u308C\u3066\u3044\u308B\u306E\u304B\u5206\u304B\u3063\u3066\u3044\u308B\u30A6\u30A3\u30AD\u30E1\u30C7\u30A3\u30A2\u30F3\u306B\u3088\u3063\u3066\u69CB\u6210\u3055\u308C\u308B\u65B9\u304C\u3088\u3044\u3001\u3068\u3044\u3046\u8A71\u3067\u3059\u3002 #jawp",
  "id" : 504049714505850880,
  "in_reply_to_status_id" : 504048282469797888,
  "created_at" : "2014-08-25 23:36:31 +0000",
  "in_reply_to_screen_name" : "sento_daisuki",
  "in_reply_to_user_id_str" : "634427637",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503903528440459264",
  "text" : "\u6700\u8FD1\u306FOpenMP\u3068\u304BMPI\u3068\u304B\u53E4\u5178\u7684\u306A\u306E\u3070\u304B\u308A\u3067\u3064\u3089\u304B\u3063\u305F\u306E\u3067\u3002",
  "id" : 503903528440459264,
  "created_at" : "2014-08-25 13:55:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503903250240659459",
  "text" : "GPGPU\u66F8\u304D\u305F\u3044\u6B32\u9AD8\u307E\u3063\u305F",
  "id" : 503903250240659459,
  "created_at" : "2014-08-25 13:54:31 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "vigorous_action@wiki",
      "screen_name" : "vigorous_action",
      "indices" : [ 0, 16 ],
      "id_str" : "157933828",
      "id" : 157933828
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503844394772881410",
  "geo" : { },
  "id_str" : "503844809098801154",
  "in_reply_to_user_id" : 157933828,
  "text" : "@vigorous_action \u305D\u3046\u3044\u3046\u306E\u3082\u3072\u3063\u304F\u308B\u3081\u3066\u8AE6\u3081\u308B\u3057\u304B\u306A\u3044\u72B6\u6CC1\u304C\u3042\u308B\u3001\u3063\u3066\u307F\u3093\u306A\u5206\u304B\u308C\u3070\u3044\u3044\u3067\u3059\u3051\u3069\u306D\u3002",
  "id" : 503844809098801154,
  "in_reply_to_status_id" : 503844394772881410,
  "created_at" : "2014-08-25 10:02:18 +0000",
  "in_reply_to_screen_name" : "vigorous_action",
  "in_reply_to_user_id_str" : "157933828",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503841883605315586",
  "geo" : { },
  "id_str" : "503844391253835777",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u4E2D\u592E\u5317\u6539\u672D\u6A2A\u306E\u30C9\u30C8\u30FC\u30EB\u306E\u3068\u3053\u3044\u307E\u3075",
  "id" : 503844391253835777,
  "in_reply_to_status_id" : 503841883605315586,
  "created_at" : "2014-08-25 10:00:38 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "vigorous_action@wiki",
      "screen_name" : "vigorous_action",
      "indices" : [ 0, 16 ],
      "id_str" : "157933828",
      "id" : 157933828
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503843078038245376",
  "geo" : { },
  "id_str" : "503843780097622016",
  "in_reply_to_user_id" : 157933828,
  "text" : "@vigorous_action \u307E\u3041\u3002\u3067\u3082\u7D20\u4EBA\u3067\u306F\u306A\u3044\u306E\u3067\u306F\u3002",
  "id" : 503843780097622016,
  "in_reply_to_status_id" : 503843078038245376,
  "created_at" : "2014-08-25 09:58:12 +0000",
  "in_reply_to_screen_name" : "vigorous_action",
  "in_reply_to_user_id_str" : "157933828",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503843432423362560",
  "text" : "\u3088\u3057\u3001\u9BD6\u3092\u30B2\u30C3\u30C8\u3057\u305F",
  "id" : 503843432423362560,
  "created_at" : "2014-08-25 09:56:49 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503841883605315586",
  "geo" : { },
  "id_str" : "503842937294188546",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u308A\u3087\u304B\u3044\u3067\u3059\u3002",
  "id" : 503842937294188546,
  "in_reply_to_status_id" : 503841883605315586,
  "created_at" : "2014-08-25 09:54:51 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "vigorous_action@wiki",
      "screen_name" : "vigorous_action",
      "indices" : [ 0, 16 ],
      "id_str" : "157933828",
      "id" : 157933828
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503840783418408960",
  "geo" : { },
  "id_str" : "503842840405741569",
  "in_reply_to_user_id" : 157933828,
  "text" : "@vigorous_action \u5ACC\u3067\u3082\u533B\u8005\u304C\u7121\u7406\u3063\u3066\u8A00\u3046\u306A\u3089\u8AE6\u3081\u308B\u3057\u304B",
  "id" : 503842840405741569,
  "in_reply_to_status_id" : 503840783418408960,
  "created_at" : "2014-08-25 09:54:28 +0000",
  "in_reply_to_screen_name" : "vigorous_action",
  "in_reply_to_user_id_str" : "157933828",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 39, 62 ],
      "url" : "https:\/\/t.co\/rHf4WsXl2x",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53fb0591498e94d1914be8b4?s=856lbgMHUui5OV203W88rW2Q1_s&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "503840409643409408",
  "text" : "\u79FB\u8EE2\u3057\u3066\u305F (@ \u6709\u96A3\u5802 \u6A2A\u6D5C\u99C5\u897F\u53E3\u30B3\u30DF\u30C3\u30AF\u738B\u56FD in \u6A2A\u6D5C\u5E02, \u795E\u5948\u5DDD\u770C) https:\/\/t.co\/rHf4WsXl2x",
  "id" : 503840409643409408,
  "created_at" : "2014-08-25 09:44:49 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503839894796369921",
  "text" : "\u96FB\u8ECA\u5185\u5BD2\u3044\u3051\u3069\u5916\u6691\u3044\u3002\u4E0A\u7740\u3092\u7740\u305F\u308A\u8131\u3044\u3060\u308A\u3002\u3002",
  "id" : 503839894796369921,
  "created_at" : "2014-08-25 09:42:46 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503839700361048064",
  "text" : "\u6551\u6025\u306E\u30C8\u30EA\u30A2\u30FC\u30B8\u30BF\u30B0\u306E\u8A71\u3001\u3059\u308B\u306E\u306F\u3044\u3044\u3068\u601D\u3046\u3093\u3060\u3051\u3069\u3001\u305D\u308C\u3092\u4E16\u9593\u4E00\u822C\u4EBA\u306B\u5E83\u3081\u3066\u3069\u3046\u3059\u308B\u306E\u3063\u3066\u3002\u300C\u52A9\u304B\u3063\u305F\u304B\u3082\u3057\u308C\u306A\u3044\u306E\u306B\uFF01\u533B\u8005\u306B\u6BBA\u3055\u308C\u305F\uFF01\u300D\u3063\u3066\u308F\u3081\u304F\u4EBA\u304C\u5897\u3048\u3066\u4ED6\u306E\u6CBB\u7642\u304C\u90AA\u9B54\u3055\u308C\u308B\u5BB3\u304C\u5897\u3048\u308B\u3060\u3051\u3067\u306F\u306A\u3044\u306E\uFF1F",
  "id" : 503839700361048064,
  "created_at" : "2014-08-25 09:42:00 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503809359936098304",
  "geo" : { },
  "id_str" : "503838475464896513",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u3061\u3087\u3063\u3068\u65E9\u304F\u3064\u3044\u305F\u306E\u3067\u305D\u306E\u8FBA\u3046\u308D\u3064\u3044\u3066\u307E\u3059\u306D\u30FC\u3002",
  "id" : 503838475464896513,
  "in_reply_to_status_id" : 503809359936098304,
  "created_at" : "2014-08-25 09:37:08 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503809359936098304",
  "geo" : { },
  "id_str" : "503818000932159488",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u3058\u3083\u3042\u6A2A\u6D5C\u99C519\u6642\u3067\uFF01",
  "id" : 503818000932159488,
  "in_reply_to_status_id" : 503809359936098304,
  "created_at" : "2014-08-25 08:15:46 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503750271650918400",
  "geo" : { },
  "id_str" : "503757106307022849",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u305D\u3046\u3067\u3059\u306D\u3001\u8ABF\u3079\u305F\u3089\u4F55\u3082\u306A\u304B\u3063\u305F\u3067\u3059\u3002\u305D\u308C\u306A\u3089\u3001\u54C1\u5DDD\u304B\u6A2A\u6D5C\u3067\u3059\uFF1F\u5C11\u3057\u5916\u308C\u3066\u3088\u3051\u308C\u3070\u5927\u4E95\u753A\u306E\u8089\u5BFF\u53F8\u3068\u3044\u3046\u624B\u3082",
  "id" : 503757106307022849,
  "in_reply_to_status_id" : 503750271650918400,
  "created_at" : "2014-08-25 04:13:48 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503722170988511232",
  "geo" : { },
  "id_str" : "503750087000875008",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u3068\u308A\u3042\u3048\u305A\u6642\u9593\u306F19\u6642\u3068\u304B\u3067\u3044\u3044\u3067\u3059\u304B\u306D\u3002\u3061\u3087\u3063\u3068\u79C1\u3082\u63A2\u3057\u3066\u307F\u307E\u3059\u3002DM\u306E\u4EF6\u306F\u3054\u98EF\u98DF\u3079\u306A\u304C\u3089\u8A71\u3067\u3082\u3044\u3044\u3067\u3059\uFF1F",
  "id" : 503750087000875008,
  "in_reply_to_status_id" : 503722170988511232,
  "created_at" : "2014-08-25 03:45:54 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503710223169638400",
  "geo" : { },
  "id_str" : "503720179528777729",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u79C1\u306F\u6CBF\u7DDA\u306A\u3089\u3069\u3053\u3067\u3082\u3044\u3044\u3093\u3067\u3059\u304C\uFF57\u3002\u5927\u5D0E\u3067\u3044\u3044\u3067\u3059\u304B\u306D\u30FC\u3002",
  "id" : 503720179528777729,
  "in_reply_to_status_id" : 503710223169638400,
  "created_at" : "2014-08-25 01:47:04 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503702548830638081",
  "text" : "\u3059\u305F\u3093\u3069\u3070\u3044\u307F\u30FC\u3068\u304B\u8A00\u3063\u3066\u30C9\u30E9\u3048\u3082\u3093\u3092\u611F\u52D5\u30E2\u30CE\u306B\u3057\u3061\u3083\u3063\u305F\u306E\u304C\u65E5\u672C\u4EBA\u306E\u30C0\u30E1\u306A\u3068\u3053\u308D\u3058\u3083\u306A\u3044\u3067\u3059\u304B\u306D",
  "id" : 503702548830638081,
  "created_at" : "2014-08-25 00:37:00 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 31 ],
      "url" : "http:\/\/t.co\/4dNqJmjZhg",
      "expanded_url" : "http:\/\/www.comico.jp\/articleList.nhn?titleNo=1898&f=a",
      "display_url" : "comico.jp\/articleList.nh\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "503701742232416256",
  "text" : "\u3060\u3044\u305F\u3044\u3042\u3063\u3066\u308B http:\/\/t.co\/4dNqJmjZhg",
  "id" : 503701742232416256,
  "created_at" : "2014-08-25 00:33:48 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503698789144616961",
  "text" : "\u3044\u3064\u304B\u30A8\u30BE\u30B7\u30DE\u30EA\u30B9\u3092\u898B\u3066\u307F\u305F\u3044\u3051\u3069\u3001\u3069\u3053\u884C\u3051\u3070\u898B\u3089\u308C\u308B\u3093\u3084\u308D\uFF1F",
  "id" : 503698789144616961,
  "created_at" : "2014-08-25 00:22:04 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/503698265607397376\/photo\/1",
      "indices" : [ 22, 44 ],
      "url" : "http:\/\/t.co\/IkatR9LaSW",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bv1-51FCcAASzRQ.jpg",
      "id_str" : "503698265301217280",
      "id" : 503698265301217280,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bv1-51FCcAASzRQ.jpg",
      "sizes" : [ {
        "h" : 453,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 960
      }, {
        "h" : 800,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1280,
        "resize" : "fit",
        "w" : 960
      } ],
      "display_url" : "pic.twitter.com\/IkatR9LaSW"
    } ],
    "hashtags" : [ {
      "text" : "\u30B7\u30DE\u30EA\u30B9",
      "indices" : [ 16, 21 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503698265607397376",
  "text" : "\u305D\u308D\u305D\u308D\u30C7\u30E9\u30A6\u30A7\u30A2\u306E\u5B63\u7BC0\u3067\u3059\u306D #\u30B7\u30DE\u30EA\u30B9 http:\/\/t.co\/IkatR9LaSW",
  "id" : 503698265607397376,
  "created_at" : "2014-08-25 00:19:59 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503562562718277632",
  "geo" : { },
  "id_str" : "503562610218766336",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u8A00\u3063\u3066\u306A\u3044\u306E\u3093\uFF1F",
  "id" : 503562610218766336,
  "in_reply_to_status_id" : 503562562718277632,
  "created_at" : "2014-08-24 15:20:56 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503562387543179266",
  "geo" : { },
  "id_str" : "503562495454216194",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 RT\u3057\u306A\u304F\u3066\u3082\u3044\u3064\u3082\u8A00\u3063\u3066\u308B\u3067\u3057\u3087\uFF01",
  "id" : 503562495454216194,
  "in_reply_to_status_id" : 503562387543179266,
  "created_at" : "2014-08-24 15:20:29 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503527300239859713",
  "geo" : { },
  "id_str" : "503528983057539072",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u5E7C\u5150\u671F\u306E\u4F55\u304B\u3068\u3044\u3046\u53EF\u80FD\u6027\u3082",
  "id" : 503528983057539072,
  "in_reply_to_status_id" : 503527300239859713,
  "created_at" : "2014-08-24 13:07:19 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u306F\u304A",
      "screen_name" : "smileyhao",
      "indices" : [ 0, 10 ],
      "id_str" : "163113678",
      "id" : 163113678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503525636112019456",
  "geo" : { },
  "id_str" : "503525675475558400",
  "in_reply_to_user_id" : 163113678,
  "text" : "@smileyhao \u6B8B\u5FF5\u30FB\u30FB\u30FB",
  "id" : 503525675475558400,
  "in_reply_to_status_id" : 503525636112019456,
  "created_at" : "2014-08-24 12:54:10 +0000",
  "in_reply_to_screen_name" : "smileyhao",
  "in_reply_to_user_id_str" : "163113678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u306F\u304A",
      "screen_name" : "smileyhao",
      "indices" : [ 0, 10 ],
      "id_str" : "163113678",
      "id" : 163113678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503525533817118720",
  "geo" : { },
  "id_str" : "503525604222701569",
  "in_reply_to_user_id" : 163113678,
  "text" : "@smileyhao \u691C\u7D22\u7D50\u679C\u306F\uFF01",
  "id" : 503525604222701569,
  "in_reply_to_status_id" : 503525533817118720,
  "created_at" : "2014-08-24 12:53:53 +0000",
  "in_reply_to_screen_name" : "smileyhao",
  "in_reply_to_user_id_str" : "163113678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503524424146878464",
  "text" : "\u308C\u3044\u3093\u3055\u3093\u3092\u6398\u308B\uFF08\u610F\u5473\u6D45\uFF09",
  "id" : 503524424146878464,
  "created_at" : "2014-08-24 12:49:12 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503523455623368704",
  "geo" : { },
  "id_str" : "503523573378453504",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u306A\u306E\u306B\u30D0\u30FC\u30B8\u30E7\u30F3\u30A2\u30C3\u30D7\u3055\u308C\u308B\u306E\u304B\u30FB\u30FB\u30FB\u305D\u308C\u306F\u3064\u3089\u3044",
  "id" : 503523573378453504,
  "in_reply_to_status_id" : 503523455623368704,
  "created_at" : "2014-08-24 12:45:49 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u610F\u5473\u6DF1",
      "indices" : [ 5, 9 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503523517967523840",
  "text" : "\u7A74\u3060\u3089\u3051 #\u610F\u5473\u6DF1",
  "id" : 503523517967523840,
  "created_at" : "2014-08-24 12:45:36 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503523217621786624",
  "text" : "\u308C\u3044\u3093\u3055\u3093\u304C\u30C9\u30FC\u30CA\u30C4\u98DF\u3079\u904E\u304E\u3066\u304A\u8179\u306B\u7A74\u304C\u958B\u304F\u3068\u304B\u305D\u3046\u3044\u3046\u8A71\uFF1F",
  "id" : 503523217621786624,
  "created_at" : "2014-08-24 12:44:24 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503521858407260161",
  "geo" : { },
  "id_str" : "503522986117169152",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u305D\u3093\u306A\u306B\u9055\u3044\u3042\u308B\u306E",
  "id" : 503522986117169152,
  "in_reply_to_status_id" : 503521858407260161,
  "created_at" : "2014-08-24 12:43:29 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 73, 95 ],
      "url" : "http:\/\/t.co\/1a0SGZHouT",
      "expanded_url" : "http:\/\/homepage2.nifty.com\/well\/enum.html#ENUM_AND_SWITCH",
      "display_url" : "homepage2.nifty.com\/well\/enum.html\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "503521141562949632",
  "text" : "\u306A\u3093\u3060\u3053\u306Eswitch\u6587\u306E\u66F8\u304D\u65B9\u30FB\u30FB\u30FB \/ C++\u30DE\u30CB\u30A2\u30C3\u30AF,enum \u306E\u4F7F\u3044\u65B9,\u5217\u6319\u578B\u306E\u4F7F\u3044\u65B9,how to use enum,C++\u8A00\u8A9E\u8B1B\u5EA7 http:\/\/t.co\/1a0SGZHouT",
  "id" : 503521141562949632,
  "created_at" : "2014-08-24 12:36:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503509732665982976",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u660E\u65E5\u3067\u3059\u3051\u3069\u3001\u4F55\u98DF\u3079\u306B\u884C\u304D\u307E\u3057\u3087\u3046\uFF08\u7279\u306B\u601D\u3044\u3064\u304B\u306A\u304B\u3063\u305F\uFF09",
  "id" : 503509732665982976,
  "created_at" : "2014-08-24 11:50:49 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503445401416912897",
  "text" : "\u7121\u99C4\u306B\u30DD\u30A4\u30F3\u30BF\u5897\u3048\u308B\u304C\u4ED5\u65B9\u306A\u3044",
  "id" : 503445401416912897,
  "created_at" : "2014-08-24 07:35:11 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503444999959105538",
  "text" : "\u3093\u3093\u30FB\u30FB\u30FB\u3053\u308C\u306F\u304A\u304A\u4ED5\u4E8B\u3060",
  "id" : 503444999959105538,
  "created_at" : "2014-08-24 07:33:36 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503443428416311297",
  "text" : "\u3084\u3063\u304B\u3044\u3060\u306A\u3053\u308C",
  "id" : 503443428416311297,
  "created_at" : "2014-08-24 07:27:21 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503441905703583744",
  "text" : "\u3042\u308C\u3001\u305D\u308C\u306F\u305D\u3046\u306A\u306F\u305A\u306A\u3093\u3060\u3051\u3069\u3001\u3058\u3083\u3042\u306A\u3093\u3067\u624B\u5143\u306E\u3053\u308C\u306F\u52D5\u304F\u3093\u3060\u30FB\u30FB\u30FB\uFF1F",
  "id" : 503441905703583744,
  "created_at" : "2014-08-24 07:21:18 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 9, 31 ],
      "url" : "http:\/\/t.co\/R4iecsqya9",
      "expanded_url" : "http:\/\/shar.es\/1nV1Di",
      "display_url" : "shar.es\/1nV1Di"
    } ]
  },
  "geo" : { },
  "id_str" : "503441647539978241",
  "text" : "\u3046\u3093\u3001\u671F\u5F85\u901A\u308A\u304B http:\/\/t.co\/R4iecsqya9",
  "id" : 503441647539978241,
  "created_at" : "2014-08-24 07:20:16 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503440960496218113",
  "text" : "\u8FF7\u8D70\u3057\u3066\u3044\u308B",
  "id" : 503440960496218113,
  "created_at" : "2014-08-24 07:17:33 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3044\u3050\u306B\u3059\u3055\u3093 @\u03C0\u03BB\u03B1\u03C3\u03BC\u03B1",
      "screen_name" : "ignis_fatuus",
      "indices" : [ 0, 13 ],
      "id_str" : "25021157",
      "id" : 25021157
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503440725548089344",
  "geo" : { },
  "id_str" : "503440939998654464",
  "in_reply_to_user_id" : 25021157,
  "text" : "@ignis_fatuus \u306F\u3044\u3001\u305D\u308A\u3083\u305D\u3046\u3067\u3059",
  "id" : 503440939998654464,
  "in_reply_to_status_id" : 503440725548089344,
  "created_at" : "2014-08-24 07:17:28 +0000",
  "in_reply_to_screen_name" : "ignis_fatuus",
  "in_reply_to_user_id_str" : "25021157",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503440907786412032",
  "text" : "\u3093\u30FB\u30FB\u30FB\uFF1F\u3068\u3044\u3046\u304B\u3001\u51B7\u9759\u306B\u8003\u3048\u3066\u5F53\u305F\u308A\u524D\u3067\u306F\u3002\u79C1\u306F\u4F55\u3092\u3057\u305F\u304B\u3063\u305F\u3093\u3060\u3051\uFF1F",
  "id" : 503440907786412032,
  "created_at" : "2014-08-24 07:17:20 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503440535088922624",
  "text" : "\u308F\u304B\u3089\u3093",
  "id" : 503440535088922624,
  "created_at" : "2014-08-24 07:15:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 43, 65 ],
      "url" : "http:\/\/t.co\/hQ94ddFaGq",
      "expanded_url" : "http:\/\/shar.es\/1nVnyU",
      "display_url" : "shar.es\/1nVnyU"
    } ]
  },
  "geo" : { },
  "id_str" : "503438732058308608",
  "text" : "std::vector\u306B\u683C\u7D0D\u3057\u3066\u3044\u308B\u6642\u70B9\u3067\u89AA\u306B\u30AD\u30E3\u30B9\u30C8\u3055\u308C\u3066\u3057\u307E\u3063\u3066\u3044\u308B\u306E\u304B\u30FB\u30FB\u30FB\uFF1F http:\/\/t.co\/hQ94ddFaGq",
  "id" : 503438732058308608,
  "created_at" : "2014-08-24 07:08:41 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304F\u3044\u306A\u3061\u3083\u3093",
      "screen_name" : "kuina_ch",
      "indices" : [ 0, 9 ],
      "id_str" : "886087615",
      "id" : 886087615
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503425972486406144",
  "geo" : { },
  "id_str" : "503426953517334529",
  "in_reply_to_user_id" : 886087615,
  "text" : "@kuina_ch \u8ABF\u3079\u305F\u3089\u4ED6\u306E\u540C\u3058\u30A2\u30AB\u30A6\u30F3\u30C8\u540D\u306E\u5225\u306E\u30B5\u30FC\u30D3\u30B9\u306B\u540C\u3058\u30D1\u30B9\u30EF\u30FC\u30C9\u3067\u30ED\u30B0\u30A4\u30F3\u3057\u3088\u3046\u3068\u3057\u305F\u5F62\u8DE1\u304C\u3044\u304F\u3064\u304B\u898B\u3064\u304B\u3063\u305F\u306E\u3067\u3001\u4ED6\u306E\u3082\u898B\u306A\u304A\u3057\u305F\u307B\u3046\u304C\u3044\u3044\u304B\u3082\u3067\u3059\u3002",
  "id" : 503426953517334529,
  "in_reply_to_status_id" : 503425972486406144,
  "created_at" : "2014-08-24 06:21:53 +0000",
  "in_reply_to_screen_name" : "kuina_ch",
  "in_reply_to_user_id_str" : "886087615",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503418965045964800",
  "text" : "\u30B7\u30FC\u30BA\u30F32\u304C\u6C17\u306B\u306A\u308B\u3002",
  "id" : 503418965045964800,
  "created_at" : "2014-08-24 05:50:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503418932481384448",
  "text" : "Sherlock\u30B7\u30FC\u30BA\u30F31\u307F\u305F\u3002\u6700\u5F8C\u3053\u3053\u3067\u7D42\u308F\u308B\u306E\u304B\u30FC\uFF01",
  "id" : 503418932481384448,
  "created_at" : "2014-08-24 05:50:01 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503385303378190336",
  "text" : "\u6700\u8FD1\u8D77\u304D\u308B\u3068\u3061\u3087\u3063\u3068\u4F53\u3044\u305F\u304B\u3063\u305F\u308A\u3059\u308B",
  "id" : 503385303378190336,
  "created_at" : "2014-08-24 03:36:23 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503234013448765440",
  "text" : "\u30ED\u30FC\u30EB\u30B1\u30FC\u30AD\u304A\u3044\u3057\u304B\u3063\u305F",
  "id" : 503234013448765440,
  "created_at" : "2014-08-23 17:35:13 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Charlotte Elizabeth",
      "screen_name" : "kozawa",
      "indices" : [ 0, 7 ],
      "id_str" : "5646002",
      "id" : 5646002
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "503131481284165632",
  "geo" : { },
  "id_str" : "503132966508167168",
  "in_reply_to_user_id" : 5646002,
  "text" : "@kozawa \u7F8E\u5473\u3057\u3044\u3063\u3066\u98DF\u3079\u3066\u304F\u308C\u3066\u308B\u304B\u3089\u3044\u3044\u3093\u3058\u3083\u306A\u3044\u3067\u3057\u3087\u3046\u304B",
  "id" : 503132966508167168,
  "in_reply_to_status_id" : 503131481284165632,
  "created_at" : "2014-08-23 10:53:41 +0000",
  "in_reply_to_screen_name" : "kozawa",
  "in_reply_to_user_id_str" : "5646002",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u540D\u53E4\u5C4B",
      "indices" : [ 32, 36 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503125499527184384",
  "text" : "\u300C\u5473\u564C\u6C41\u304C\u9ED2\u3044\u4EBA\u3068\u4ED8\u304D\u5408\u3046\u3068\u306F\u601D\u308F\u306A\u304B\u3063\u305F\u300D\u3063\u3066\u604B\u4EBA\u306B\u8A00\u308F\u308C\u305F #\u540D\u53E4\u5C4B",
  "id" : 503125499527184384,
  "created_at" : "2014-08-23 10:24:01 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "503125321780977664",
  "text" : "\u304A\u306A\u304B\u3044\u3063\u3071\u3044\u3067\u3059",
  "id" : 503125321780977664,
  "created_at" : "2014-08-23 10:23:18 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502876854584225793",
  "text" : "\u4ECA\u305D\u308C\u3067\u56DE\u5FA9\u3057\u3066\u3082\u307E\u305F\u540C\u3058\u3053\u3068\u304C\u3042\u308C\u3070\u307E\u305F\u540C\u3058\u72B6\u614B\u306B\u306A\u308B\u3057\u3001\u6839\u672C\u7684\u306A\u89E3\u6C7A\u306B\u306A\u3089\u306A\u3044\u3068\u3001\u307B\u3093\u3068\u306B\u3060\u3093\u3060\u3093\u3064\u3089\u304F\u306A\u308B",
  "id" : 502876854584225793,
  "created_at" : "2014-08-22 17:55:59 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jawp",
      "indices" : [ 93, 98 ]
    } ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/dLxw3N8p2R",
      "expanded_url" : "https:\/\/meta.wikimedia.org\/wiki\/Community_Engagement_%28Product%29\/Process_ideas#Stop_hiring_people_from_outside",
      "display_url" : "meta.wikimedia.org\/wiki\/Community\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "502859020458160128",
  "text" : "\u30A6\u30A3\u30AD\u30E1\u30C7\u30A3\u30A2\u8CA1\u56E3\u306F\u3001\u5916\u90E8\u306EFacebook\u3084Twitter\u306E\u3088\u3046\u306A\u5916\u304B\u3089\u6280\u8853\u8005\u3092\u96C7\u3046\u306E\u3092\u3084\u3081\u3066\u3001\u30B3\u30DF\u30E5\u30CB\u30C6\u30A3\u306E\u4E2D\u306E\u4EBA\u3092\u80B2\u3066\u3088\u3046\u3068\u3044\u3046\u8A71 https:\/\/t.co\/dLxw3N8p2R #jawp",
  "id" : 502859020458160128,
  "created_at" : "2014-08-22 16:45:07 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502851529531863040",
  "text" : "\u30B7\u30DE\u30EA\u30B9\u3001\u6700\u8FD1\u670D\u3068\u304B\u306B\u722A\u304C\u3072\u3063\u304B\u304B\u308A\u304B\u3051\u3066\u308B\u304B\u3089\u3001\u3044\u3044\u52A0\u6E1B\u306B\u722A\u5207\u308A\u3057\u3066\u3042\u3052\u306A\u304D\u3083\u306A\u306E\u3060\u3051\u3069\u3001\u81EA\u5206\u3067\u3084\u308B\u306E\u306F\u53B3\u3057\u3044\u304B\u3089\u3001\u52D5\u7269\u75C5\u9662\u63A2\u3059\u304B\u30FC\u3002",
  "id" : 502851529531863040,
  "created_at" : "2014-08-22 16:15:21 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502844099649478656",
  "text" : "\u4ECA\u65E5\u3082\u4ECA\u65E5\u3068\u3066\u63DA\u3052\u7269\u796D\u308A",
  "id" : 502844099649478656,
  "created_at" : "2014-08-22 15:45:50 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502831295118860288",
  "text" : "\u30A4\u30AE\u30EA\u30B9\u306E\u30B7\u30E3\u30FC\u30ED\u30C3\u30AF\u30FB\u30DB\u30FC\u30E0\u30BA\u73FE\u4EE3\u7248\u30C9\u30E9\u30DE\u307F\u308B\u3002",
  "id" : 502831295118860288,
  "created_at" : "2014-08-22 14:54:57 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502822118963167233",
  "geo" : { },
  "id_str" : "502822523331821568",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u5199\u771F\u307E\u3060\u30FC",
  "id" : 502822523331821568,
  "in_reply_to_status_id" : 502822118963167233,
  "created_at" : "2014-08-22 14:20:06 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502602502257532928",
  "geo" : { },
  "id_str" : "502603109248811008",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u4ECA\u65E5\u30821\u65E5\u9811\u5F35\u3063\u3066\u30FC\uFF01",
  "id" : 502603109248811008,
  "in_reply_to_status_id" : 502602502257532928,
  "created_at" : "2014-08-21 23:48:13 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502494498954293248",
  "text" : "\u305A\u3064\u3046\u304C\u30FC\u3072\u3069\u3044",
  "id" : 502494498954293248,
  "created_at" : "2014-08-21 16:36:39 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502493841232904192",
  "text" : "\u307B\u3089\u3001\u524D\u306B\u8A00\u3063\u305F\u3068\u304A\u308A\u3001\u591C\u306E\u3053\u306E\u9045\u3044\u6642\u9593\u306E\u65B9\u304C\u3001\u304B\u308F\u3044\u3044\u30B7\u30DE\u30EA\u30B9\u898B\u308C\u308B\u3093\u3067\u3059\u3063\u3066\uFF01\u5973\u306E\u5B50\u3092\u9023\u308C\u8FBC\u3080\u307F\u305F\u3044\u3068\u304B\u3050\u3061\u3083\u3050\u3061\u3083\u8A00\u3063\u3066\u305F\u4EBA\u3001\u3044\u3044\u304B\u3089\u6765\u3066\u307F\u306A\u3055\u3044\u3063\u3066\uFF01",
  "id" : 502493841232904192,
  "created_at" : "2014-08-21 16:34:02 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/502493184866275328\/photo\/1",
      "indices" : [ 16, 38 ],
      "url" : "http:\/\/t.co\/Xi6rWYW4V1",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bvk245hCIAAotC9.jpg",
      "id_str" : "502493184568467456",
      "id" : 502493184568467456,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bvk245hCIAAotC9.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 191,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1920
      }, {
        "h" : 337,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      } ],
      "display_url" : "pic.twitter.com\/Xi6rWYW4V1"
    } ],
    "hashtags" : [ {
      "text" : "\u30B7\u30DE\u30EA\u30B9",
      "indices" : [ 10, 15 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502493184866275328",
  "text" : "\u3061\u3087\u3063\u3068\u76EE\u304C\u958B\u3044\u305F #\u30B7\u30DE\u30EA\u30B9 http:\/\/t.co\/Xi6rWYW4V1",
  "id" : 502493184866275328,
  "created_at" : "2014-08-21 16:31:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/502492949741977601\/photo\/1",
      "indices" : [ 17, 39 ],
      "url" : "http:\/\/t.co\/HcBwHn6vjb",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/Bvk2rNuCAAAuEpV.jpg",
      "id_str" : "502492949473525760",
      "id" : 502492949473525760,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/Bvk2rNuCAAAuEpV.jpg",
      "sizes" : [ {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 191,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 1080,
        "resize" : "fit",
        "w" : 1920
      }, {
        "h" : 337,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 576,
        "resize" : "fit",
        "w" : 1024
      } ],
      "display_url" : "pic.twitter.com\/HcBwHn6vjb"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502492949741977601",
  "text" : "\u9759\u304B\u3060\u306A\u3068\u601D\u3063\u305F\u3089\u53F0\u306E\u4E0A\u3067\u5BDD\u3066\u305F http:\/\/t.co\/HcBwHn6vjb",
  "id" : 502492949741977601,
  "created_at" : "2014-08-21 16:30:29 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307D\u3093\u3053\u3064@Lv13",
      "screen_name" : "ponkotuy",
      "indices" : [ 0, 9 ],
      "id_str" : "84240596",
      "id" : 84240596
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502468657126338561",
  "geo" : { },
  "id_str" : "502469185016848384",
  "in_reply_to_user_id" : 84240596,
  "text" : "@ponkotuy \u4F9D\u5B58\u95A2\u4FC2\u306E\u3042\u308B\u3053\u3068\u306F\u300C\u521D\u671F\u5316\u5B50\u3067\u3067\u304D\u308B\u3053\u3068\u300D\u306B\u542B\u307E\u308C\u306A\u3044\u6C17\u304C\u3059\u308B\u30FB\u30FB\u30FB",
  "id" : 502469185016848384,
  "in_reply_to_status_id" : 502468657126338561,
  "created_at" : "2014-08-21 14:56:03 +0000",
  "in_reply_to_screen_name" : "ponkotuy",
  "in_reply_to_user_id_str" : "84240596",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307D\u3093\u3053\u3064@Lv13",
      "screen_name" : "ponkotuy",
      "indices" : [ 0, 9 ],
      "id_str" : "84240596",
      "id" : 84240596
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502466863990054914",
  "geo" : { },
  "id_str" : "502467890344255488",
  "in_reply_to_user_id" : 84240596,
  "text" : "@ponkotuy \u305D\u308C\u306F\u307E\u308B\u304B\u3089\u521D\u671F\u5316\u5B50\u306B\u4F9D\u5B58\u95A2\u4FC2\u304C\u3042\u308B\u3082\u306E\u3092\u5165\u308C\u308B\u306A\u3063\u3066\u3044\u3046",
  "id" : 502467890344255488,
  "in_reply_to_status_id" : 502466863990054914,
  "created_at" : "2014-08-21 14:50:55 +0000",
  "in_reply_to_screen_name" : "ponkotuy",
  "in_reply_to_user_id_str" : "84240596",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502454711065141248",
  "text" : "\u30B3\u30FC\u30C9\u30B5\u30A4\u30BA\u3084\u6027\u80FD\u306F\u30D0\u30AF\u304C\u306A\u3044\u30FB\u30D0\u30B0\u304C\u8D77\u304D\u306B\u304F\u3044\u3053\u3068\u524D\u63D0\u3060\u3068\u601D\u3063\u3066\u751F\u304D\u3066\u304D\u305F\u304C\u3001\u305D\u308C\u3088\u308A\u9759\u7684\u89E3\u6790\u30C4\u30FC\u30EB\u306E\u307B\u3046\u304C\u5927\u4E8B\u3089\u3057\u3044\u3068\u805E\u304D\u3001\u6EC5\u3073\u3066\u307B\u3057\u3044\u3068\u6765\u5E74\u306E\u4E03\u5915\u306B\u304A\u9858\u3044\u3057\u3088\u3046\u3068\u5FC3\u306B\u6C7A\u3081\u305F\u3002",
  "id" : 502454711065141248,
  "created_at" : "2014-08-21 13:58:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502091365690253312",
  "geo" : { },
  "id_str" : "502091504324599809",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u30B5\u30A6\u30CA\u6691\u3044\u304B\u3089\u5ACC\u3044\uFF57",
  "id" : 502091504324599809,
  "in_reply_to_status_id" : 502091365690253312,
  "created_at" : "2014-08-20 13:55:17 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502089309936037889",
  "geo" : { },
  "id_str" : "502090541576638466",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u982D\u306F\u3058\u3081\u3068\u3044\u3046\u304B\u982D\u306E\u307F\u3067\u3059\u304C\uFF57",
  "id" : 502090541576638466,
  "in_reply_to_status_id" : 502089309936037889,
  "created_at" : "2014-08-20 13:51:28 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502088492365541376",
  "geo" : { },
  "id_str" : "502088683743223810",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u6700\u8FD1\u306F\u982D\u304B\u3089\u6C57\u3060\u304F\u306B\u3088\u304F\u306A\u308B\u3093\u3067\u3059\u304C\u3069\u3046\u3057\u305F\u3089\u3044\u3044\u3067\u3059\u304B",
  "id" : 502088683743223810,
  "in_reply_to_status_id" : 502088492365541376,
  "created_at" : "2014-08-20 13:44:05 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30EC\u30E2\u30F3\u30D4\u30FC\u30EB\u30FB\u3077\u3061\u3053",
      "screen_name" : "puchlco",
      "indices" : [ 0, 8 ],
      "id_str" : "88585892",
      "id" : 88585892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502078374215294976",
  "geo" : { },
  "id_str" : "502079194835062784",
  "in_reply_to_user_id" : 88585892,
  "text" : "@puchlco \u30AB\u30B2\u30ED\u30A6\u3067\u3059\u304B\uFF01",
  "id" : 502079194835062784,
  "in_reply_to_status_id" : 502078374215294976,
  "created_at" : "2014-08-20 13:06:22 +0000",
  "in_reply_to_screen_name" : "puchlco",
  "in_reply_to_user_id_str" : "88585892",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30EC\u30E2\u30F3\u30D4\u30FC\u30EB\u30FB\u3077\u3061\u3053",
      "screen_name" : "puchlco",
      "indices" : [ 0, 8 ],
      "id_str" : "88585892",
      "id" : 88585892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "502076541421895680",
  "geo" : { },
  "id_str" : "502076647017689089",
  "in_reply_to_user_id" : 88585892,
  "text" : "@puchlco \u30C8\u30A4\u30EC\u306F\u3069\u3046\u3057\u3066\u308B\u3093\u3067\u3059\u304B\uFF01\uFF01",
  "id" : 502076647017689089,
  "in_reply_to_status_id" : 502076541421895680,
  "created_at" : "2014-08-20 12:56:15 +0000",
  "in_reply_to_screen_name" : "puchlco",
  "in_reply_to_user_id_str" : "88585892",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502069172491141120",
  "text" : "UPS\u3092\u3044\u3044\u52A0\u6E1B\u306B\u65B0\u3057\u3044\u306E\u306B\u3057\u308D\u3063\u3066\u8A71\u3067\u3059\u306D",
  "id" : 502069172491141120,
  "created_at" : "2014-08-20 12:26:33 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502069102605635584",
  "text" : "\u306A\u3093\u304B\u8D77\u52D5\u76F4\u5F8C\u304B\u3089\u304A\u304B\u3057\u3044\u306A\u30FB\u30FB\u30FB\u3069\u3046\u3057\u305F",
  "id" : 502069102605635584,
  "created_at" : "2014-08-20 12:26:16 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502061545396781056",
  "text" : "\u6700\u8FD1\u3001\u99C5\u304B\u3089\u5BB6\u306B\u5E30\u3063\u3066\u6B69\u304F\u3060\u3051\u3067\u6C57\u3060\u304F\u306B\u306A\u308B\u304B\u3089\u3001\u66F4\u5E74\u671F",
  "id" : 502061545396781056,
  "created_at" : "2014-08-20 11:56:14 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "502048605369163776",
  "text" : "\u304D\u3087\u3046\u306F\u63DA\u3052\u7269\u306E\u6C17\u5206\u3002\u4F55\u3092\u63DA\u3052\u3088\u3046\u304B\u3002",
  "id" : 502048605369163776,
  "created_at" : "2014-08-20 11:04:49 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501731948826357760",
  "text" : "\u3053\u306E\u3054\u6642\u4E16\u306BVisualStudio\u306E10\u30B3\u308420\u30B3\u7ACB\u3061\u4E0A\u3052\u305F\u3050\u3089\u3044\u3067\u3076\u3061\u3076\u3061\u8A00\u3046\u8001\u5BB3\u306B\u306F\u306A\u308A\u305F\u304F\u306A\u3044\u3067\u3059\u306D",
  "id" : 501731948826357760,
  "created_at" : "2014-08-19 14:06:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501728492094881792",
  "text" : "\u3063\u305F\u305F\u304F\u3093\u304C\u7570\u65B9\u6027\u6750\u6599\u306B\u624B\u3092\u51FA\u305D\u3046\u3068\u3057\u3066\u3044\u30664\u968E\u306E\u30C6\u30F3\u30BD\u30EB\u304C\u3093\u3070\u308C\u30FC\u3063\u3066\u306A\u308B",
  "id" : 501728492094881792,
  "created_at" : "2014-08-19 13:52:48 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501700686728290304",
  "text" : "\u77E5\u3063\u3066\u306F\u3044\u3051\u306A\u3044\u3082\u306E\u3092\u898B\u3066\u3057\u307E\u3063\u305F\u6C17\u304C\u3059\u308B\u306E\u3067\u3001\u3053\u306E\u79D8\u5BC6\u3092\u30B9\u30D1\u30A4\u306B\u72D9\u308F\u308C\u308B\u3002",
  "id" : 501700686728290304,
  "created_at" : "2014-08-19 12:02:19 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501576964960813056",
  "geo" : { },
  "id_str" : "501700351888609281",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u6642\u9593\u3068\u5834\u6240\u306F\u307E\u305F\u6539\u3081\u3066\uFF01",
  "id" : 501700351888609281,
  "in_reply_to_status_id" : 501576964960813056,
  "created_at" : "2014-08-19 12:00:59 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501576173734748160",
  "text" : "\u3063\u3066\u8003\u3048\u308B\u3068\u3001\u3084\u308A\u305F\u3044\u3053\u3068\u3092\u3084\u308B\/\u3084\u308A\u305F\u3044\u3053\u3068\u3092\u3084\u3089\u305B\u308B\u3063\u3066\u306E\u306F\u3001\u4F5C\u696D\u52B9\u7387\u3060\u3051\u3092\u8003\u3048\u3066\u3082\u3001\u3068\u3066\u3082\u91CD\u8981\u306A\u3053\u3068\u306A\u3093\u3067\u3059\u306D\u3047\u3002",
  "id" : 501576173734748160,
  "created_at" : "2014-08-19 03:47:33 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501575787074424832",
  "text" : "\u307E\u3041\u7B2C\u4E00\u3044\u307E\u3084\u3063\u3066\u308B\u3053\u3068\u306B\u305D\u3093\u306A\u8A87\u308A\u3042\u308B\u308F\u3051\u3067\u3082\u306A\u3044\u304B\u3089\u306D\u3047\u3002\u597D\u304D\u3067\u3084\u308A\u305F\u3044\u3053\u3068\u306A\u3089\u5225\u306B\u3057\u3066\u3002",
  "id" : 501575787074424832,
  "created_at" : "2014-08-19 03:46:01 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501574305830801409",
  "text" : "\u304B\u3063\u3053\u3088\u304F\u306F\u306A\u3044\u306E\u3060\u304C\u3001\u305D\u3082\u305D\u3082\u671F\u9593\u3092\u597D\u304D\u306B\u533A\u5207\u3063\u3066\u3044\u3044\u3088\u3063\u3066\u4E38\u6295\u3052\u3055\u308C\u305F\u7D50\u679C\u304C\u3053\u308C\u306A\u306E\u3067\u3001\u307E\u3041\u6B8B\u308A\u306E\u6642\u9593\u306F\u3086\u3063\u305F\u308A\u904E\u3054\u3057\u307E\u3059\u304B\u3002",
  "id" : 501574305830801409,
  "created_at" : "2014-08-19 03:40:07 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501573413119328256",
  "text" : "\u3063\u3066\u3044\u3046\u8A71\u3092\u601D\u3044\u51FA\u3057\u3066\u307F\u308B\u306B\u3001\u4ECA\u30012\u9031\u9593\u304B\u304B\u308B\u3063\u3066\u4E88\u60F3\u3055\u308C\u3066\u305F\u4F5C\u696D\u30921\u65E5\u3067\u7D42\u308F\u3089\u305B\u305F\u3053\u3068\u3067\u300C\u65E9\u304F\u7D42\u308F\u3089\u305B\u3066\u65E9\u304F\u6B21\u306E\u3053\u3068\u3084\u3063\u3066\u3082\u7279\u306B\u8A55\u4FA1\u3055\u308C\u308B\u308F\u3051\u3067\u3082\u306A\u304F\u305D\u306E\u307E\u307E\u3084\u308B\u3053\u3068\u5897\u3048\u308B\u3060\u3051\u3060\u3082\u3093\u306A\u3041\u300D\u3068\u8003\u3048\u3066\u308B\u3042\u305F\u308A\u306F\u304B\u3063\u3053\u3088\u304F\u306A\u3044\u3002",
  "id" : 501573413119328256,
  "created_at" : "2014-08-19 03:36:35 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501572044526325760",
  "text" : "\u300C\u53E9\u304B\u308C\u308B\u3053\u3068\u3070\u304B\u308A\u3067\u8912\u3081\u3089\u308C\u308B\u3053\u3068\u306F\u4E00\u5207\u306A\u3044\u304C\u305D\u308C\u304C\u5F53\u7136\u3067\u3001\u305D\u306E\u72B6\u6CC1\u3053\u305D\u304C\u79C1\u9054\u306E\u7406\u60F3\u300D\u3063\u3066\u8A00\u3044\u5207\u3063\u3066\u305F\u3042\u305F\u308A\u3059\u3054\u3044\u306A\u30FC\u3068\u601D\u3063\u305F\u3002\u82E5\u8005\u3092\u524D\u306B\u3057\u3066\u3048\u30FC\u304B\u3063\u3053\u3057\u3043\u3067\u8A00\u3063\u305F\u3060\u3051\u304B\u3082\u3057\u308C\u3093\u3051\u3069\u3001\u78BA\u304B\u306B\u304B\u3063\u3053\u3088\u304B\u3063\u305F\u3002",
  "id" : 501572044526325760,
  "created_at" : "2014-08-19 03:31:08 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501570897388384257",
  "text" : "\u30BC\u30CD\u30B3\u30F3\u3068\u304B\u56FD\u4EA4\u7701\u5730\u65B9\u6574\u5099\u5C40\u306E\u5049\u3044\u65B9\u3005\u3068\u3054\u98EF\u3092\u98DF\u3079\u308B\u6A5F\u4F1A\u304C\u3042\u3063\u305F\u306E\u3060\u3051\u3069\u3001\u305D\u306E\u6642\u8A71\u984C\u306B\u306A\u3063\u3066\u305F\u306E\u306F\u300C\u6700\u8FD1\u306E\u571F\u6728\u696D\u754C\u306F\u6069\u7740\u305B\u304C\u307E\u3057\u3044\u3002\u304A\u308C\u3089\u306E\u4ED5\u4E8B\u306F\u3001\u3042\u3063\u3066\u5F53\u305F\u308A\u524D\u3067\u554F\u984C\u304C\u8D77\u304D\u305F\u3089\u53E9\u304B\u308C\u3066\u5F53\u7136\u306A\u3093\u3060\u3002\u305D\u308C\u304C\u571F\u6728\u3067\u3042\u308A\u793E\u4F1A\u57FA\u76E4\u306A\u3093\u3060\u300D\u3063\u3066\u8A71\u3067\u3001\u3044\u305F\u304F\u611F\u52D5\u3057\u305F\u3082\u306E\u3067\u3059\u3002",
  "id" : 501570897388384257,
  "created_at" : "2014-08-19 03:26:35 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501539798209675264",
  "geo" : { },
  "id_str" : "501565640105144320",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u76F8\u624B\u304C\u305D\u308C\u3092\u53D7\u3051\u5165\u308C\u3066\u304F\u308C\u308B\u306A\u3089\u826F\u3044\u306E\u3067\u306F\uFF1F",
  "id" : 501565640105144320,
  "in_reply_to_status_id" : 501539798209675264,
  "created_at" : "2014-08-19 03:05:41 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501525318910226432",
  "geo" : { },
  "id_str" : "501562787034722305",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u3058\u3083\u304225\u65E5\u306B\u3054\u98EF\u884C\u304D\u307E\u3057\u3087\u30FC",
  "id" : 501562787034722305,
  "in_reply_to_status_id" : 501525318910226432,
  "created_at" : "2014-08-19 02:54:21 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501562643685978112",
  "text" : "\u590F\u3060\u306D\u30FC",
  "id" : 501562643685978112,
  "created_at" : "2014-08-19 02:53:47 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501560709470101504",
  "text" : "\u590F\u3063\u307D\u3044\u306A\u30FC",
  "id" : 501560709470101504,
  "created_at" : "2014-08-19 02:46:06 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501376995922440193",
  "text" : "MPI\u3068uBLAS\u4F7F\u3046\u305F\u3081\u3060\u3051\u306Bboost\u306F\u5B58\u5728\u3059\u308B",
  "id" : 501376995922440193,
  "created_at" : "2014-08-18 14:36:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u7DCF\u9577@\u771F\u3063\u59EB\u60A3\u8005",
      "screen_name" : "krustf",
      "indices" : [ 0, 7 ],
      "id_str" : "57958675",
      "id" : 57958675
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501376255321591808",
  "geo" : { },
  "id_str" : "501376872815394818",
  "in_reply_to_user_id" : 57958675,
  "text" : "@krustf \u307E\u3041\u3060\u3044\u305F\u3044\u306F\u30D8\u30C3\u30C0\u3067\u6E08\u307F\u307E\u3059\u3051\u3069\u306D\u30FC",
  "id" : 501376872815394818,
  "in_reply_to_status_id" : 501376255321591808,
  "created_at" : "2014-08-18 14:35:36 +0000",
  "in_reply_to_screen_name" : "krustf",
  "in_reply_to_user_id_str" : "57958675",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u7DCF\u9577@\u771F\u3063\u59EB\u60A3\u8005",
      "screen_name" : "krustf",
      "indices" : [ 0, 7 ],
      "id_str" : "57958675",
      "id" : 57958675
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501375754316181506",
  "geo" : { },
  "id_str" : "501375861174456320",
  "in_reply_to_user_id" : 57958675,
  "text" : "@krustf \u6642\u9593\u3060\u3051\u304B\u304B\u308A\u307E\u3059\u306D\u30FB\u30FB\u30FB",
  "id" : 501375861174456320,
  "in_reply_to_status_id" : 501375754316181506,
  "created_at" : "2014-08-18 14:31:35 +0000",
  "in_reply_to_screen_name" : "krustf",
  "in_reply_to_user_id_str" : "57958675",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501371863289692161",
  "text" : "AMGCG\u6CD5\u3001\u3061\u3083\u3093\u3068\u4EBA\u306B\u6559\u3048\u3089\u308C\u308B\u3050\u3089\u3044\u7406\u89E3\u3057\u3088\u3046\u3068\u601D\u3044\u7D9A\u3051\u3066\u306F\u30842\u5E74\u3050\u3089\u3044\u306A\u6C17\u304C\u3059\u308B",
  "id" : 501371863289692161,
  "created_at" : "2014-08-18 14:15:41 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501371432438218753",
  "text" : "\u758E\u884C\u5217\u306E\u9B54\u8853\u5E2B\u306B\u306A\u3063\u3066AMG\u30DE\u30B9\u30BF\u30FC\u306B\u306A\u308A\u305F\u3044",
  "id" : 501371432438218753,
  "created_at" : "2014-08-18 14:13:59 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501371317572993024",
  "text" : "\u758E\u884C\u5217\u306E\u9B54\u8853\u5E2B\u306A\u308A\u305F\u3044",
  "id" : 501371317572993024,
  "created_at" : "2014-08-18 14:13:31 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307E\u3044\u306F\u4E00\u5E74\u751F",
      "screen_name" : "mai_10",
      "indices" : [ 0, 7 ],
      "id_str" : "66628322",
      "id" : 66628322
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501369769702854657",
  "geo" : { },
  "id_str" : "501370005972213762",
  "in_reply_to_user_id" : 66628322,
  "text" : "@mai_10 \u3042\u3041\u3001\u306A\u308B\u307B\u3069",
  "id" : 501370005972213762,
  "in_reply_to_status_id" : 501369769702854657,
  "created_at" : "2014-08-18 14:08:19 +0000",
  "in_reply_to_screen_name" : "mai_10",
  "in_reply_to_user_id_str" : "66628322",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501369700375228418",
  "text" : "\u3055\u3066\u3001\u6708\u672B\u306E\u52C9\u5F37\u4F1A\u7533\u3057\u8FBC\u3093\u3060\u3057\u3001\u52C9\u5F37\u4F1A\u30C9\u30EA\u30D6\u30F3\u3067\u9811\u5F35\u308A\u307E\u3059\u304B\u3002",
  "id" : 501369700375228418,
  "created_at" : "2014-08-18 14:07:06 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307E\u3044\u306F\u4E00\u5E74\u751F",
      "screen_name" : "mai_10",
      "indices" : [ 0, 7 ],
      "id_str" : "66628322",
      "id" : 66628322
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501369468006576132",
  "geo" : { },
  "id_str" : "501369629004926976",
  "in_reply_to_user_id" : 66628322,
  "text" : "@mai_10 \u307E\u3044\u59C9\u3001\u30BF\u30D6\u30EC\u30C3\u30C8\u3068\u304B\u4F7F\u3063\u3066\u308B\u306E\u304B\u30FB\u30FB\u30FB",
  "id" : 501369629004926976,
  "in_reply_to_status_id" : 501369468006576132,
  "created_at" : "2014-08-18 14:06:49 +0000",
  "in_reply_to_screen_name" : "mai_10",
  "in_reply_to_user_id_str" : "66628322",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/connpass.com\" rel=\"nofollow\"\u003Econnpass\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 31, 53 ],
      "url" : "http:\/\/t.co\/bQjjZSOohH",
      "expanded_url" : "http:\/\/opencae-kanto.connpass.com\/event\/7835\/?utm_campaign=event_participate_to_follower&utm_medium=twitter&utm_source=notifications",
      "display_url" : "opencae-kanto.connpass.com\/event\/7835\/?ut\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "501368159501160450",
  "text" : "\u7B2C41\u56DE\u30AA\u30FC\u30D7\u30F3CAE\u52C9\u5F37\u4F1A\uFF20\u95A2\u6771\uFF08\u6D41\u4F53\u306A\u3069\uFF09\u306B\u53C2\u52A0\u3057\u307E\u3059\uFF01 http:\/\/t.co\/bQjjZSOohH",
  "id" : 501368159501160450,
  "created_at" : "2014-08-18 14:00:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "jawp",
      "indices" : [ 70, 75 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501366875691831296",
  "text" : "superprotect\u5C0E\u5165\u304B\u30FC\u3002\u6563\u3005\u3082\u3081\u305F\u3051\u3069\u7D50\u5C40\u305D\u306E\u307E\u307E\u8CA1\u56E3\u8077\u54E1\u3060\u3051\u304C\u4F7F\u3048\u308B\u6A29\u9650\u306A\u306E\u306D\u3047\u3002\u30B3\u30DF\u30E5\u30CB\u30C6\u30A3\u8EFD\u8996\u306B\u3064\u306A\u304C\u308B\u304B\u3069\u3046\u304B\u6CE8\u76EE\u3067\u3059\u3002 #jawp",
  "id" : 501366875691831296,
  "created_at" : "2014-08-18 13:55:52 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499002226593910784",
  "geo" : { },
  "id_str" : "501359367715684353",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca 25\u65E5\u304B26\u65E5\u3063\u3066\u304A\u6642\u9593\u3042\u308A\u307E\u3059\u304B",
  "id" : 501359367715684353,
  "in_reply_to_status_id" : 499002226593910784,
  "created_at" : "2014-08-18 13:26:02 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501356827141890048",
  "text" : "\u3057\u304B\u3057\u3086\u3063\u304F\u308A\u305B\u305A\u306B\u6B21\u306E\u4F5C\u696D\u3082\u3069\u3093\u3069\u3093\u6D88\u5316\u3057\u3066\u3001\u6708\u672B\u3068\u6765\u6708\u306B\u5099\u3048\u306D\u3070",
  "id" : 501356827141890048,
  "created_at" : "2014-08-18 13:15:57 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501356638385618944",
  "text" : "2\u9031\u9593\u304B\u304B\u308A\u305D\u3046\u3067\u3059\u3063\u3066\u8A00\u3063\u3066\u304A\u3044\u305F\u304C1\u65E5\u3067\u7D42\u308F\u3063\u305F\u3002",
  "id" : 501356638385618944,
  "created_at" : "2014-08-18 13:15:12 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501208152579141632",
  "text" : "\u305D\u3046\u3044\u3084\u6708\u672B\u306E\u30AA\u30FC\u30D7\u30F3CAE\u52C9\u5F37\u4F1A\u306E\u7533\u8FBC\u3057\u3066\u306A\u3044\u3084\u3001\u305B\u306D\u3070\u3002",
  "id" : 501208152579141632,
  "created_at" : "2014-08-18 03:25:10 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501156918128148481",
  "text" : "\u99C5\u306B\u6765\u308B\u307E\u3067\u3067\u3059\u3067\u306B\u6C57\u3060\u304F\u306A\u3093\u3067\u3059\u304C",
  "id" : 501156918128148481,
  "created_at" : "2014-08-18 00:01:34 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501040265927200769",
  "text" : "\u308A\u3075\u308C\u304F\u3073\u30FC",
  "id" : 501040265927200769,
  "created_at" : "2014-08-17 16:18:02 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u91D1\u6D77\u8FB0\u5F66",
      "screen_name" : "side_tana",
      "indices" : [ 0, 10 ],
      "id_str" : "16150989",
      "id" : 16150989
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501037953586429952",
  "geo" : { },
  "id_str" : "501038205781565441",
  "in_reply_to_user_id" : 16150989,
  "text" : "@side_tana \u3044\u3064\u3082\u601D\u3046\u3051\u3069\u3072\u3069\u3044\u305D\u308C",
  "id" : 501038205781565441,
  "in_reply_to_status_id" : 501037953586429952,
  "created_at" : "2014-08-17 16:09:51 +0000",
  "in_reply_to_screen_name" : "side_tana",
  "in_reply_to_user_id_str" : "16150989",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "501038045190045697",
  "text" : "\u304A\u571F\u7523\u306E\u30BF\u30F3\u30B9\u30E2\u30FC\u30AF\u98DF\u3079\u308B\u2192\u5358\u54C1\u3060\u3068\u306A\u2192\u6885\u9152\u3067\u3082\u98F2\u3080\u304B\u2192\u6885\u9152\u4F59\u3063\u305F\u2192\u30DD\u30C6\u30C1\u98DF\u3079\u308B\u304B\u2192\u30DD\u30C6\u30C1\u3042\u307E\u3063\u305F\uFF08\u3044\u307E\u3053\u3053\uFF09",
  "id" : 501038045190045697,
  "created_at" : "2014-08-17 16:09:13 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307B\u3089\u3075\u304D",
      "screen_name" : "n0ixe",
      "indices" : [ 0, 6 ],
      "id_str" : "140231677",
      "id" : 140231677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501036476956553218",
  "geo" : { },
  "id_str" : "501036680447393793",
  "in_reply_to_user_id" : 140231677,
  "text" : "@n0ixe \u79C1\u3082",
  "id" : 501036680447393793,
  "in_reply_to_status_id" : 501036476956553218,
  "created_at" : "2014-08-17 16:03:48 +0000",
  "in_reply_to_screen_name" : "n0ixe",
  "in_reply_to_user_id_str" : "140231677",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307B\u3089\u3075\u304D",
      "screen_name" : "n0ixe",
      "indices" : [ 0, 6 ],
      "id_str" : "140231677",
      "id" : 140231677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501036313462595588",
  "geo" : { },
  "id_str" : "501036399634567168",
  "in_reply_to_user_id" : 140231677,
  "text" : "@n0ixe \u6BBB\u4ED8\u304D\u30A8\u30D3\u3089\u3057\u3044",
  "id" : 501036399634567168,
  "in_reply_to_status_id" : 501036313462595588,
  "created_at" : "2014-08-17 16:02:41 +0000",
  "in_reply_to_screen_name" : "n0ixe",
  "in_reply_to_user_id_str" : "140231677",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307B\u3089\u3075\u304D",
      "screen_name" : "n0ixe",
      "indices" : [ 0, 6 ],
      "id_str" : "140231677",
      "id" : 140231677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "501030071088316417",
  "geo" : { },
  "id_str" : "501036195208368129",
  "in_reply_to_user_id" : 140231677,
  "text" : "@n0ixe \u30AB\u30E9\u30C3\u3068\u4E0A\u3052\u308C\u3070\u7F8E\u5473\u3057\u3044\u3089\u3057\u3044",
  "id" : 501036195208368129,
  "in_reply_to_status_id" : 501030071088316417,
  "created_at" : "2014-08-17 16:01:52 +0000",
  "in_reply_to_screen_name" : "n0ixe",
  "in_reply_to_user_id_str" : "140231677",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500971768165588992",
  "text" : "\u67D0\u52D5\u753B\u306E\u305B\u3044\u3067True Blue\u3092\u30D7\u30EC\u30A4\u3059\u308B\u305F\u3073\u306B\u541B\u304C\u4EE3\u3092\u601D\u3044\u51FA\u3059",
  "id" : 500971768165588992,
  "created_at" : "2014-08-17 11:45:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30D5\u30EB\u30FC\u30C4\u30DF\u30C3\u30AF\u30B9",
      "screen_name" : "dororich",
      "indices" : [ 14, 23 ],
      "id_str" : "40065143",
      "id" : 40065143
    }, {
      "name" : "\u9752\u5B50\u5B88\u6B4C",
      "screen_name" : "aokomoriuta",
      "indices" : [ 25, 37 ],
      "id_str" : "10902082",
      "id" : 10902082
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500955207652474880",
  "geo" : { },
  "id_str" : "500957827284746241",
  "in_reply_to_user_id" : 40065143,
  "text" : "\u4E45\u3057\u3076\u308A\u3060\u3063\u305F\u3089\u3057\u3044 QT @dororich: @aokomoriuta \u3055\u3093\u304C \u30C9\u30ED\u30EA\u30C3\u30C1\u306A\u3046 \u3057\u307E\u3057\u305F\u3002\u30B0\u30C3\u30C9\u30A4\u30D6\u30CB\u30F3\u30B0\uFF01\u3082\u3046\u98F2\u3093\u3067\u304F\u308C\u306A\u3044\u304B\u3068\u601D\u3063\u3066\u3044\u307E\u3057\u305F\u3002\u305F\u307E\u306B\u306F\u601D\u3044\u51FA\u3057\u3066\u304F\u3060\u3055\u3044\u306D\uFF1F *1Cday[today:1\/total:3]",
  "id" : 500957827284746241,
  "in_reply_to_status_id" : 500955207652474880,
  "created_at" : "2014-08-17 10:50:28 +0000",
  "in_reply_to_screen_name" : "dororich",
  "in_reply_to_user_id_str" : "40065143",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u30C9\u30ED\u30EA\u30C3\u30C1\u306A\u3046\u3092\u77E5\u3089\u306A\u3044\u4E16\u4EE3",
      "indices" : [ 0, 15 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500956976881221635",
  "text" : "#\u30C9\u30ED\u30EA\u30C3\u30C1\u306A\u3046\u3092\u77E5\u3089\u306A\u3044\u4E16\u4EE3 \u3053\u308F\u3044",
  "id" : 500956976881221635,
  "created_at" : "2014-08-17 10:47:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500954592327135232",
  "text" : "\u30C9\u30ED\u30EA\u30C3\u30C1\u306A\u3046",
  "id" : 500954592327135232,
  "created_at" : "2014-08-17 10:37:36 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500934104024510464",
  "text" : "\u304A\u571F\u7523\u306E\u8C5A\u30BF\u30F3\u98DF\u3079\u3066\u308B",
  "id" : 500934104024510464,
  "created_at" : "2014-08-17 09:16:11 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3067\u3053\u3073\u3059",
      "screen_name" : "decobisu",
      "indices" : [ 0, 9 ],
      "id_str" : "16959486",
      "id" : 16959486
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500924893605556225",
  "geo" : { },
  "id_str" : "500926893399359488",
  "in_reply_to_user_id" : 16959486,
  "text" : "@decobisu \u7D50\u5A5A\u3092\u76EE\u6307\u3057\u3066\u305F\u3089\u5A5A\u6D3B\u3063\u307D\u3044\uFF1F",
  "id" : 500926893399359488,
  "in_reply_to_status_id" : 500924893605556225,
  "created_at" : "2014-08-17 08:47:32 +0000",
  "in_reply_to_screen_name" : "decobisu",
  "in_reply_to_user_id_str" : "16959486",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3067\u3053\u3073\u3059",
      "screen_name" : "decobisu",
      "indices" : [ 0, 9 ],
      "id_str" : "16959486",
      "id" : 16959486
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500924544593723392",
  "geo" : { },
  "id_str" : "500924691263942656",
  "in_reply_to_user_id" : 16959486,
  "text" : "@decobisu \u3057\u3066\u307E\u3059\u304B",
  "id" : 500924691263942656,
  "in_reply_to_status_id" : 500924544593723392,
  "created_at" : "2014-08-17 08:38:47 +0000",
  "in_reply_to_screen_name" : "decobisu",
  "in_reply_to_user_id_str" : "16959486",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3076\u3093\u3061\u3087\u3046",
      "screen_name" : "yutopp",
      "indices" : [ 0, 7 ],
      "id_str" : "51032597",
      "id" : 51032597
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500834136446074881",
  "geo" : { },
  "id_str" : "500834216716681217",
  "in_reply_to_user_id" : 51032597,
  "text" : "@yutopp \u30C7\u30FC\u30C8\u306B\u9045\u523B\u306F\u53B3\u7981\u3067\u3059\u3088",
  "id" : 500834216716681217,
  "in_reply_to_status_id" : 500834136446074881,
  "created_at" : "2014-08-17 02:39:16 +0000",
  "in_reply_to_screen_name" : "yutopp",
  "in_reply_to_user_id_str" : "51032597",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 41, 64 ],
      "url" : "https:\/\/t.co\/HpcfBdnVoy",
      "expanded_url" : "https:\/\/twitter.com\/sasakitoshinao\/status\/494259341055512576",
      "display_url" : "twitter.com\/sasakitoshinao\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "500831482143707137",
  "text" : "\u3053\u3046\u3044\u3046\u306E\u306B\u300C\u7406\u8AD6\u5024\u300D\u3063\u3066\u8A00\u308F\u308C\u308B\u3068\u9055\u548C\u611F\u3042\u308B\u30FB\u30FB\u30FB\u3002\u300C\u4E88\u6E2C\u5024\u300D\u3058\u3083\u306A\u3044\u3093\u3067\u3059\uFF1F https:\/\/t.co\/HpcfBdnVoy",
  "id" : 500831482143707137,
  "created_at" : "2014-08-17 02:28:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u91D1\u6D77\u8FB0\u5F66",
      "screen_name" : "side_tana",
      "indices" : [ 0, 10 ],
      "id_str" : "16150989",
      "id" : 16150989
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500828254341574657",
  "geo" : { },
  "id_str" : "500828965922013184",
  "in_reply_to_user_id" : 16150989,
  "text" : "@side_tana \u540D\u524D\u5909\u3048\u3066\u306B\u3083\u3093\u3053\u3055\u3093\u306E\u30AA\u30EA\u30B8\u30CA\u30EB\u53EF\u611B\u3055\u30A2\u30D4\u30FC\u30EB\u3092",
  "id" : 500828965922013184,
  "in_reply_to_status_id" : 500828254341574657,
  "created_at" : "2014-08-17 02:18:25 +0000",
  "in_reply_to_screen_name" : "side_tana",
  "in_reply_to_user_id_str" : "16150989",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500828866982596608",
  "text" : "\u3042\u3001\u300C\u3092\u300D\u304C\u5165\u3063\u3066\u305F\u306E\u304B",
  "id" : 500828866982596608,
  "created_at" : "2014-08-17 02:18:01 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500828479646998528",
  "geo" : { },
  "id_str" : "500828811353526272",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u3058\u3083\u3042\u6B21\u306F\u30AA\u30EA\u30B8\u30CA\u30EB\u3092\uFF01",
  "id" : 500828811353526272,
  "in_reply_to_status_id" : 500828479646998528,
  "created_at" : "2014-08-17 02:17:48 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500828492301205504",
  "text" : "\u53CD\u5FDC\u3057\u306A\u3044",
  "id" : 500828492301205504,
  "created_at" : "2014-08-17 02:16:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u91D1\u6D77\u8FB0\u5F66",
      "screen_name" : "side_tana",
      "indices" : [ 0, 10 ],
      "id_str" : "16150989",
      "id" : 16150989
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500828254341574657",
  "geo" : { },
  "id_str" : "500828350508572673",
  "in_reply_to_user_id" : 16150989,
  "text" : "@side_tana \u540D\u524D\u3092\u5909\u3048\u3066\u306B\u3083\u3093\u3053\u3055\u3093\u3092\u3082\u3063\u3068\u53EF\u611B\u304F\u3057\u3066",
  "id" : 500828350508572673,
  "in_reply_to_status_id" : 500828254341574657,
  "created_at" : "2014-08-17 02:15:58 +0000",
  "in_reply_to_screen_name" : "side_tana",
  "in_reply_to_user_id_str" : "16150989",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500827302607847425",
  "geo" : { },
  "id_str" : "500828234385072130",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u3071\u304F\u308A\u306F\u3044\u3051\u307E\u305B\uFF4E",
  "id" : 500828234385072130,
  "in_reply_to_status_id" : 500827302607847425,
  "created_at" : "2014-08-17 02:15:30 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500826963703889922",
  "geo" : { },
  "id_str" : "500827067072524289",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u3053\u3053\u3067\u306B\u3083\u3093\u3053\u3055\u3093\u306B\u3088\u308B\u53EF\u611B\u3055\u30A2\u30D4\u30FC\u30EB\u82B8\u3092\u3054\u3089\u3093\u304F\u3060\u3055\u3044\u2192",
  "id" : 500827067072524289,
  "in_reply_to_status_id" : 500826963703889922,
  "created_at" : "2014-08-17 02:10:52 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500826797060022272",
  "geo" : { },
  "id_str" : "500826848964534273",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u306B\u3083\u3093\u3053\u306E\u307B\u3046\u304C\u30AB\u30EF\u30A4\u30A4\u3093\u3067\u3059\u306D",
  "id" : 500826848964534273,
  "in_reply_to_status_id" : 500826797060022272,
  "created_at" : "2014-08-17 02:10:00 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500826698686799875",
  "geo" : { },
  "id_str" : "500826756400427009",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u306B\u3083\u3093\u3053\u3068\u308F\u3093\u3053\u306F\u4F55\u304C\u9055\u3044\u307E\u3059\u304B",
  "id" : 500826756400427009,
  "in_reply_to_status_id" : 500826698686799875,
  "created_at" : "2014-08-17 02:09:38 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500826590830288896",
  "geo" : { },
  "id_str" : "500826649286283265",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u306B\u3083\u3093\u3053\u3068\u308F\u3093\u3053\u307F\u305F\u3044\u306A\uFF1F",
  "id" : 500826649286283265,
  "in_reply_to_status_id" : 500826590830288896,
  "created_at" : "2014-08-17 02:09:12 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500825764065865728",
  "geo" : { },
  "id_str" : "500826320977154050",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u3069\u3046\u9055\u3046\u3093\u3067\u3059\u304B",
  "id" : 500826320977154050,
  "in_reply_to_status_id" : 500825764065865728,
  "created_at" : "2014-08-17 02:07:54 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500769981215166465",
  "geo" : { },
  "id_str" : "500816041295872000",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u697D\u3057\u3093\u3067\u304D\u3066\u304F\u3060\u3055\u3044\uFF01",
  "id" : 500816041295872000,
  "in_reply_to_status_id" : 500769981215166465,
  "created_at" : "2014-08-17 01:27:03 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500815561903702016",
  "text" : "\u3044\u3084\u3057\u304B\u3057\u305D\u3046\u3059\u308B\u3068\u300CPowerShell\u3058\u3083\u306A\u304F\u3066C# \u3067\u3084\u308C\u300D\u6848\u4EF6\u304C\u751F\u307E\u308C\u305D\u3046\u3060\u304B\u3089\u3001\u5225\u306E\u95C7\u304C",
  "id" : 500815561903702016,
  "created_at" : "2014-08-17 01:25:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500815000315768833",
  "text" : "PowerShell\u3092exe\u306B\u30B3\u30F3\u30D1\u30A4\u30EB\uFF08\uFF1F\uFF09\u3067\u304D\u305F\u3089\u591A\u304F\u306E\u547D\u304C\u6551\u3048\u308B\u3068\u601D\u3046\uFF08\u904E\u52B4\u6B7B\u304B\u3089\uFF09",
  "id" : 500815000315768833,
  "created_at" : "2014-08-17 01:22:55 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500813990356742145",
  "text" : "bash\u308F\u304B\u3089\u3093",
  "id" : 500813990356742145,
  "created_at" : "2014-08-17 01:18:54 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500811401665855489",
  "text" : "\u304A\u8FCE\u3048\u884C\u3063\u3066\u304D\u307E\u3059",
  "id" : 500811401665855489,
  "created_at" : "2014-08-17 01:08:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304F\u3044\u306A\u3061\u3083\u3093",
      "screen_name" : "kuina_ch",
      "indices" : [ 0, 9 ],
      "id_str" : "886087615",
      "id" : 886087615
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500692267577774081",
  "geo" : { },
  "id_str" : "500692896849219584",
  "in_reply_to_user_id" : 886087615,
  "text" : "@kuina_ch \u7C89\u30E2\u30CE\u306F\u3059\u3050\u306B\u60AA\u304F\u306A\u308A\u307E\u3059\u304B\u3089\u306D\u3047",
  "id" : 500692896849219584,
  "in_reply_to_status_id" : 500692267577774081,
  "created_at" : "2014-08-16 17:17:43 +0000",
  "in_reply_to_screen_name" : "kuina_ch",
  "in_reply_to_user_id_str" : "886087615",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500692151110348800",
  "text" : "\u3042\u3064\u3044\u30FC",
  "id" : 500692151110348800,
  "created_at" : "2014-08-16 17:14:45 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304F\u3044\u306A\u3061\u3083\u3093",
      "screen_name" : "kuina_ch",
      "indices" : [ 0, 9 ],
      "id_str" : "886087615",
      "id" : 886087615
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500692042914095104",
  "geo" : { },
  "id_str" : "500692135629189121",
  "in_reply_to_user_id" : 886087615,
  "text" : "@kuina_ch \u9178\u5316\u9632\u6B62\u306E\u306F\u305A",
  "id" : 500692135629189121,
  "in_reply_to_status_id" : 500692042914095104,
  "created_at" : "2014-08-16 17:14:42 +0000",
  "in_reply_to_screen_name" : "kuina_ch",
  "in_reply_to_user_id_str" : "886087615",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304F\u3044\u306A\u3061\u3083\u3093",
      "screen_name" : "kuina_ch",
      "indices" : [ 0, 9 ],
      "id_str" : "886087615",
      "id" : 886087615
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500667360617836546",
  "geo" : { },
  "id_str" : "500667932939022336",
  "in_reply_to_user_id" : 886087615,
  "text" : "@kuina_ch \u3069\u3046\u3057\u3066\u3082\u4EBA\u304C\u591A\u3044\u3068\u305D\u3046\u306A\u308A\u307E\u3059\u3088",
  "id" : 500667932939022336,
  "in_reply_to_status_id" : 500667360617836546,
  "created_at" : "2014-08-16 15:38:31 +0000",
  "in_reply_to_screen_name" : "kuina_ch",
  "in_reply_to_user_id_str" : "886087615",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304F\u3044\u306A\u3061\u3083\u3093",
      "screen_name" : "kuina_ch",
      "indices" : [ 0, 9 ],
      "id_str" : "886087615",
      "id" : 886087615
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500666053429432320",
  "geo" : { },
  "id_str" : "500667042052046848",
  "in_reply_to_user_id" : 886087615,
  "text" : "@kuina_ch \u5207\u78CB\u7422\u78E8\u3057\u305F\u3044\u3068\u601D\u3063\u3066\u308B\u4EBA\u305F\u3061\u4EE5\u5916\u3092\u5165\u308C\u305F\u306E\u304C\u554F\u984C\u70B9\u3060\u3063\u305F\u306E\u3067\u306F",
  "id" : 500667042052046848,
  "in_reply_to_status_id" : 500666053429432320,
  "created_at" : "2014-08-16 15:34:59 +0000",
  "in_reply_to_screen_name" : "kuina_ch",
  "in_reply_to_user_id_str" : "886087615",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500655373171191810",
  "text" : "\u97F3\u30B2\u30FC\u3082\u697D\u3057\u304F\u3084\u3063\u3066\u308B\u4EBA\u3092\u898B\u305F\u3053\u3068\u304C\u306A\u3044",
  "id" : 500655373171191810,
  "created_at" : "2014-08-16 14:48:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500653519771152385",
  "text" : "\u6765\u6708\u3001\u65E5\u5149\u6771\u7167\u5BAE\u884C\u304F\u4E88\u5B9A\u3060\u304B\u3089\u3001\u300E\u8475\u5FB3\u5DDD\u4E09\u4EE3\u300F\u3092\u89B3\u3066\u5FA9\u7FD2\u3057\u3088\u3046",
  "id" : 500653519771152385,
  "created_at" : "2014-08-16 14:41:15 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500651971770974210",
  "text" : "\u4F55\u304B\u306E\u63A1\u6398\u5834\u306B\u30C7\u30FC\u30C8\u3067\u884C\u3063\u3066\u300C\u3046\u308F\u30FC\u3001\u306A\u306B\u3053\u308C\uFF01\u4E0D\u601D\u8B70\u3060\u3051\u3069\u304B\u3063\u3053\u3044\u3044\u306D\uFF01\u300D\u3063\u3066\u306F\u3057\u3083\u3044\u3067\u308B\u5973\u306E\u5B50\u306B\u3001\u51B7\u9759\u306B\u300C\u3044\u3084\u3001\u3042\u308C\u306F\u304D\u3063\u3068\u25CB\u25CB\u3063\u3066\u8A00\u3046\u306E\u3060\u3068\u601D\u3046\u3002\u305D\u306E\u6839\u62E0\u306F\u30FC\u30FB\u30FB\u30FB\u300D\u3068\u304B\u8003\u5BDF\u3057\u3060\u3057\u3066\u6C17\u6301\u3061\u60AA\u3044\u3063\u3066\u601D\u308F\u308C\u308B\u3084\u3064\u3067\u3059\u306D\u3001\u77E5\u3063\u3066\u308B\u3002",
  "id" : 500651971770974210,
  "created_at" : "2014-08-16 14:35:06 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u767D",
      "screen_name" : "parodyer",
      "indices" : [ 0, 9 ],
      "id_str" : "82635103",
      "id" : 82635103
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500645998863994880",
  "geo" : { },
  "id_str" : "500646144741883904",
  "in_reply_to_user_id" : 82635103,
  "text" : "@parodyer \u307E\u3041\u6838\u7206\u5F3E\u306F\u7BC4\u56F2\u653B\u6483\u3067\u3059\u304B\u3089\u306D\u3047",
  "id" : 500646144741883904,
  "in_reply_to_status_id" : 500645998863994880,
  "created_at" : "2014-08-16 14:11:57 +0000",
  "in_reply_to_screen_name" : "parodyer",
  "in_reply_to_user_id_str" : "82635103",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500645616788058113",
  "geo" : { },
  "id_str" : "500646062797770754",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u6838\u306E\u71B1\u9632\u3052\u306A\u3044\u3068\u539F\u5B50\u529B\u767A\u96FB\u3067\u304D\u306A\u3044\u30FB\u30FB\u30FB",
  "id" : 500646062797770754,
  "in_reply_to_status_id" : 500645616788058113,
  "created_at" : "2014-08-16 14:11:37 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500645446604177411",
  "text" : "\u305D\u3046\u3044\u3048\u3070\u9283\u6483\u3067\u3082\u58CA\u308C\u306A\u3044\u91D1\u5EAB\u306F\u6838\u7206\u5F3E\u306A\u3089\u3068\u304B\u8A00\u3063\u3066\u308B\u4EBA\u3044\u305F\u3051\u3069\u3001\u6838\u7206\u5F3E\u3063\u3066\u7269\u7406\u7684\u306A\u885D\u6483\u305D\u308C\u307B\u3069\u3060\u3068\u601D\u3046\u304B\u3089\u3001\u58CA\u308C\u306A\u3044\u3093\u3058\u3083\u306A\u3044\u3067\u3059\u304B\u306D\u3047\u3002",
  "id" : 500645446604177411,
  "created_at" : "2014-08-16 14:09:10 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500644079227514882",
  "geo" : { },
  "id_str" : "500644171741274112",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u78BA\u304B\u306B\u305D\u308C\u3067\u3059\u306D",
  "id" : 500644171741274112,
  "in_reply_to_status_id" : 500644079227514882,
  "created_at" : "2014-08-16 14:04:06 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500643863480893441",
  "geo" : { },
  "id_str" : "500643978912358400",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u3058\u3083\u3042\u3084\u3063\u3071\u308A\u98F2\u307F\u306B\u884C\u3063\u3066\u7518\u3048\u3089\u308C\u308B\u4EBA\u306B\u306A\u308B\u3093\u3067\u3059\u306D",
  "id" : 500643978912358400,
  "in_reply_to_status_id" : 500643863480893441,
  "created_at" : "2014-08-16 14:03:20 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500643588623978501",
  "geo" : { },
  "id_str" : "500643664792526848",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u98F2\u307F\u306B\u884C\u3063\u3066\u7518\u3048\u308B\u30FB\u30FB\u30FB",
  "id" : 500643664792526848,
  "in_reply_to_status_id" : 500643588623978501,
  "created_at" : "2014-08-16 14:02:05 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500643323644612610",
  "geo" : { },
  "id_str" : "500643455215747072",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u305D\u3046\u3044\u3046\u4EBA\u306B\u306A\u308D\u3046\uFF1F",
  "id" : 500643455215747072,
  "in_reply_to_status_id" : 500643323644612610,
  "created_at" : "2014-08-16 14:01:15 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500643076692381696",
  "geo" : { },
  "id_str" : "500643223253942276",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u4E00\u7DD2\u306B\u98F2\u3093\u3067\u305F\u4EBA\u3068\u304B",
  "id" : 500643223253942276,
  "in_reply_to_status_id" : 500643076692381696,
  "created_at" : "2014-08-16 14:00:20 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500642485903691776",
  "geo" : { },
  "id_str" : "500642581466714113",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u4EBA\u604B\u3057\u304F\u306A\u3063\u305F\u3089\u4EBA\u306B\u7518\u3048\u308C\u3070\u3044\u3044\u306E\u3067\u306F\uFF57",
  "id" : 500642581466714113,
  "in_reply_to_status_id" : 500642485903691776,
  "created_at" : "2014-08-16 13:57:47 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500636778663927808",
  "text" : "\u305D\u308C\u306F\u305D\u3046\u3068\u3001P.T.\u304C\u3068\u3066\u3082\u6016\u3044\u3068\u8A55\u5224\u306A\u306E\u3067\u3059\u304C",
  "id" : 500636778663927808,
  "created_at" : "2014-08-16 13:34:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500636645222129664",
  "text" : "\u3042\u3089\u3055\u30FC\u304C\u306A\u3093\u304B\u3044\u3063\u3066\u308B",
  "id" : 500636645222129664,
  "created_at" : "2014-08-16 13:34:12 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hidehiko MURAO",
      "screen_name" : "HidehikoMURAO",
      "indices" : [ 0, 14 ],
      "id_str" : "187002730",
      "id" : 187002730
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500629599080833027",
  "geo" : { },
  "id_str" : "500633406732898304",
  "in_reply_to_user_id" : 187002730,
  "text" : "@HidehikoMURAO \u306A\u308B\u307B\u3069\u30FB\u30FB\u30FB\u6D88\u3057\u30B4\u30E0\u3067\u6D88\u3057\u3066\u3082\u3069\u3063\u3061\u307F\u3061\u6C5A\u304F\u306A\u308B\u304B\u3089\u3044\u3063\u3057\u3087\u304B\u306A\u3063\u3066\u306A\u3063\u3066\u307E\u3059\uFF57",
  "id" : 500633406732898304,
  "in_reply_to_status_id" : 500629599080833027,
  "created_at" : "2014-08-16 13:21:20 +0000",
  "in_reply_to_screen_name" : "HidehikoMURAO",
  "in_reply_to_user_id_str" : "187002730",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500627717172109312",
  "geo" : { },
  "id_str" : "500627979517440000",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u3058\u3083\u3042\u65E2\u8AAD\u3082\u3064\u3051\u306A\u3044\uFF01",
  "id" : 500627979517440000,
  "in_reply_to_status_id" : 500627717172109312,
  "created_at" : "2014-08-16 12:59:46 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500626613067730947",
  "geo" : { },
  "id_str" : "500627511810609152",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u305D\u3046\u3044\u3046\u6642\u3053\u305D\u65E2\u8AAD\u653E\u7F6E\uFF1F",
  "id" : 500627511810609152,
  "in_reply_to_status_id" : 500626613067730947,
  "created_at" : "2014-08-16 12:57:54 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hidehiko MURAO",
      "screen_name" : "HidehikoMURAO",
      "indices" : [ 0, 14 ],
      "id_str" : "187002730",
      "id" : 187002730
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500609816750858242",
  "geo" : { },
  "id_str" : "500627478251978752",
  "in_reply_to_user_id" : 187002730,
  "text" : "@HidehikoMURAO \u7D19\u3068\u30DA\u30F3\u3067\u30DC\u30FC\u30EB\u30DA\u30F3\u3067\u3044\u3044\u3084\u3063\u3066\u6700\u8FD1\u306A\u3063\u3066\u307E\u3059\uFF57",
  "id" : 500627478251978752,
  "in_reply_to_status_id" : 500609816750858242,
  "created_at" : "2014-08-16 12:57:46 +0000",
  "in_reply_to_screen_name" : "HidehikoMURAO",
  "in_reply_to_user_id_str" : "187002730",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500626359882768384",
  "geo" : { },
  "id_str" : "500626453889683458",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u305C\u3093\u3076\u300C\u3042\u306A\u305F\u306B\u69CB\u3063\u3066\u307B\u3057\u3044\uFF01\u300D\u3063\u3066\u3053\u3068\u3060\u3068\u601D\u3063\u3066\uFF57",
  "id" : 500626453889683458,
  "in_reply_to_status_id" : 500626359882768384,
  "created_at" : "2014-08-16 12:53:42 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500626033427505155",
  "geo" : { },
  "id_str" : "500626137769185280",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u304B\u307E\u3048\u30FC\u3063\u3066\u3053\u3068\u3067\u306F\uFF57",
  "id" : 500626137769185280,
  "in_reply_to_status_id" : 500626033427505155,
  "created_at" : "2014-08-16 12:52:27 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500625670683099136",
  "geo" : { },
  "id_str" : "500625883300761602",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u4F8B\u3048\u3070\uFF1F",
  "id" : 500625883300761602,
  "in_reply_to_status_id" : 500625670683099136,
  "created_at" : "2014-08-16 12:51:26 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500622335972364290",
  "geo" : { },
  "id_str" : "500622597835329538",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u3053\u3063\u3061\u306F\u3055\u307B\u3069\u304A\u3044\u3057\u304F\u306A\u3044\u30A2\u30A4\u30B9\u3092\u98DF\u3079\u3066\u3044\u308B\u3068\u3044\u3046\u306E\u306B\u30FB\u30FB\u30FB\uFF08\u51B7\u51CD\u5EAB\u306E\u4E2D\u304B\u3089\u898B\u3064\u3051\u305F\uFF09",
  "id" : 500622597835329538,
  "in_reply_to_status_id" : 500622335972364290,
  "created_at" : "2014-08-16 12:38:23 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500621937794506752",
  "geo" : { },
  "id_str" : "500622219425218561",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u30EA\u30A2\u5145\u3060\uFF01",
  "id" : 500622219425218561,
  "in_reply_to_status_id" : 500621937794506752,
  "created_at" : "2014-08-16 12:36:52 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hidehiko MURAO",
      "screen_name" : "HidehikoMURAO",
      "indices" : [ 0, 14 ],
      "id_str" : "187002730",
      "id" : 187002730
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500607200222408706",
  "geo" : { },
  "id_str" : "500607304916406273",
  "in_reply_to_user_id" : 187002730,
  "text" : "@HidehikoMURAO \u6587\u5B57\u66F8\u304F\u6A5F\u4F1A\u3063\u3066\u3042\u308B\u3093\u3067\u3059\u30FB\u30FB\u30FB\uFF1F",
  "id" : 500607304916406273,
  "in_reply_to_status_id" : 500607200222408706,
  "created_at" : "2014-08-16 11:37:36 +0000",
  "in_reply_to_screen_name" : "HidehikoMURAO",
  "in_reply_to_user_id_str" : "187002730",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500604094315126787",
  "text" : "\u30D9\u30AF\u30C8\u30EB\u89E3\u6790\u3082\u5E45\u5E83\u3044\u304B\u30FB\u30FB\u30FB",
  "id" : 500604094315126787,
  "created_at" : "2014-08-16 11:24:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500603886973886467",
  "text" : "\u6975\u5EA7\u6A19\u7CFB\u3067\u306E\u30D9\u30AF\u30C8\u30EB\u89E3\u6790\u307E\u3067\u3044\u304F\u3068\u3061\u3087\u3063\u3068\u7BC4\u56F2\u306F\u72ED\u305D\u3046\u3060\u3051\u3069\u3001\u5927\u5B66\u9662\u30EC\u30D9\u30EB\u3060\u3068\u57FA\u790E\u77E5\u8B58\u3063\u307D\u3044",
  "id" : 500603886973886467,
  "created_at" : "2014-08-16 11:24:02 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500603423226470400",
  "text" : "\u3066\u3044\u3046\u304B\u5BDD\u3066\u305F\u304B\u3089\u7814\u7A76\u305B\u306D\u3070",
  "id" : 500603423226470400,
  "created_at" : "2014-08-16 11:22:11 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500603396189999104",
  "text" : "\u4ECA\u65E5\u306E\u3054\u98EF\u708A\u3044\u3066\u306A\u3044\u3084",
  "id" : 500603396189999104,
  "created_at" : "2014-08-16 11:22:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500603317915881472",
  "text" : "\u3042\u3068\u6B74\u53F2\u5C4B\u3055\u3093\u3082\u3044\u308B\u304B\u3089\u30FB\u30FB\u30FB",
  "id" : 500603317915881472,
  "created_at" : "2014-08-16 11:21:46 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500603281022808064",
  "text" : "\u5EFA\u7BC9\u306F\u307B\u3089\u3001\u82B8\u8853\u5C4B\u3055\u3093\u3082\u3044\u308B\u304B\u3089",
  "id" : 500603281022808064,
  "created_at" : "2014-08-16 11:21:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500603088533590016",
  "text" : "\u203B\u5EFA\u7BC9\u306F\u5DE5\u5B66\u306B\u542B\u307E\u306A\u3044\u5206\u985E",
  "id" : 500603088533590016,
  "created_at" : "2014-08-16 11:20:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500602900419051520",
  "text" : "\u7406\u7CFB\u3063\u3066\u5E45\u5E83\u3044\u304B\u3089\u3063\u3066\u306E\u306F\u3042\u308B\u3051\u3069\u3001\u5C11\u306A\u304F\u3068\u3082\u5DE5\u5B66\u5C4B\u3055\u3093\u3067\u6975\u5EA7\u6A19\u5206\u304B\u3089\u306A\u3044\u3063\u3066\u306E\u306F\u81F4\u547D\u7684\u3067\u306F",
  "id" : 500602900419051520,
  "created_at" : "2014-08-16 11:20:06 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500602099135033345",
  "text" : "Twitter\u306E\u516C\u5F0F\u304C\u65E2\u5B9A\u306E\u52D5\u4F5C\u3067RT\u3057\u305F\u4EBA\u3082@\u5148\u306B\u542B\u3081\u308B\u3088\u3046\u306B\u306A\u3063\u305F\u306E\u304C\u8AF8\u60AA\u306E\u6839\u6E90\u3067\u306F",
  "id" : 500602099135033345,
  "created_at" : "2014-08-16 11:16:55 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E0D\u60D1\u306E\u72E9\u4EBA@\u9154\u3044\u3069\u308C\u8266\u968A",
      "screen_name" : "tosca_fuwaku",
      "indices" : [ 19, 32 ],
      "id_str" : "325487678",
      "id" : 325487678
    }, {
      "name" : "\u9752\u5B50\u5B88\u6B4C",
      "screen_name" : "aokomoriuta",
      "indices" : [ 34, 46 ],
      "id_str" : "10902082",
      "id" : 10902082
    }, {
      "name" : "\u307D\u3093\u3053\u3064@Lv13",
      "screen_name" : "ponkotuy",
      "indices" : [ 47, 56 ],
      "id_str" : "84240596",
      "id" : 84240596
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500601733089734656",
  "geo" : { },
  "id_str" : "500601836714201088",
  "in_reply_to_user_id" : 325487678,
  "text" : "\u3044\u3084\u305D\u308C\u3067\u3082\u8981\u308B\u3067\u3057\u3087\u3046\u30FB\u30FB\u30FB QT @tosca_fuwaku: @aokomoriuta @ponkotuy \u751F\u304D\u7269\u306E\u304B\u3089\u3060\u3068\u304B\u5730\u9762\u7A7F\u308A\u8FD4\u3057\u3066\u308B\u3093\u3058\u3083\u306A\u3044\u3067\u3059\u304B\uFF1F",
  "id" : 500601836714201088,
  "in_reply_to_status_id" : 500601733089734656,
  "created_at" : "2014-08-16 11:15:53 +0000",
  "in_reply_to_screen_name" : "tosca_fuwaku",
  "in_reply_to_user_id_str" : "325487678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500601495054594048",
  "text" : "\u3042\u30FC\u3001\u9AD8\u6821\u30EC\u30D9\u30EB\u3067\u306F\u4ED5\u65B9\u306A\u3044\u3068\u3057\u3066\u3002",
  "id" : 500601495054594048,
  "created_at" : "2014-08-16 11:14:31 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307D\u3093\u3053\u3064@Lv13",
      "screen_name" : "ponkotuy",
      "indices" : [ 34, 43 ],
      "id_str" : "84240596",
      "id" : 84240596
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500601057236373506",
  "geo" : { },
  "id_str" : "500601291249164288",
  "in_reply_to_user_id" : 84240596,
  "text" : "\u3068\u3044\u3046\u304B\u6975\u5EA7\u6A19\u308F\u304B\u3089\u306A\u3044\u7406\u7CFB\u3068\u304B\u4F55\u3092\u5B66\u3093\u3067\u308B\u3093\u3067\u3059\u304B\u306D\u30FB\u30FB\u30FB QT @ponkotuy: \u307E\u3041\u5225\u306B\u7269\u7406\u3058\u3083\u306A\u304F\u3066\u3082\u7406\u7CFB\u306A\u3089\u534A\u5206\u3050\u3089\u3044\u7406\u89E3\u3067\u304D\u3066\u305D\u3046 [Jupiter]",
  "id" : 500601291249164288,
  "in_reply_to_status_id" : 500601057236373506,
  "created_at" : "2014-08-16 11:13:43 +0000",
  "in_reply_to_screen_name" : "ponkotuy",
  "in_reply_to_user_id_str" : "84240596",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500598268414263296",
  "text" : "\u306F\u30FC\u3001\u5B8C\u5168\u306B\u5BDD\u3066\u305F",
  "id" : 500598268414263296,
  "created_at" : "2014-08-16 11:01:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500472254434918400",
  "text" : "\u6C96\u7E04\u3067\u8CB7\u3063\u305F\u98DF\u3079\u308B\u30E9\u30FC\u6CB9\u3042\u3093\u307E\u3057\u304A\u3044\u3057\u304F\u306A\u3044",
  "id" : 500472254434918400,
  "created_at" : "2014-08-16 02:40:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500452058751700992",
  "text" : "\u3055\u3066\u3001\u3054\u98EF\u98DF\u3079\u3066\u7814\u7A76\u305B\u306D\u3070\u306D\u30FC",
  "id" : 500452058751700992,
  "created_at" : "2014-08-16 01:20:43 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500451668958261250",
  "geo" : { },
  "id_str" : "500451976664985601",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u7D50\u5A5A\u307E\u3067\u8003\u3048\u3066\u308B\u306A\u3093\u3066\u5C06\u6765\u898B\u636E\u3048\u3066\u3066\u4ECA\u3069\u304D\u306E\u5239\u90A3\u7684\u306A\u751F\u304D\u65B9\u3092\u3057\u3066\u308B\u5927\u5B66\u751F\u3068\u306F\u9055\u3063\u3066\u3057\u3063\u304B\u308A\u3057\u3066\u307E\u3059\u306D\uFF01",
  "id" : 500451976664985601,
  "in_reply_to_status_id" : 500451668958261250,
  "created_at" : "2014-08-16 01:20:23 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500451479384109056",
  "text" : "\u5F7C\u6C0F\u3068\u306E\u65C5\u884C\u3067\u4F0A\u8C46\u306B\u884C\u304F\u5973\u5B50\u5927\u751F\u3063\u3066\u306E\u3082\u306A\u3093\u304B\u3042\u308C\u306D",
  "id" : 500451479384109056,
  "created_at" : "2014-08-16 01:18:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500450317574500352",
  "geo" : { },
  "id_str" : "500451373431795715",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u3044\u3044\u3067\u3059\u306D\u3001\u304A\u76F8\u624B\u306F\u3084\u3063\u3071\u308A\u793E\u4F1A\u4EBA\u3067\u3059\u304B\uFF01",
  "id" : 500451373431795715,
  "in_reply_to_status_id" : 500450317574500352,
  "created_at" : "2014-08-16 01:18:00 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500449980700585985",
  "geo" : { },
  "id_str" : "500450053689860098",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u3069\u3053\u304B\u65C5\u884C\u3068\u304B\u884C\u304F\u3093\u3067\u3059\u304B\u30FC\uFF1F",
  "id" : 500450053689860098,
  "in_reply_to_status_id" : 500449980700585985,
  "created_at" : "2014-08-16 01:12:45 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500449630694285312",
  "text" : "\u4E45\u3057\u3076\u308A\u306B\u4F11\u307F\u306B\u65E9\u8D77\u304D\u3057\u305F\u3089\u4E45\u3057\u3076\u308A\u306B\u671D\u304B\u3089\u304A\u8179\u3059\u3044\u305F",
  "id" : 500449630694285312,
  "created_at" : "2014-08-16 01:11:04 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500449170692386816",
  "geo" : { },
  "id_str" : "500449278515376128",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u4ECA\u306F\u590F\u4F11\u307F\u3067\u3059\u304B\u3001\u3044\u3044\u3067\u3059\u306D\u30FC\uFF57",
  "id" : 500449278515376128,
  "in_reply_to_status_id" : 500449170692386816,
  "created_at" : "2014-08-16 01:09:40 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500448916140085248",
  "geo" : { },
  "id_str" : "500448989116764160",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u30EC\u30DD\u30FC\u30C8\u5927\u5909\u305D\u3046\u3067\u3059\u306D\uFF01",
  "id" : 500448989116764160,
  "in_reply_to_status_id" : 500448916140085248,
  "created_at" : "2014-08-16 01:08:31 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500448462874234880",
  "geo" : { },
  "id_str" : "500448746354642945",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u5973\u5B50\u5927\u751F\u3055\u3093\u304A\u5143\u6C17\u3067\u3059\u304B",
  "id" : 500448746354642945,
  "in_reply_to_status_id" : 500448462874234880,
  "created_at" : "2014-08-16 01:07:33 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500448277787971585",
  "geo" : { },
  "id_str" : "500448342300565505",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u666E\u901A\u306B\u901A\u3058\u308B\u306E\u3067\u306F\u30FC",
  "id" : 500448342300565505,
  "in_reply_to_status_id" : 500448277787971585,
  "created_at" : "2014-08-16 01:05:57 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500447639788208129",
  "geo" : { },
  "id_str" : "500447961717809152",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 25\u306F\u771F\u3093\u4E2D\u3067\u3059\u3088\uFF57\u300220\u6B73\u3063\u3066\u8A00\u3063\u3066\u3082\u901A\u3058\u308B\uFF01",
  "id" : 500447961717809152,
  "in_reply_to_status_id" : 500447639788208129,
  "created_at" : "2014-08-16 01:04:26 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500446970758971392",
  "geo" : { },
  "id_str" : "500447179853398016",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u30A2\u30E9\u30B5\u30FC\u3068\u304B\u8A00\u3063\u3066\u305F\u306E\u306B\u9055\u3046\u3058\u3083\u306A\u3044\u3067\u3059\u304B\uFF57\uFF57",
  "id" : 500447179853398016,
  "in_reply_to_status_id" : 500446970758971392,
  "created_at" : "2014-08-16 01:01:20 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500446564767109121",
  "geo" : { },
  "id_str" : "500446666416062466",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u3080\u3057\u308D\u308C\u3044\u3093\u3055\u309325\u6B73\u306A\u3093\u3067\u3059\u304B",
  "id" : 500446666416062466,
  "in_reply_to_status_id" : 500446564767109121,
  "created_at" : "2014-08-16 00:59:17 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500440256613588992",
  "text" : "\u3082\u3063\u3068\u9577\u304F\u5BDD\u3066\u308B\u3064\u3082\u308A\u304C\u5B85\u6025\u4FBF\u306B\u8D77\u3053\u3055\u308C\u307E\u3057\u305F\u304A\u306F\u3088\u3046\u3054\u3056\u3044\u307E\u3059",
  "id" : 500440256613588992,
  "created_at" : "2014-08-16 00:33:49 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30D0\u30B8\u30EA\u30B9\u30AF\u30BF\u30A4\u30E0",
      "screen_name" : "696vo",
      "indices" : [ 0, 6 ],
      "id_str" : "59455376",
      "id" : 59455376
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500287728626331648",
  "geo" : { },
  "id_str" : "500287861833203712",
  "in_reply_to_user_id" : 59455376,
  "text" : "@696vo \u653E\u7F6E\u3059\u308B\u3068\u524A\u308B\u3060\u3051\u3067\u3059\u307E\u306A\u3044\u3053\u3068\u306B\u3002\u3002",
  "id" : 500287861833203712,
  "in_reply_to_status_id" : 500287728626331648,
  "created_at" : "2014-08-15 14:28:15 +0000",
  "in_reply_to_screen_name" : "696vo",
  "in_reply_to_user_id_str" : "59455376",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3064\u3070\u3061",
      "screen_name" : "28chx398",
      "indices" : [ 0, 9 ],
      "id_str" : "242940963",
      "id" : 242940963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500287427320102913",
  "geo" : { },
  "id_str" : "500287588440100865",
  "in_reply_to_user_id" : 242940963,
  "text" : "@28chx398 \u305D\u306E\u3053\u3068\u3067\u3044\u3058\u3081\u3089\u308C\u3066\u307C\u3063\u3061\u3081\u3057\u3092\u98DF\u3079\u308B\u3088\u3046\u306B\u306A\u3063\u305F",
  "id" : 500287588440100865,
  "in_reply_to_status_id" : 500287427320102913,
  "created_at" : "2014-08-15 14:27:10 +0000",
  "in_reply_to_screen_name" : "28chx398",
  "in_reply_to_user_id_str" : "242940963",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30D0\u30B8\u30EA\u30B9\u30AF\u30BF\u30A4\u30E0",
      "screen_name" : "696vo",
      "indices" : [ 0, 6 ],
      "id_str" : "59455376",
      "id" : 59455376
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500287392410898432",
  "geo" : { },
  "id_str" : "500287513915703297",
  "in_reply_to_user_id" : 59455376,
  "text" : "@696vo \u3059\u3050\u6B6F\u533B\u8005\uFF01",
  "id" : 500287513915703297,
  "in_reply_to_status_id" : 500287392410898432,
  "created_at" : "2014-08-15 14:26:52 +0000",
  "in_reply_to_screen_name" : "696vo",
  "in_reply_to_user_id_str" : "59455376",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3064\u3070\u3061",
      "screen_name" : "28chx398",
      "indices" : [ 0, 9 ],
      "id_str" : "242940963",
      "id" : 242940963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500287133479739393",
  "geo" : { },
  "id_str" : "500287309342715905",
  "in_reply_to_user_id" : 242940963,
  "text" : "@28chx398 \u5973\u5B50\u30C8\u30A4\u30EC\u304C\u6DF7\u3093\u3067\u305F\u3051\u3069\u3069\u3046\u3057\u3066\u3082\u6211\u6162\u3067\u304D\u306A\u304F\u3066\u6065\u305A\u304B\u3057\u3044\u3051\u3069\u7537\u5B50\u30C8\u30A4\u30EC\u306B\u5165\u3063\u305F\u5E7C\u5973\u3068\u304B\u304B\u3082",
  "id" : 500287309342715905,
  "in_reply_to_status_id" : 500287133479739393,
  "created_at" : "2014-08-15 14:26:04 +0000",
  "in_reply_to_screen_name" : "28chx398",
  "in_reply_to_user_id_str" : "242940963",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30D0\u30B8\u30EA\u30B9\u30AF\u30BF\u30A4\u30E0",
      "screen_name" : "696vo",
      "indices" : [ 0, 6 ],
      "id_str" : "59455376",
      "id" : 59455376
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500286764414562306",
  "geo" : { },
  "id_str" : "500287106279673856",
  "in_reply_to_user_id" : 59455376,
  "text" : "@696vo \u305D\u308C\u866B\u6B6F\u304C\u9032\u884C\u3059\u308B\u3084\u3064\u30FB\u30FB\u30FB",
  "id" : 500287106279673856,
  "in_reply_to_status_id" : 500286764414562306,
  "created_at" : "2014-08-15 14:25:15 +0000",
  "in_reply_to_screen_name" : "696vo",
  "in_reply_to_user_id_str" : "59455376",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3064\u3070\u3061",
      "screen_name" : "28chx398",
      "indices" : [ 0, 9 ],
      "id_str" : "242940963",
      "id" : 242940963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500286785126006784",
  "geo" : { },
  "id_str" : "500286833792540673",
  "in_reply_to_user_id" : 242940963,
  "text" : "@28chx398 \u51FA\u4F1A\u3044\u306F\u30C8\u30A4\u30EC\u304B\u30FB\u30FB\u30FB",
  "id" : 500286833792540673,
  "in_reply_to_status_id" : 500286785126006784,
  "created_at" : "2014-08-15 14:24:10 +0000",
  "in_reply_to_screen_name" : "28chx398",
  "in_reply_to_user_id_str" : "242940963",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3064\u3070\u3061",
      "screen_name" : "28chx398",
      "indices" : [ 0, 9 ],
      "id_str" : "242940963",
      "id" : 242940963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500285919920783360",
  "geo" : { },
  "id_str" : "500286460142972928",
  "in_reply_to_user_id" : 242940963,
  "text" : "@28chx398 \u307C\u3063\u3061\u3081\u3057\u4E2D\u306E\u5A5A\u7D04\u8005\u3063\u3066\u3061\u3087\u3063\u3068\u304B\u308F\u3044\u305D\u3046\u3060",
  "id" : 500286460142972928,
  "in_reply_to_status_id" : 500285919920783360,
  "created_at" : "2014-08-15 14:22:41 +0000",
  "in_reply_to_screen_name" : "28chx398",
  "in_reply_to_user_id_str" : "242940963",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3064\u3070\u3061",
      "screen_name" : "28chx398",
      "indices" : [ 0, 9 ],
      "id_str" : "242940963",
      "id" : 242940963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500285581847314434",
  "geo" : { },
  "id_str" : "500285764815421440",
  "in_reply_to_user_id" : 242940963,
  "text" : "@28chx398 \u307C\u3063\u3061\u3081\u3057\u3055\u3093\u304B\u3089\u3082\u30EA\u30D7\u30E9\u30A4\u304D\u3066\u3001\u4ECA\u3053\u306ETL\u304C\u30A2\u30C4\u30A4",
  "id" : 500285764815421440,
  "in_reply_to_status_id" : 500285581847314434,
  "created_at" : "2014-08-15 14:19:55 +0000",
  "in_reply_to_screen_name" : "28chx398",
  "in_reply_to_user_id_str" : "242940963",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3064\u3070\u3061",
      "screen_name" : "28chx398",
      "indices" : [ 0, 9 ],
      "id_str" : "242940963",
      "id" : 242940963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500282823572344834",
  "geo" : { },
  "id_str" : "500285373059039232",
  "in_reply_to_user_id" : 242940963,
  "text" : "@28chx398 \u5A5A\u7D04\u8005 replied to one of your Tweets!\u3063\u3066\u30E1\u30FC\u30EB\u304C\u304D\u3066\u3068\u3066\u3082\u9762\u767D\u3044\u3067\u3059",
  "id" : 500285373059039232,
  "in_reply_to_status_id" : 500282823572344834,
  "created_at" : "2014-08-15 14:18:22 +0000",
  "in_reply_to_screen_name" : "28chx398",
  "in_reply_to_user_id_str" : "242940963",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3064\u3070\u3061",
      "screen_name" : "28chx398",
      "indices" : [ 0, 9 ],
      "id_str" : "242940963",
      "id" : 242940963
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500282514494078976",
  "geo" : { },
  "id_str" : "500282610119999488",
  "in_reply_to_user_id" : 242940963,
  "text" : "@28chx398 \u3054\u5A5A\u7D04\u304A\u3081\u3067\u3068\u3046\u3054\u3056\u3044\u307E\u3059\uFF01\uFF01\uFF01\uFF01",
  "id" : 500282610119999488,
  "in_reply_to_status_id" : 500282514494078976,
  "created_at" : "2014-08-15 14:07:23 +0000",
  "in_reply_to_screen_name" : "28chx398",
  "in_reply_to_user_id_str" : "242940963",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500270366187470848",
  "text" : "\u30B7\u30DE\u30EA\u30B9\u304B\u308F\u3044\u3044\u306E\u306B\u306A\u3041\uFF08\u30AD\u30FC\u30DC\u30FC\u30C9\u306E\u5468\u308A\u3046\u308D\u3064\u3044\u3066\u90AA\u9B54\uFF09",
  "id" : 500270366187470848,
  "created_at" : "2014-08-15 13:18:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500270114629885952",
  "text" : "\u7D14\u7C8B\u306B\u591C\u306E\u307B\u3046\u304C\u30B7\u30DE\u30EA\u30B9\u69CB\u3048\u308B\u304B\u3089\u3063\u3066\u8A00\u3063\u305F\u306E\u306BTwitter\u306E\u307F\u3093\u306A\u306B\u300C\u5973\u3092\u9023\u308C\u8FBC\u3080\u307F\u305F\u3044\u300D\u3068\u304B\u8A00\u308F\u308C\u305F\u306E\u3067\u3082\u3046\u7D76\u5BFE\u306B\u30B7\u30DE\u30EA\u30B9\u306E\u5199\u771F\u3092Twitter\u306B\u8F09\u305B\u3066\u3084\u3089\u306A\u3044",
  "id" : 500270114629885952,
  "created_at" : "2014-08-15 13:17:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30CF\u30EB",
      "screen_name" : "_haru_",
      "indices" : [ 0, 7 ],
      "id_str" : "15616888",
      "id" : 15616888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500269420879421440",
  "geo" : { },
  "id_str" : "500269723125174272",
  "in_reply_to_user_id" : 15616888,
  "text" : "@_haru_ \u30B7\u30DE\u30EA\u30B9\u3092\u6C5A\u3059\u3088\u3046\u306A\u3053\u3068\u3092\u3057\u3066\u306F\u99C4\u76EE\u3060\u3001\u30B7\u30DE\u30EA\u30B9\u306F\u3082\u3063\u3068\u3053\u3046\u7D14\u7C8B\u306A\u3093\u3060",
  "id" : 500269723125174272,
  "in_reply_to_status_id" : 500269420879421440,
  "created_at" : "2014-08-15 13:16:11 +0000",
  "in_reply_to_screen_name" : "_haru_",
  "in_reply_to_user_id_str" : "15616888",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3042\u305A\u306E\u3082\u3051",
      "screen_name" : "AzMoke",
      "indices" : [ 0, 7 ],
      "id_str" : "619653810",
      "id" : 619653810
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500269209096450049",
  "geo" : { },
  "id_str" : "500269370648432640",
  "in_reply_to_user_id" : 619653810,
  "text" : "@AzMoke \u3053\u3046\u3044\u3046\u30B3\u30B9\u30D7\u30EC\u3063\u3066\u6BCE\u65E5\u304A\u306A\u3058\u3082\u306E\u306A\u3093\u3067\u3059\u304B\u306D\u30FB\u30FB\u30FB\uFF1F",
  "id" : 500269370648432640,
  "in_reply_to_status_id" : 500269209096450049,
  "created_at" : "2014-08-15 13:14:47 +0000",
  "in_reply_to_screen_name" : "AzMoke",
  "in_reply_to_user_id_str" : "619653810",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500268969236758528",
  "text" : "\u8D77\u304D\u304C\u3051\u306E\u30B7\u30DE\u30EA\u30B9\u306F\u3075\u306B\u3075\u306B\u3057\u653E\u984C\u306A\u306E\u3067\u6765\u308B\u306A\u3089\u591C\u3060\u3068\u3044\u3044\u3088\uFF01",
  "id" : 500268969236758528,
  "created_at" : "2014-08-15 13:13:11 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "RDA\u304A\u3070\u3055\u3093\u307F\u306A\u305B",
      "screen_name" : "swcc_mns",
      "indices" : [ 0, 9 ],
      "id_str" : "155167087",
      "id" : 155167087
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "500268496404484098",
  "geo" : { },
  "id_str" : "500268860780453888",
  "in_reply_to_user_id" : 155167087,
  "text" : "@swcc_mns \u3055\u3063\u304D\u3082\u3075\u306B\u3075\u306B\u3057\u3066\u307E\u3057\u305F",
  "id" : 500268860780453888,
  "in_reply_to_status_id" : 500268496404484098,
  "created_at" : "2014-08-15 13:12:45 +0000",
  "in_reply_to_screen_name" : "swcc_mns",
  "in_reply_to_user_id_str" : "155167087",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/500268089296973825\/photo\/1",
      "indices" : [ 105, 127 ],
      "url" : "http:\/\/t.co\/Q0gnjkX1Om",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BvFPLSxCEAE3DX8.jpg",
      "id_str" : "500268089049485313",
      "id" : 500268089049485313,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BvFPLSxCEAE3DX8.jpg",
      "sizes" : [ {
        "h" : 604,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1066,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 1820,
        "resize" : "fit",
        "w" : 1024
      } ],
      "display_url" : "pic.twitter.com\/Q0gnjkX1Om"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500268089296973825",
  "text" : "\u300C\u3055\u3044\u304D\u3093\u30B7\u30DE\u30EA\u30B9\u306E\u5199\u771F\u306E\u305B\u306A\u3044\u306E\u30FC\uFF1F\u300D\u3068\u304B\u8A00\u308F\u308C\u305F\u3051\u3069\u3001\u3044\u3064\u3082\u64AE\u3063\u3066\u3082\u3053\u3093\u306A\u611F\u3058\u3067\u3042\u3052\u308C\u308B\u3050\u3089\u3044\u306E\u8CEA\u306A\u306E\u306F\u7A00\u3060\u3057\u307B\u3093\u3068\u306F\u3082\u3063\u3068\u3060\u3089\u3051\u3066\u308B\u306E\u3068\u304B\u3042\u308B\u3051\u3069\u4EBA\u304C\u5BC4\u308B\u3068\u8D77\u304D\u3061\u3083\u3046\u3057\u3060\u304B\u3089\u307F\u3093\u306A\u76F4\u306B\u898B\u306B\u6765\u308B\u3068\u3044\u3044\u3088\uFF01 http:\/\/t.co\/Q0gnjkX1Om",
  "id" : 500268089296973825,
  "created_at" : "2014-08-15 13:09:41 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500251550095130625",
  "text" : "\u3042\u3093\u307E\u308A\u7F8E\u5473\u3057\u304F\u306A\u304B\u3063\u305F\u304B\u3089\u3001\u307E\u305F\u3042\u306E\u30CF\u30F3\u30D0\u30FC\u30B0\u98DF\u3079\u305F\u3044",
  "id" : 500251550095130625,
  "created_at" : "2014-08-15 12:03:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/f6S9z6leRK",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53ede638498e04fd3e48142e?s=wn4VMK1PRvPLPFbpdCguJN4TE1M&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "500233338570149888",
  "text" : "\u306B\u304F\uFF01\uFF01 (@ \u30D9\u30EA\u30FC\u30B0\u30C3\u30C9\u30DE\u30F3 \u6A2A\u6D5C\u5E97 in \u6A2A\u6D5C\u5E02, \u795E\u5948\u5DDD\u770C) https:\/\/t.co\/f6S9z6leRK",
  "id" : 500233338570149888,
  "created_at" : "2014-08-15 10:51:36 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500132469341843457",
  "text" : "\u9AEA\u3092\u5207\u3063\u3066\u304B\u3089\u6C57\u304C\u3060\u3089\u3060\u3089\u6EF4\u3063\u3066\u304F\u308B\u3088\u3046\u306B\u306A\u3063\u305F\u3002\u6C57\u304B\u304D\u306B\u306A\u3063\u305F\u3001\u3068\u3044\u3046\u3088\u308A\u3001\u982D\u306E\u4FDD\u6C57\u80FD\u529B\u304C\u4F4E\u4E0B\u3057\u305F\u306E\u304C\u539F\u56E0\uFF1F",
  "id" : 500132469341843457,
  "created_at" : "2014-08-15 04:10:47 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500124287697571840",
  "text" : "\u30DD\u30F3\u30B8\u30E5\u30FC\u30B9\u30CE\u98F2\u3093\u3067\u3057\u3083\u3063\u304D\u308A\u3057\u305F",
  "id" : 500124287697571840,
  "created_at" : "2014-08-15 03:38:16 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "500123390829555712",
  "text" : "\u306A\u3093\u3060\u3053\u306E\u4EBA\u306E\u591A\u3055\u3068\u601D\u3063\u305F\u3089\u30B3\u30DF\u30B1\u304B",
  "id" : 500123390829555712,
  "created_at" : "2014-08-15 03:34:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499995638126219264",
  "text" : "\u3042\u304B\u3093\u3001\u305C\u3093\u305C\u3093\u306D\u308C\u3093",
  "id" : 499995638126219264,
  "created_at" : "2014-08-14 19:07:04 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499928752273383426",
  "text" : "Nagoya\u3092\u8AAC\u660E\u3059\u308B\u6642\u306Fthe 3rd biggest city, which is between Tokyo and Osaka\u3067\u901A\u3058\u308B\u3002Toyota\u3068\u304B\u8A00\u3063\u3066\u3082\u540D\u524D\u306F\u77E5\u3063\u3066\u308B\u3051\u3069\u3069\u3053\u304B\u77E5\u3089\u306A\u3044\u3063\u3066\u9854\u3055\u308C\u308B\u3002",
  "id" : 499928752273383426,
  "created_at" : "2014-08-14 14:41:17 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499927503192850432",
  "text" : "\u5E38\u306B\u6E6F\u714E\u3057\u3066\u304A\u3051\u3070\u3044\u3044\u306E\u3067\u306F",
  "id" : 499927503192850432,
  "created_at" : "2014-08-14 14:36:19 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u10E0\u10DD\u10D1\u10D4\u10E0\u10E2\u10D0\u10E1\u10D9 ",
      "screen_name" : "robertask",
      "indices" : [ 0, 10 ],
      "id_str" : "86671709",
      "id" : 86671709
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499920018386063361",
  "geo" : { },
  "id_str" : "499925006067834881",
  "in_reply_to_user_id" : 86671709,
  "text" : "@robertask \u3069\u3063\u3061\u304B\u3068\u3044\u3046\u3068\u4F0A\u52E2\u6E7E\u53F0\u98A8\u306E\u60B2\u60E8\u3055\u3092\u4F1D\u3048\u308B\u305F\u3081\uFF1F",
  "id" : 499925006067834881,
  "in_reply_to_status_id" : 499920018386063361,
  "created_at" : "2014-08-14 14:26:24 +0000",
  "in_reply_to_screen_name" : "robertask",
  "in_reply_to_user_id_str" : "86671709",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499919165562449921",
  "geo" : { },
  "id_str" : "499924942108905472",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u3076\u3089\u3063\u304F\u3058\u3087\u30FC\u304F",
  "id" : 499924942108905472,
  "in_reply_to_status_id" : 499919165562449921,
  "created_at" : "2014-08-14 14:26:09 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499918005795450880",
  "geo" : { },
  "id_str" : "499918291792441344",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u3053\u308F\u30FB\u30FB\u30FB",
  "id" : 499918291792441344,
  "in_reply_to_status_id" : 499918005795450880,
  "created_at" : "2014-08-14 13:59:43 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499918274595811328",
  "text" : "\u5BDD\u3088\u3046\u3068\u601D\u3063\u305F\u3051\u3069\u306D\u308C\u306A\u304B\u3063\u305F",
  "id" : 499918274595811328,
  "created_at" : "2014-08-14 13:59:39 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u10E0\u10DD\u10D1\u10D4\u10E0\u10E2\u10D0\u10E1\u10D9 ",
      "screen_name" : "robertask",
      "indices" : [ 0, 10 ],
      "id_str" : "86671709",
      "id" : 86671709
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499910078435033089",
  "geo" : { },
  "id_str" : "499912266808954880",
  "in_reply_to_user_id" : 86671709,
  "text" : "@robertask \u6559\u80B2\u7528\u306A\u306E\u3067\u30FB\u30FB\u30FB",
  "id" : 499912266808954880,
  "in_reply_to_status_id" : 499910078435033089,
  "created_at" : "2014-08-14 13:35:46 +0000",
  "in_reply_to_screen_name" : "robertask",
  "in_reply_to_user_id_str" : "86671709",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u10E0\u10DD\u10D1\u10D4\u10E0\u10E2\u10D0\u10E1\u10D9 ",
      "screen_name" : "robertask",
      "indices" : [ 0, 10 ],
      "id_str" : "86671709",
      "id" : 86671709
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499908236594515969",
  "geo" : { },
  "id_str" : "499908421244555265",
  "in_reply_to_user_id" : 86671709,
  "text" : "@robertask \u5C0F\u5B66\u6821\u3067\u898B\u305F\u4F0A\u52E2\u6E7E\u53F0\u98A8\u306E\u30A2\u30CB\u30E1\uFF08\uFF1F\uFF09\u3067\u3001\u5973\u306E\u5B50\u306E\u80CC\u4E2D\u306B\u30C7\u30AB\u30A4\u4E38\u592A\u304C\u8FFD\u7A81\u3059\u308B\u5834\u9762\u304C\u885D\u6483\u7684\u3067\u3057\u305F\u30FB\u30FB\u30FB",
  "id" : 499908421244555265,
  "in_reply_to_status_id" : 499908236594515969,
  "created_at" : "2014-08-14 13:20:30 +0000",
  "in_reply_to_screen_name" : "robertask",
  "in_reply_to_user_id_str" : "86671709",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hide Tom",
      "screen_name" : "J_Searobin",
      "indices" : [ 0, 11 ],
      "id_str" : "98135281",
      "id" : 98135281
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499902037841039360",
  "geo" : { },
  "id_str" : "499902514372673536",
  "in_reply_to_user_id" : 98135281,
  "text" : "@J_Searobin \u708A\u98EF\u5668\u306B\u306F\u307E\u3060\u3054\u98EF\u3042\u308B\uFF06\u304A\u7C73\u4EE5\u5916\u5165\u308C\u308B\u3068\u6D17\u3046\u306E\u5927\u5909\u306A\u306E\u3067\u30FB\u30FB\u30FB",
  "id" : 499902514372673536,
  "in_reply_to_status_id" : 499902037841039360,
  "created_at" : "2014-08-14 12:57:01 +0000",
  "in_reply_to_screen_name" : "J_Searobin",
  "in_reply_to_user_id_str" : "98135281",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499902005058355200",
  "geo" : { },
  "id_str" : "499902315172597761",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u78BA\u304B\u306B\uFF01",
  "id" : 499902315172597761,
  "in_reply_to_status_id" : 499902005058355200,
  "created_at" : "2014-08-14 12:56:14 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Hide Tom",
      "screen_name" : "J_Searobin",
      "indices" : [ 0, 11 ],
      "id_str" : "98135281",
      "id" : 98135281
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499901571467993088",
  "geo" : { },
  "id_str" : "499901809461170178",
  "in_reply_to_user_id" : 98135281,
  "text" : "@J_Searobin \u3054\u98EF\u307B\u3057\u3044\u306E\u3067\u30FB\u30FB\u30FB",
  "id" : 499901809461170178,
  "in_reply_to_status_id" : 499901571467993088,
  "created_at" : "2014-08-14 12:54:13 +0000",
  "in_reply_to_screen_name" : "J_Searobin",
  "in_reply_to_user_id_str" : "98135281",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499901339338436608",
  "geo" : { },
  "id_str" : "499901774430367745",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u5DDD\u306B\u6301\u3063\u3066\u3044\u3051\u3070\u306A\u304F\u306A\u308B\u306E\u3067\u306F",
  "id" : 499901774430367745,
  "in_reply_to_status_id" : 499901339338436608,
  "created_at" : "2014-08-14 12:54:05 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499901424931573761",
  "text" : "\u3042\u3068\u30011\u4EBA\u3067\u3054\u98EF\u3060\u3068\u3001\uFF08\u6D17\u3044\u7269\u304C\uFF09\u3081\u3093\u3069\u304F\u3055\u3044\u306E\u3067\u3054\u98EF\u306E\u4E0A\u306B\u304A\u304B\u305A\u306E\u3063\u3051\u3061\u3083\u3044\u307E\u3059\u306D\u3002",
  "id" : 499901424931573761,
  "created_at" : "2014-08-14 12:52:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u767D",
      "screen_name" : "parodyer",
      "indices" : [ 0, 9 ],
      "id_str" : "82635103",
      "id" : 82635103
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499901069141348353",
  "geo" : { },
  "id_str" : "499901143846121472",
  "in_reply_to_user_id" : 82635103,
  "text" : "@parodyer \u3067\u3082\u91CE\u6027\u5473\u306F\u91CE\u6027\u5473\u3067\u304A\u3044\u3057\u3044\u3067\u3059\u3088",
  "id" : 499901143846121472,
  "in_reply_to_status_id" : 499901069141348353,
  "created_at" : "2014-08-14 12:51:35 +0000",
  "in_reply_to_screen_name" : "parodyer",
  "in_reply_to_user_id_str" : "82635103",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499899463524052992",
  "geo" : { },
  "id_str" : "499901061339959297",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u713C\u914E\u30921\u5347\u3082\u306B\u3083\u3093\u3053\u3055\u3093\u98F2\u3081\u308B\u3093\u3067\u3057\u305F\u3063\u3051\uFF1F",
  "id" : 499901061339959297,
  "in_reply_to_status_id" : 499899463524052992,
  "created_at" : "2014-08-14 12:51:15 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499897486387863552",
  "geo" : { },
  "id_str" : "499899067007115266",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u305D\u306E\u713C\u914E\u306F\u8AB0\u304B\u3078\u306E\u30D7\u30EC\u30BC\u30F3\u30C8\u3067\u3059\u306D\uFF1F",
  "id" : 499899067007115266,
  "in_reply_to_status_id" : 499897486387863552,
  "created_at" : "2014-08-14 12:43:19 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499894383940874240",
  "text" : "MinGW\u3088\u308Acygwin\u6D3E\u3060\u3057\u3001cygwin\u3088\u308A\u4EEE\u60F3\u5316\u6D3E\u3060\u3057\u3001\u4EEE\u60F3\u5316\u3059\u308B\u3050\u3089\u3044\u306F\u3089\u5B9F\u6A5F\u52D5\u304B\u3059\u6D3E\u3067\u3059",
  "id" : 499894383940874240,
  "created_at" : "2014-08-14 12:24:43 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u6620\u753B\u306E\u306A\u304B",
      "screen_name" : "Vengineer",
      "indices" : [ 0, 10 ],
      "id_str" : "61330281",
      "id" : 61330281
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499893247603265536",
  "geo" : { },
  "id_str" : "499893422480568320",
  "in_reply_to_user_id" : 61330281,
  "text" : "@Vengineer \u307E\u3041\u73FE\u5B9F\u7684\u306B\u306F\u6700\u521D\u304B\u3089\u5168\u3066\u306F\u51FA\u3057\u5207\u308C\u306A\u3044\u3057\u751F\u304D\u3066\u3044\u308B\u3046\u3061\u306B\u5909\u308F\u308B\u3053\u3068\u3082\u3042\u308B\u3067\u3057\u3087\u3046\u3057\u306D",
  "id" : 499893422480568320,
  "in_reply_to_status_id" : 499893247603265536,
  "created_at" : "2014-08-14 12:20:54 +0000",
  "in_reply_to_screen_name" : "Vengineer",
  "in_reply_to_user_id_str" : "61330281",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499893261478006784",
  "text" : "\u3054\u306F\u3093\u304C\u30FC\u3054\u306F\u3093\u304C\u30FC\u305F\u30FC\u3051\u307E\u30FC\u3057\u305F\u30FC",
  "id" : 499893261478006784,
  "created_at" : "2014-08-14 12:20:15 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499892848234209280",
  "text" : "\u6700\u8FD1\u30CD\u30C8\u30B2\u3092\u59CB\u3081\u3066\u3057\u307E\u3063\u305F\u306E\u3067\u6642\u9593\u6D88\u8CBB\u304C\u306F\u3052\u3057\u3044",
  "id" : 499892848234209280,
  "created_at" : "2014-08-14 12:18:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u304B\u305F\u305D",
      "screen_name" : "yucataso",
      "indices" : [ 0, 9 ],
      "id_str" : "67385537",
      "id" : 67385537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499891664895234048",
  "geo" : { },
  "id_str" : "499892214915272705",
  "in_reply_to_user_id" : 67385537,
  "text" : "@yucataso \u3042\u30FC\u3001\u6700\u521D\u304B\u3089\u304A\u4E92\u3044\u7D50\u5A5A\u524D\u63D0\u3067\u3001\u304B\u3064\u6253\u7B97\u7684\u306B\u76F8\u624B\u3092\u898B\u308B\u3053\u3068\u304C\u3067\u304D\u308B\u3063\u3066\u306E\u306F\u78BA\u304B\u306B\u305D\u3046\u3067\u3059\u306D\u30FC\u3002\u604B\u611B\u3060\u3068\u300C\u604B\u300D\u304C\u5165\u3063\u3066\u305D\u306E\u8FBA\u51B7\u9759\u306B\u898B\u3048\u3066\u306A\u3044\u53EF\u80FD\u6027\u3042\u308A\u307E\u3059\u3057\u3002",
  "id" : 499892214915272705,
  "in_reply_to_status_id" : 499891664895234048,
  "created_at" : "2014-08-14 12:16:06 +0000",
  "in_reply_to_screen_name" : "yucataso",
  "in_reply_to_user_id_str" : "67385537",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499891358685880320",
  "text" : "\u53CB\u9054\u3082\u604B\u4EBA\u3082\u306D",
  "id" : 499891358685880320,
  "created_at" : "2014-08-14 12:12:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499891213781057539",
  "text" : "\u3067\u304D\u306A\u3044\u3053\u3068\u306F\u3067\u304D\u306A\u3044\u3057\u3001\u3057\u305F\u304F\u306A\u3044\u3053\u3068\u306F\u3057\u305F\u304F\u306A\u3044\u306E\u3067\u3001\u305D\u306E\u3042\u305F\u308A\u304C\u304A\u4E92\u3044\u306B\u8A31\u5BB9\u3067\u304D\u308B\u76F8\u624B\u3068\u4E00\u7DD2\u306B\u3044\u308B\u306E\u304C\u3044\u3044\u3068\u601D\u3044\u307E\u3059\u3002",
  "id" : 499891213781057539,
  "created_at" : "2014-08-14 12:12:07 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u304B\u305F\u305D",
      "screen_name" : "yucataso",
      "indices" : [ 0, 9 ],
      "id_str" : "67385537",
      "id" : 67385537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499890725576667137",
  "geo" : { },
  "id_str" : "499890923275165696",
  "in_reply_to_user_id" : 67385537,
  "text" : "@yucataso \u306A\u308B\u307B\u3069\u3001\u305D\u308C\u591A\u5206\u3044\u3061\u3070\u3093\u5927\u4E8B\u3060\u3068\u601D\u3044\u307E\u3059\uFF01",
  "id" : 499890923275165696,
  "in_reply_to_status_id" : 499890725576667137,
  "created_at" : "2014-08-14 12:10:58 +0000",
  "in_reply_to_screen_name" : "yucataso",
  "in_reply_to_user_id_str" : "67385537",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u304B\u305F\u305D",
      "screen_name" : "yucataso",
      "indices" : [ 0, 9 ],
      "id_str" : "67385537",
      "id" : 67385537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499889845771046912",
  "geo" : { },
  "id_str" : "499890264362606592",
  "in_reply_to_user_id" : 67385537,
  "text" : "@yucataso \u300C\u6599\u7406\u4F5C\u308B\u306E\u597D\u304D\u3060\u304B\u3089\u3054\u98EF\u306F\u4F5C\u308B\u3088\u30FC\u300D\u3068\u300C\u6383\u9664\u597D\u304D\u3060\u304B\u3089\u6383\u9664\u306F\u4EFB\u3057\u3066\uFF01\u300D\u306E\u30DE\u30C3\u30C1\u30F3\u30B0\uFF1F",
  "id" : 499890264362606592,
  "in_reply_to_status_id" : 499889845771046912,
  "created_at" : "2014-08-14 12:08:21 +0000",
  "in_reply_to_screen_name" : "yucataso",
  "in_reply_to_user_id_str" : "67385537",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u304B\u305F\u305D",
      "screen_name" : "yucataso",
      "indices" : [ 0, 9 ],
      "id_str" : "67385537",
      "id" : 67385537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499889341695397888",
  "geo" : { },
  "id_str" : "499889605328392194",
  "in_reply_to_user_id" : 67385537,
  "text" : "@yucataso \u56F0\u3063\u305F\u3068\u304D\u306B\u52A9\u3051\u305F\u308A\u52A9\u3051\u3066\u3082\u3089\u3048\u305F\u308A\u3059\u308B\u4EBA\u3068\u304B\u3067\u3059\uFF1F",
  "id" : 499889605328392194,
  "in_reply_to_status_id" : 499889341695397888,
  "created_at" : "2014-08-14 12:05:44 +0000",
  "in_reply_to_screen_name" : "yucataso",
  "in_reply_to_user_id_str" : "67385537",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u304B\u305F\u305D",
      "screen_name" : "yucataso",
      "indices" : [ 0, 9 ],
      "id_str" : "67385537",
      "id" : 67385537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499888655528235008",
  "geo" : { },
  "id_str" : "499889211047034881",
  "in_reply_to_user_id" : 67385537,
  "text" : "@yucataso \u5408\u7406\u7684\u306A\u7D50\u5A5A\u3063\u3066\u306A\u3093\u304B\u6A5F\u68B0\u3063\u307D\u3044\u30FB\u30FB\u30FB",
  "id" : 499889211047034881,
  "in_reply_to_status_id" : 499888655528235008,
  "created_at" : "2014-08-14 12:04:10 +0000",
  "in_reply_to_screen_name" : "yucataso",
  "in_reply_to_user_id_str" : "67385537",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u666E\u901A\u3058\u3083\u306A\u3044\u95A2\u4FC2\u306E\u7537\u53CB\u9054",
      "indices" : [ 34, 47 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499889049943826432",
  "text" : "\u6BCD\u89AA\u304C\u518D\u5A5A\u3057\u305F\u76F8\u624B\u304C\u5B9F\u306F\u30AF\u30E9\u30B9\u3067\u4E00\u7DD2\u306B\u5B66\u7D1A\u59D4\u54E1\u3084\u3063\u3066\u305F\u7537\u5B50\u3060\u3063\u305F\uFF1F #\u666E\u901A\u3058\u3083\u306A\u3044\u95A2\u4FC2\u306E\u7537\u53CB\u9054",
  "id" : 499889049943826432,
  "created_at" : "2014-08-14 12:03:31 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "\u3068\u306F",
      "indices" : [ 13, 16 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499888525223817216",
  "text" : "\u666E\u901A\u3058\u3083\u306A\u3044\u95A2\u4FC2\u306E\u7537\u53CB\u9054 #\u3068\u306F",
  "id" : 499888525223817216,
  "created_at" : "2014-08-14 12:01:26 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u304B\u305F\u305D",
      "screen_name" : "yucataso",
      "indices" : [ 0, 9 ],
      "id_str" : "67385537",
      "id" : 67385537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499887786741096450",
  "geo" : { },
  "id_str" : "499887965531680768",
  "in_reply_to_user_id" : 67385537,
  "text" : "@yucataso \u3055\u3093\u3001\u304A\u898B\u5408\u3044\u3068\u304B\u5408\u308F\u306A\u3055\u305D\u3046\u30FB\u30FB\u30FB",
  "id" : 499887965531680768,
  "in_reply_to_status_id" : 499887786741096450,
  "created_at" : "2014-08-14 11:59:13 +0000",
  "in_reply_to_screen_name" : "yucataso",
  "in_reply_to_user_id_str" : "67385537",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499886366323257345",
  "geo" : { },
  "id_str" : "499886449974452226",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u6B21\u56DE\u304B\u3089\u307E\u305F\u5927\u5909\u306A\u76EE\u306B\u5408\u3046\u3084\u3064\u3067\u3059",
  "id" : 499886449974452226,
  "in_reply_to_status_id" : 499886366323257345,
  "created_at" : "2014-08-14 11:53:11 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499886106704244736",
  "geo" : { },
  "id_str" : "499886223771459586",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u30A2\u30C9\u30D9\u30F3\u30C1\u30E3\u30FC\u3082\u306E\u306E\u30A2\u30CB\u30E1\u306E\u6C34\u7740\u56DE\u7684\u306A\u3082\u306E\u3067\u3059\u306D",
  "id" : 499886223771459586,
  "in_reply_to_status_id" : 499886106704244736,
  "created_at" : "2014-08-14 11:52:17 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499885825618759680",
  "geo" : { },
  "id_str" : "499885965226160128",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u6226\u3044\u306F\u4F55\u3082\u7523\u307E\u306A\u304B\u3063\u305F\u30FB\u30FB\u30FB",
  "id" : 499885965226160128,
  "in_reply_to_status_id" : 499885825618759680,
  "created_at" : "2014-08-14 11:51:16 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    }, {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 14, 25 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499885573650124801",
  "geo" : { },
  "id_str" : "499885751606054912",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight @rainbow410 \u304B\u304A\u308A\u306A\u3055\u3093\u306F\u3068\u3046\u8003\u3048\u3066\u3082\u666E\u901A\u3058\u3083\u306A\u3044\uFF08\u51B7\u9759\u306A\u610F\u898B\uFF09",
  "id" : 499885751606054912,
  "in_reply_to_status_id" : 499885573650124801,
  "created_at" : "2014-08-14 11:50:25 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499885403688538113",
  "geo" : { },
  "id_str" : "499885490149937153",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u3042\u306E\u5834\u306B\u3044\u305F\u4EBA\u306B\u3072\u3068\u308A\u3067\u3082\u666E\u901A\u304C\u3044\u305F\u3060\u308D\u3046\u304B\uFF01",
  "id" : 499885490149937153,
  "in_reply_to_status_id" : 499885403688538113,
  "created_at" : "2014-08-14 11:49:22 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499885214571569152",
  "text" : "\u666E\u901A\u306E\u4EBA\u306F\u81EA\u5206\u306E\u3053\u3068\u666E\u901A\u3063\u3066\u8A00\u308F\u306A\u3044\u304B\u3089\u308C\u3044\u3093\u3055\u3093\u306F\u666E\u901A\u3058\u3083\u306A\u3044",
  "id" : 499885214571569152,
  "created_at" : "2014-08-14 11:48:17 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E0D\u60D1\u306E\u72E9\u4EBA@\u9154\u3044\u3069\u308C\u8266\u968A",
      "screen_name" : "tosca_fuwaku",
      "indices" : [ 0, 13 ],
      "id_str" : "325487678",
      "id" : 325487678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499884065571348480",
  "geo" : { },
  "id_str" : "499884368060350465",
  "in_reply_to_user_id" : 325487678,
  "text" : "@tosca_fuwaku \u307E\u3041\u5B66\u554F\u3054\u3068\u306B\u3082\u9055\u3046\u3067\u3057\u3087\u3046\u3057\u4E00\u6982\u306B\u826F\u3057\u60AA\u3057\u306F\u8A00\u3048\u306A\u3044\u3067\u3059\u306D",
  "id" : 499884368060350465,
  "in_reply_to_status_id" : 499884065571348480,
  "created_at" : "2014-08-14 11:44:55 +0000",
  "in_reply_to_screen_name" : "tosca_fuwaku",
  "in_reply_to_user_id_str" : "325487678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499884092486193155",
  "text" : "\u672A\u77E5\u5909\u65704\u306B\u5F0F3\u3060\u304B\u3089\u539F\u7406\u7684\u306B\u89E3\u3051\u308B\u308F\u3051\u306A\u3044\u306E\u3060\u3051\u3069\u3001\u7BC4\u56F2\u306F\u3057\u307C\u308A\u3053\u3081\u308B\uFF1F",
  "id" : 499884092486193155,
  "created_at" : "2014-08-14 11:43:49 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499883811627225088",
  "text" : "\u4E00\u610F\u306B\u6C7A\u307E\u3089\u306A\u3055\u305D\u3046",
  "id" : 499883811627225088,
  "created_at" : "2014-08-14 11:42:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499883760309919744",
  "text" : "\u554F\u984C\uFF1A\u8EAB\u9577\u30FB\u4F53\u91CD\u30FBBMI\u306E\u5897\u5206\u304C\u4E0E\u3048\u3089\u308C\u305F\u6642\u3001\u305D\u306E\u8EAB\u9577\u3001\u4F53\u91CD\u3092\u7B54\u3048\u3088",
  "id" : 499883760309919744,
  "created_at" : "2014-08-14 11:42:30 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499883085966487554",
  "geo" : { },
  "id_str" : "499883138990882817",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3060\u304B\u3089\u5148\u306B\u30B1\u30FC\u30AD\u306A\u306E\u3060\u3088\uFF01",
  "id" : 499883138990882817,
  "in_reply_to_status_id" : 499883085966487554,
  "created_at" : "2014-08-14 11:40:02 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499882936015921153",
  "geo" : { },
  "id_str" : "499883086700486657",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u96E3\u3057\u3044\u554F\u984C\u3060\u30FB\u30FB\u30FB",
  "id" : 499883086700486657,
  "in_reply_to_status_id" : 499882936015921153,
  "created_at" : "2014-08-14 11:39:49 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499882752796155904",
  "geo" : { },
  "id_str" : "499883053796167681",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u9006\u306F\u304A\u8179\u3044\u3063\u3071\u3044\uFF57\u3002",
  "id" : 499883053796167681,
  "in_reply_to_status_id" : 499882752796155904,
  "created_at" : "2014-08-14 11:39:42 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499882982497198080",
  "text" : "\u5065\u5EB7\u8A3A\u65AD\u306E\u7D50\u679C\u304D\u3066\u305F\u3002\u8EAB\u9577\u304C8mm\u5897\u3048\u3066\u3001\u4F53\u91CD\u304C3.3kg\u5897\u3048\u3066\u305F\u3002BMI\u306F0.8\u4E0A\u304C\u3063\u3066\u305F\u3002\u826F\u3044\u50BE\u5411\u3060\u3002",
  "id" : 499882982497198080,
  "created_at" : "2014-08-14 11:39:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499882423576842241",
  "geo" : { },
  "id_str" : "499882706595885056",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u304A\u663C\u306B\u30B1\u30FC\u30AD\u98DF\u3079\u305F\u5F8C\u306B\u304A\u663C\u3054\u306F\u3093\u98DF\u3079\u308B\u305E\u5996\u602A\uFF1F",
  "id" : 499882706595885056,
  "in_reply_to_status_id" : 499882423576842241,
  "created_at" : "2014-08-14 11:38:19 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499881990120685568",
  "geo" : { },
  "id_str" : "499882600387715075",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u3058\u3083\u3042\u7C73\u7C89\u30D1\u30F3\u3068\u304B\u30FB\u30FB\u30FB",
  "id" : 499882600387715075,
  "in_reply_to_status_id" : 499881990120685568,
  "created_at" : "2014-08-14 11:37:53 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499881730812022784",
  "geo" : { },
  "id_str" : "499881904410075136",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u7C73\u98DF\u3079\u306A\u3044\u3068\u3063\u3066\u4EBA\u306F\u3001\u3082\u3061\u3067\u3082\u7C73\u98DF\u3079\u308B\u3093\u3067\u3059\u304B\u306D",
  "id" : 499881904410075136,
  "in_reply_to_status_id" : 499881730812022784,
  "created_at" : "2014-08-14 11:35:08 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E0D\u60D1\u306E\u72E9\u4EBA@\u9154\u3044\u3069\u308C\u8266\u968A",
      "screen_name" : "tosca_fuwaku",
      "indices" : [ 0, 13 ],
      "id_str" : "325487678",
      "id" : 325487678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499880588300410880",
  "geo" : { },
  "id_str" : "499881547432878082",
  "in_reply_to_user_id" : 325487678,
  "text" : "@tosca_fuwaku \u305D\u3046\u3067\u3059\u306D\u3002\u8AD6\u6587\u3067\u306FY\/N\u3067\u3059\u3002",
  "id" : 499881547432878082,
  "in_reply_to_status_id" : 499880588300410880,
  "created_at" : "2014-08-14 11:33:42 +0000",
  "in_reply_to_screen_name" : "tosca_fuwaku",
  "in_reply_to_user_id_str" : "325487678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 0, 13 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499878354682523648",
  "geo" : { },
  "id_str" : "499881467766251520",
  "in_reply_to_user_id" : 140781133,
  "text" : "@caori_knight \u3042\u30FC\u306A\u308B\u307B\u3069\u3002\u30E9\u30FC\u30E1\u30F3\u3060\u3081\u3067\u30C1\u30E3\u30FC\u30CF\u30F3\u826F\u3044\u3068\u304B\u3060\u3063\u305F\u3089\u305D\u3046\u3063\u307D\u3044\u3067\u3059\u306D\u3002",
  "id" : 499881467766251520,
  "in_reply_to_status_id" : 499878354682523648,
  "created_at" : "2014-08-14 11:33:23 +0000",
  "in_reply_to_screen_name" : "caori_knight",
  "in_reply_to_user_id_str" : "140781133",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499878282263658496",
  "geo" : { },
  "id_str" : "499881368449335297",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3053\u3061\u3089\u5074\u304B\u3089\u3059\u308B\u3068\u5996\u602A\u307F\u305F\u3044\u306A\u3082\u3093\u3060\u304B\u3089\u30FB\u30FB\u30FB",
  "id" : 499881368449335297,
  "in_reply_to_status_id" : 499878282263658496,
  "created_at" : "2014-08-14 11:33:00 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499877453775384576",
  "geo" : { },
  "id_str" : "499878227976790016",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3067\u305F\uFF01\uFF01",
  "id" : 499878227976790016,
  "in_reply_to_status_id" : 499877453775384576,
  "created_at" : "2014-08-14 11:20:31 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499877763499585536",
  "text" : "\u30B1\u30FC\u30AD\u3068\u304B\u30D4\u30B5\u3068\u304B\u30DE\u30C3\u30AF\u306F\u5915\u98EF\u3068\u3057\u3066\u306F\u8A8D\u3081\u306A\u3044\u3063\u3066\u306E\u306F\u3001\u307E\u3041512\u30AE\u30AC\u6B69\u3050\u3089\u3044\u8B72\u3063\u3066\u7406\u89E3\u3067\u304D\u308B\u3068\u3057\u3066\u3001\u30E9\u30FC\u30E1\u30F3\u3068\u304B\u3046\u3069\u3093\u306F\u5915\u98EF\u3058\u3083\u306A\u3044\u3063\u3066\u306E\u306F\u3001\u7406\u89E3\u3067\u304D\u306A\u3044\u306A\u3041\u3002\u304A\u8179\u81A8\u308C\u308B\u3084\u3093\uFF1F",
  "id" : 499877763499585536,
  "created_at" : "2014-08-14 11:18:40 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499872671690006529",
  "geo" : { },
  "id_str" : "499876792400756737",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3042\u3063\u3001\u304A\u663C\u306B\u30B1\u30FC\u30AD\u3068\u304B\u3001\u5915\u98EF\u30DE\u30C3\u30AF\u3068\u304B\u8A31\u305B\u306A\u3044\u6D3E\u3067\u3059\u304B\u30FB\u30FB\u30FB",
  "id" : 499876792400756737,
  "in_reply_to_status_id" : 499872671690006529,
  "created_at" : "2014-08-14 11:14:49 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 69, 92 ],
      "url" : "https:\/\/t.co\/wS2ezjxj82",
      "expanded_url" : "https:\/\/medium.com\/solve-for-x\/x-to-close-417936dfc0dc",
      "display_url" : "medium.com\/solve-for-x\/x-\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "499876462296444928",
  "text" : "\u300C\u00D7\uFF1D\u9589\u3058\u308B\u30DC\u30BF\u30F3\u300D\u3063\u3066\u3001\u78BA\u304B\u306B\u65E5\u672C\u3060\u304B\u3089\u5F53\u7136\u306E\u3054\u3068\u304F\u53D7\u3051\u5165\u308C\u3066\u305F\u3051\u3069\u3001\u5916\u56FD\u3067\u306F\u00D7\u306B\u305D\u3093\u306A\u30A4\u30E1\u30FC\u30B8\u306A\u3044\u3057\u3001\u65E5\u672C\u7531\u6765\u304B\u3082\u3063\u3066\u306E\u3082\u7D0D\u5F97\u3067\u304D\u305F https:\/\/t.co\/wS2ezjxj82",
  "id" : 499876462296444928,
  "created_at" : "2014-08-14 11:13:30 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499871325674938368",
  "geo" : { },
  "id_str" : "499871995564019713",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u3048\uFF1F",
  "id" : 499871995564019713,
  "in_reply_to_status_id" : 499871325674938368,
  "created_at" : "2014-08-14 10:55:45 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307B\u3089\u3075\u304D",
      "screen_name" : "n0ixe",
      "indices" : [ 0, 6 ],
      "id_str" : "140231677",
      "id" : 140231677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499869864752738307",
  "geo" : { },
  "id_str" : "499871468621004801",
  "in_reply_to_user_id" : 140231677,
  "text" : "@n0ixe \u307E\u3041\u305D\u308C\u3082\u3042\u308B\u3093\u3067\u3059\u304C\u3001\u79C1\u306F\u5225\u306B\u6BCE\u65E5\u3061\u3083\u3093\u3068\u306F\u3057\u305F\u3082\u306E\u3092\u98DF\u3079\u306A\u304D\u3083\u3068\u3044\u3046\u611F\u899A\u304C\u306A\u304F\u3066\u3001\u305F\u307E\u306B\u306F\u9069\u5F53\u3067\u3044\u3044\u304B\u306A\u3063\u3066\u306A\u308B\u306E\u3067\u3001\u305D\u306E\u305F\u307E\u306E\u6A5F\u4F1A\u304C\u4E00\u4EBA\u306E\u6642\u3057\u304B\u306A\u3044\u306E\u3067\u3059\u3088\u3002",
  "id" : 499871468621004801,
  "in_reply_to_status_id" : 499869864752738307,
  "created_at" : "2014-08-14 10:53:39 +0000",
  "in_reply_to_screen_name" : "n0ixe",
  "in_reply_to_user_id_str" : "140231677",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "kzhr",
      "screen_name" : "kzhr",
      "indices" : [ 0, 5 ],
      "id_str" : "11484132",
      "id" : 11484132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499869658963402752",
  "geo" : { },
  "id_str" : "499871153058349058",
  "in_reply_to_user_id" : 11484132,
  "text" : "@kzhr \u305D\u3046\u304B\uFF1F",
  "id" : 499871153058349058,
  "in_reply_to_status_id" : 499869658963402752,
  "created_at" : "2014-08-14 10:52:24 +0000",
  "in_reply_to_screen_name" : "kzhr",
  "in_reply_to_user_id_str" : "11484132",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499870726610890753",
  "text" : "for\u306E\u4E2D\u3067initializer_list\u306F\u5272\u3068\u3088\u304F\u3084\u308B\u306E\u3060\u3051\u3069\u3001\u306A\u306B\u304B\u30C0\u30E1\u306A\u306E\uFF1F",
  "id" : 499870726610890753,
  "created_at" : "2014-08-14 10:50:43 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499869029096361984",
  "text" : "\u300C\u3072\u3068\u308A\u3060\u3068\u3059\u3050\u30DD\u30C6\u30C1\u304C\u5915\u98EF\u306B\u306A\u308B\u3093\u3060\u304B\u3089\u30FC\uFF57\u300D\u3063\u3066\u604B\u4EBA\u306B\u6012\u3089\u308C\u308B\u306E\u3060\u3051\u3069\u3001\u9006\u306B\u8A00\u3048\u3070\u666E\u6BB5\u306F\u3061\u3083\u3093\u3068\u5915\u98EF\u4F5C\u3063\u3066\u308B\u3063\u3066\u3053\u3068\u3067\u3001\u3060\u304B\u3089\u300C\u3072\u3068\u308A\u3060\u3057\u9069\u5F53\u3067\u3044\u3044\u3084\u30FC\u3001\u306A\u306B\u304C\u3044\u3044\u304B\u306A\u3001\u4F5C\u3089\u306A\u304F\u3066\u3044\u3044\u306A\u3089\u304A\u83D3\u5B50\u3067\u3044\u3063\u304B\u3001\u3058\u3083\u3042\u30DD\u30C6\u30C1\u304B\u306A\u300D\u3063\u3066\u601D\u8003\u306B\u306A\u308B\u306E\u3067\u3057\u3087\u3046\u3002",
  "id" : 499869029096361984,
  "created_at" : "2014-08-14 10:43:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499865371424202752",
  "text" : "\u3055\u3066\u4ECA\u65E5\u306F\u4F55\u98DF\u3079\u3088\u3046\uFF1F",
  "id" : 499865371424202752,
  "created_at" : "2014-08-14 10:29:26 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499707377935847424",
  "text" : "\u4E00\u756A\u6700\u521D\u306B\u300C\u30B3\u30A2\u30EC\u30B9\u30A2\u30AF\u30BB\u30B9\u300D\u3063\u3081\u805E\u3044\u305F\u6642\u300C\u30B3\u30A2\u30FB\u30EC\u30B9\u30FB\u30A2\u30AF\u30BB\u30B9\u300D\u3060\u3068\u601D\u3063\u305F\u3088\u306D\u3001\u8AB0\u3082\u304C\u901A\u308B\u9053\u3060\u3088\u306D\u3002",
  "id" : 499707377935847424,
  "created_at" : "2014-08-14 00:01:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499697547892375552",
  "text" : "\u306D\u3080\u30FB\u30FB\u30FB",
  "id" : 499697547892375552,
  "created_at" : "2014-08-13 23:22:34 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499583085650792449",
  "text" : "\u3088\u30FC\u307F\u3083\u304F\u306E\u30FC\u306F\u3057\u3089\u305B\u304B\u30FC\u305F\u3092\u30FC\u304D\u3056\u307F\u30FC\u306E\u30FC\u3044\u30FC\u308C\u30FC\u304B\u30FC\u305F\u3092\u30FC",
  "id" : 499583085650792449,
  "created_at" : "2014-08-13 15:47:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499580443230867458",
  "text" : "\u5FD8\u308C\u3066\u3066\u4F55\u306E\u554F\u984C\u3082\u306A\u3044\u3051\u3069\u306D\uFF01",
  "id" : 499580443230867458,
  "created_at" : "2014-08-13 15:37:14 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499580345792987136",
  "text" : "\u4ECA\u5E74\u306F\u8AB0\u304B\u3089\u3082\u5BBF\u63D0\u4F9B\u4F9D\u983C\u306A\u304B\u3063\u305F\u304B\u3089\u5FD8\u308C\u3066\u305F",
  "id" : 499580345792987136,
  "created_at" : "2014-08-13 15:36:50 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499580238863417344",
  "text" : "\u305D\u308D\u305D\u308D\u307F\u306A\u3055\u3093\u590F\u30B3\u30DF\u3067\u3059\u306A\uFF1F",
  "id" : 499580238863417344,
  "created_at" : "2014-08-13 15:36:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499546431993040896",
  "text" : "\u91D1\u66DC\u591C\u6687\u306A\u306E\u3067\u8AB0\u304B\u3054\u98EF\u98DF\u3079\u306B\u884C\u304D\u307E\u3057\u3087\u3046",
  "id" : 499546431993040896,
  "created_at" : "2014-08-13 13:22:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499516739009261571",
  "text" : "\u3068\u3053\u308D\u3067\u3055\u3063\u304D\u3069\u306A\u305F\u304B\u30E1\u30FC\u30EB\u9001\u3063\u3066\u3044\u305F\u3060\u3044\u305F\u3063\u307D\u3044\u3093\u3067\u3059\u304C\u3001\u3069\u306A\u305F\u3067\u3057\u3087\u3046\uFF1F",
  "id" : 499516739009261571,
  "created_at" : "2014-08-13 11:24:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/dev.twitter.com\/docs\/tfw\" rel=\"nofollow\"\u003ETwitter for Websites\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 61, 83 ],
      "url" : "http:\/\/t.co\/fD3KH1UvCC",
      "expanded_url" : "http:\/\/csqa.kddi.com\/posts\/view\/qid\/1404160043",
      "display_url" : "csqa.kddi.com\/posts\/view\/qid\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "499516645149143040",
  "text" : "\u305A\u3063\u3068\u30B1\u30A4\u30BF\u30A4(ISW13F)\u3067E\u30E1\u30FC\u30EB\u304C\u958B\u3051\u306A\u304B\u3063\u305F\u306E\u3067\u8ABF\u3079\u305F\u3089\u3053\u308C\u3060\u3063\u305F\u308F\u3002\u30E1\u30FC\u30EB\u6D88\u3048\u305F\u3051\u3069\u8D77\u52D5\u3067\u304D\u308B\u3088\u3046\u306B\u306A\u3063\u305F\u3002 http:\/\/t.co\/fD3KH1UvCC",
  "id" : 499516645149143040,
  "created_at" : "2014-08-13 11:23:43 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499505186864234496",
  "text" : "Boost.Preprocessor\u4F7F\u3044\u305F\u304B\u3063\u305F\u30FB\u30FB\u30FB",
  "id" : 499505186864234496,
  "created_at" : "2014-08-13 10:38:11 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499504779416969218",
  "text" : "\u4ECA\u65E5\u307B\u307C1\u65E5\u4E2D\u3001C\u3067\u30D7\u30EA\u30D7\u30ED\u30BB\u30C3\u30B5\u3067\u30EB\u30FC\u30D7\u4F5C\u3063\u305F\u308A\u3057\u3066\u305F\u3057\u3001\u3077\u308A\u3077\u308D\u305B\u3063\u3055\u529B\u304C1\u4E0A\u304C\u3063\u305F\u6C17\u304C\u3059\u308B\u3002",
  "id" : 499504779416969218,
  "created_at" : "2014-08-13 10:36:34 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499504589259821057",
  "text" : "\u4ECA\u65E5\u306F\u8C5A\u8089\u306E\u65E5\u3067\u3059\uFF01",
  "id" : 499504589259821057,
  "created_at" : "2014-08-13 10:35:49 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u9022\u7F8E",
      "screen_name" : "secondYJM",
      "indices" : [ 0, 10 ],
      "id_str" : "2338345100",
      "id" : 2338345100
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499196328635682817",
  "geo" : { },
  "id_str" : "499196683033395201",
  "in_reply_to_user_id" : 2338345100,
  "text" : "@secondYJM \u8A00\u308F\u306A\u304F\u3066\u3044\u3044\u3053\u3068\u3082\u3042\u308B\u3088",
  "id" : 499196683033395201,
  "in_reply_to_status_id" : 499196328635682817,
  "created_at" : "2014-08-12 14:12:18 +0000",
  "in_reply_to_screen_name" : "secondYJM",
  "in_reply_to_user_id_str" : "2338345100",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3048\u3093\u305D",
      "screen_name" : "ens_o",
      "indices" : [ 0, 6 ],
      "id_str" : "127528231",
      "id" : 127528231
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499195746374017024",
  "geo" : { },
  "id_str" : "499195891832467456",
  "in_reply_to_user_id" : 127528231,
  "text" : "@ens_o \u540C\u3058\u4EBA\u3068\u4F55\u5EA6\u3082\uFF1F",
  "id" : 499195891832467456,
  "in_reply_to_status_id" : 499195746374017024,
  "created_at" : "2014-08-12 14:09:09 +0000",
  "in_reply_to_screen_name" : "ens_o",
  "in_reply_to_user_id_str" : "127528231",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3048\u3093\u305D",
      "screen_name" : "ens_o",
      "indices" : [ 0, 6 ],
      "id_str" : "127528231",
      "id" : 127528231
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499195348456194049",
  "geo" : { },
  "id_str" : "499195498301894656",
  "in_reply_to_user_id" : 127528231,
  "text" : "@ens_o \u3058\u3083\u304210\u56DE\u3050\u3089\u3044",
  "id" : 499195498301894656,
  "in_reply_to_status_id" : 499195348456194049,
  "created_at" : "2014-08-12 14:07:36 +0000",
  "in_reply_to_screen_name" : "ens_o",
  "in_reply_to_user_id_str" : "127528231",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3048\u3093\u305D",
      "screen_name" : "ens_o",
      "indices" : [ 0, 6 ],
      "id_str" : "127528231",
      "id" : 127528231
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499195036244774912",
  "geo" : { },
  "id_str" : "499195105975078916",
  "in_reply_to_user_id" : 127528231,
  "text" : "@ens_o \u305F\u304F\u3055\u3093\u7D50\u5A5A\u3057\u3088\u3046\uFF01",
  "id" : 499195105975078916,
  "in_reply_to_status_id" : 499195036244774912,
  "created_at" : "2014-08-12 14:06:02 +0000",
  "in_reply_to_screen_name" : "ens_o",
  "in_reply_to_user_id_str" : "127528231",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30EC\u30E2\u30F3\u30D4\u30FC\u30EB\u30FB\u3077\u3061\u3053",
      "screen_name" : "puchlco",
      "indices" : [ 0, 8 ],
      "id_str" : "88585892",
      "id" : 88585892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499187828245278720",
  "geo" : { },
  "id_str" : "499189860859531264",
  "in_reply_to_user_id" : 88585892,
  "text" : "@puchlco \u3046\u3059\u3042\u3058",
  "id" : 499189860859531264,
  "in_reply_to_status_id" : 499187828245278720,
  "created_at" : "2014-08-12 13:45:11 +0000",
  "in_reply_to_screen_name" : "puchlco",
  "in_reply_to_user_id_str" : "88585892",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30EC\u30E2\u30F3\u30D4\u30FC\u30EB\u30FB\u3077\u3061\u3053",
      "screen_name" : "puchlco",
      "indices" : [ 0, 8 ],
      "id_str" : "88585892",
      "id" : 88585892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499187602965020672",
  "geo" : { },
  "id_str" : "499187666382880769",
  "in_reply_to_user_id" : 88585892,
  "text" : "@puchlco \u3068\u3093\u3053\u3064\u30E9\u30FC\u30E1\u30F3\u306B\u6700\u9069\uFF1F",
  "id" : 499187666382880769,
  "in_reply_to_status_id" : 499187602965020672,
  "created_at" : "2014-08-12 13:36:28 +0000",
  "in_reply_to_screen_name" : "puchlco",
  "in_reply_to_user_id_str" : "88585892",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30EC\u30E2\u30F3\u30D4\u30FC\u30EB\u30FB\u3077\u3061\u3053",
      "screen_name" : "puchlco",
      "indices" : [ 0, 8 ],
      "id_str" : "88585892",
      "id" : 88585892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499187090731442177",
  "geo" : { },
  "id_str" : "499187124810174466",
  "in_reply_to_user_id" : 88585892,
  "text" : "@puchlco \u30A4\u30A4\u30A8\u30B1\u30C3\u30B3\u30A6\u30C7\u30B9",
  "id" : 499187124810174466,
  "in_reply_to_status_id" : 499187090731442177,
  "created_at" : "2014-08-12 13:34:19 +0000",
  "in_reply_to_screen_name" : "puchlco",
  "in_reply_to_user_id_str" : "88585892",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30EC\u30E2\u30F3\u30D4\u30FC\u30EB\u30FB\u3077\u3061\u3053",
      "screen_name" : "puchlco",
      "indices" : [ 0, 8 ],
      "id_str" : "88585892",
      "id" : 88585892
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499186760622936065",
  "geo" : { },
  "id_str" : "499186867544154112",
  "in_reply_to_user_id" : 88585892,
  "text" : "@puchlco \u305D\u3093\u306A\u306E\u611B\u3067\u308B\u5BFE\u8C61\u306A\u306E\u3067\\99\u5B89\u3059\u304E\u308B\u306B\u6C7A\u307E\u3063\u3066\u308B\u3058\u3083\u306A\u3044\u3067\u3059\u304B\uFF01",
  "id" : 499186867544154112,
  "in_reply_to_status_id" : 499186760622936065,
  "created_at" : "2014-08-12 13:33:18 +0000",
  "in_reply_to_screen_name" : "puchlco",
  "in_reply_to_user_id_str" : "88585892",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499183380076847104",
  "text" : "\u300C\u3075\u3041\u307C\u308A\u305F\u3044\u300D\u3063\u3066\u8A00\u3063\u3066\u308B\u30C4\u30A4\u30FC\u30C8\u3092\u3075\u3041\u307C\u308B\u3060\u3051\u3075\u3041\u307C\u3063\u3066\u3075\u3041\u307C\u3089\u305B\u306A\u3044\u7C21\u5358\u306A\u304A\u4ED5\u4E8B",
  "id" : 499183380076847104,
  "created_at" : "2014-08-12 13:19:26 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u9022\u7F8E",
      "screen_name" : "secondYJM",
      "indices" : [ 0, 10 ],
      "id_str" : "2338345100",
      "id" : 2338345100
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499178611924934656",
  "geo" : { },
  "id_str" : "499178781081210880",
  "in_reply_to_user_id" : 2338345100,
  "text" : "@secondYJM \u305D\u308C\u3092\u305C\u3072\u5927\u4EBA\u7528\u306B\u3082\u307B\u3057\u3044\u30FB\u30FB\u30FB\uFF01",
  "id" : 499178781081210880,
  "in_reply_to_status_id" : 499178611924934656,
  "created_at" : "2014-08-12 13:01:10 +0000",
  "in_reply_to_screen_name" : "secondYJM",
  "in_reply_to_user_id_str" : "2338345100",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499169879186878466",
  "text" : "\u8C5A\u30D0\u30E9\u305D\u308D\u305D\u308D\u98DF\u3079\u305F\u3044\u306D",
  "id" : 499169879186878466,
  "created_at" : "2014-08-12 12:25:47 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499162261668110336",
  "text" : "\u4E09\u89D2\u95A2\u6570\u306E\u52A0\u6CD5\u5B9A\u7406\u3068\u304B\u3053\u3059\u3082\u3059\u306A\u3093\u3061\u3083\u3089\u3067\u899A\u3048\u3066\u306A\u3044\u3057\u3001\u305D\u3082\u305D\u3082\u500D\u89D2\u3068\u304B\u534A\u89D2\u3068\u304B\u899A\u3048\u3066\u3059\u3089\u306A\u304B\u3063\u305F\u3067\u3059\u304C",
  "id" : 499162261668110336,
  "created_at" : "2014-08-12 11:55:31 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3042\u3059\u307F\u3093",
      "screen_name" : "an_asumin",
      "indices" : [ 0, 10 ],
      "id_str" : "202087794",
      "id" : 202087794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499160962474074112",
  "geo" : { },
  "id_str" : "499161762592088064",
  "in_reply_to_user_id" : 202087794,
  "text" : "@an_asumin \u3042\u3059\u307F\u3093\u4EBA\u6C17\u306B\u3042\u3084\u304B\u308D\u3046\u3068\u601D\u3063\u3066\u30FB\u30FB\u30FB\uFF08\u3042\u3059\u307F\u3093\u3055\u3093\u6016\u3044\uFF09",
  "id" : 499161762592088064,
  "in_reply_to_status_id" : 499160962474074112,
  "created_at" : "2014-08-12 11:53:32 +0000",
  "in_reply_to_screen_name" : "an_asumin",
  "in_reply_to_user_id_str" : "202087794",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499160866999119872",
  "text" : "\u3061\u3083\u3093\u3068\u3075\u3041\u307C\u3063\u3066\u304B\u3089\u767A\u8A00\u3059\u308C\u3070\u554F\u984C\u306A\u3044",
  "id" : 499160866999119872,
  "created_at" : "2014-08-12 11:49:59 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499160491499847683",
  "text" : "\u3042\u3059\u307F\u3093\n\u3059\u307F\u3093\u3000\n\u307F\u3093\u3000\u3000\u3042\n\u3093\u3000\u3000\u3042\u3059\n\u3000\u3000\u3042\u3059\u307F\n\u3000\u3042\u3059\u307F\u3093\n\u3042\u3059\u307F\u3093\u3000\n\u3059\u307F\u3093\n\u307F\u3093\u3000\u3000\u3042\n\u3093\u3000\u3000\u3042\u3059\n\u3000\u3000\u3042\u3059\u307F\n\u3000\u3042\u3059\u307F\u3093\n\u3042\u3059\u307F\u3093\u3000\n\u3059\u307F\u3093\u3000\u3000\n\u307F\u3093\u3000\u3000\u3042\n\u3093\u3000\u3000\u3042\u3059\n\u3000\u3000\u3042\u3059\u307F\n\u3000\u3042\u3059\u307F\u3093\n\u3042\u3059\u307F\u3093\u3000\n\u3059\u307F\u3093\u3000\u3000\n\u307F\u3093\u3000\u3000\u3042\n\u3093\u3000\u3000\u3042\u3059\n\u3000\u3000\u3042\u3059\u307F\n\u3000\u3042\u3059\u307F\u3093",
  "id" : 499160491499847683,
  "created_at" : "2014-08-12 11:48:29 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3088\u308A\u306E",
      "screen_name" : "treeagate",
      "indices" : [ 0, 10 ],
      "id_str" : "115904452",
      "id" : 115904452
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499158169390567427",
  "geo" : { },
  "id_str" : "499159004250001408",
  "in_reply_to_user_id" : 115904452,
  "text" : "@treeagate \u3067\u3082\u3061\u3083\u3093\u3068\u7B49\u4FA1\u4EA4\u63DB\u3057\u3066\u308B",
  "id" : 499159004250001408,
  "in_reply_to_status_id" : 499158169390567427,
  "created_at" : "2014-08-12 11:42:35 +0000",
  "in_reply_to_screen_name" : "treeagate",
  "in_reply_to_user_id_str" : "115904452",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499158121395146752",
  "geo" : { },
  "id_str" : "499158663446003713",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei \u8272\u7D19\u304C\u9AD8\u3044\u304B\u30FB\u30FB\u30FB",
  "id" : 499158663446003713,
  "in_reply_to_status_id" : 499158121395146752,
  "created_at" : "2014-08-12 11:41:13 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499158380049494016",
  "text" : "\u96FB\u8ECA\u5185\u3068\u304B\u63FA\u308C\u304C\u5FC3\u5730\u3088\u304F\u3066\u5BDD\u3066\u3057\u307E\u3046\u3063\u3066\u3001\u8A71\u304B\u3089\u3001\u306A\u3093\u3067\u3053\u306E\u63FA\u308C\u3092\u518D\u73FE\u3059\u308B\u30D9\u30C3\u30C9\u304C\u751F\u307E\u308C\u306A\u3044\u3093\u3067\u3059\u304B\uFF1F",
  "id" : 499158380049494016,
  "created_at" : "2014-08-12 11:40:06 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499157873947975680",
  "text" : "\u5C06\u6765\u306B\u5024\u304C\u3042\u304C\u308B\u3068\u717D\u3063\u3066\u304A\u304F\u3053\u3068\u3067\u3001\u81EA\u5206\u306F\u3042\u3059\u307F\u3093\u3055\u3093\u306B\u304A\u9858\u3044\u3057\u3066\u30B5\u30A4\u30F3100\u679A\u3050\u3089\u3044\u3082\u3089\u3063\u3066\u3001100\u5186\u3067\u58F2\u308C\u30701\u4E07\u5186\u5132\u3051\u3089\u308C\u308B\u3002\u7D20\u6674\u3089\u3057\u3044\u3002",
  "id" : 499157873947975680,
  "created_at" : "2014-08-12 11:38:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499156777368178688",
  "text" : "\u8B66\u5BDF\u3082\u902E\u6355\u3057\u305F\u3060\u3051\u3067\u767A\u8868\u3057\u306A\u3051\u308C\u3070\u3044\u3044\u306E\u306B\u3002\u5831\u9053\u6A5F\u95A2\u3082\u88C1\u5224\u3067\u6709\u7F6A\u304C\u78BA\u5B9A\u3057\u3066\u304B\u3089\u306B\u3057\u305F\u3089\uFF1F",
  "id" : 499156777368178688,
  "created_at" : "2014-08-12 11:33:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499155354765103106",
  "text" : "\u3068\u3001\u3050\u3050\u3089\u3073\u3066\u3043\u4F4E\u3044\u3053\u3068\u3092\u5606\u3044\u3066\u3044\u305F\u3053\u3068\u3092\u627F\u77E5\u3067\u3042\u3048\u3066@\u306A\u3057",
  "id" : 499155354765103106,
  "created_at" : "2014-08-12 11:28:05 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499155090842726401",
  "text" : "\u3042\u3059\u307F\u3093\u3055\u3093\u3001\u3069\u3093\u3069\u3093\u6709\u540D\u306B\u306A\u3063\u3066\u3044\u304F\u306E\u3067\u3001\u3044\u307E\u306E\u3046\u3061\u306B\u30B5\u30A4\u30F3\u3082\u3089\u3063\u3066\u304A\u3051\u3070\u5B6B\u306E\u7D50\u5A5A\u8CC7\u91D1\u3050\u3089\u3044\u306B\u306F\u306A\u308A\u305D\u3046\u3002",
  "id" : 499155090842726401,
  "created_at" : "2014-08-12 11:27:02 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499153196338511872",
  "text" : "\u5E30\u3063\u3066\u3061\u3087\u3063\u3068\u30B2\u30FC\u30E0\u3057\u3066\u4F11\u307F\u305F\u3044\u306D",
  "id" : 499153196338511872,
  "created_at" : "2014-08-12 11:19:30 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499149601790648320",
  "text" : "\u4ECA\u65E5\u306E\u6669\u3054\u306F\u3093\u306F\u30AB\u30C4\u30AA\u3089\u3057\u3044\u306E\u3067\u697D\u3057\u307F\u3067\u3059",
  "id" : 499149601790648320,
  "created_at" : "2014-08-12 11:05:13 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Yosuke ONOUE",
      "screen_name" : "_likr",
      "indices" : [ 0, 6 ],
      "id_str" : "60329920",
      "id" : 60329920
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "499003978189791232",
  "geo" : { },
  "id_str" : "499004431795376128",
  "in_reply_to_user_id" : 60329920,
  "text" : "@_likr \u666E\u901A\u306B\u901A\u77E5\u304F\u308B\u306E\u3067",
  "id" : 499004431795376128,
  "in_reply_to_status_id" : 499003978189791232,
  "created_at" : "2014-08-12 01:28:22 +0000",
  "in_reply_to_screen_name" : "_likr",
  "in_reply_to_user_id_str" : "60329920",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "499003195180318721",
  "text" : "\u304A\u306E\u3046\u3047\u30FC\u3055\u3093\u3001\u3075\u3041\u307C\u3063\u305F\u3063\u3066\u3053\u3068\u306F\u3001\u308F\u3056\u308F\u3056\u6771\u4EAC\u307E\u3067\u6280\u8853\u7684\u3067\u3042\u3044\u3061\u3085\u30FC\u3057\u3066\u304F\u308C\u308B\u3063\u3066\u3053\u3068\u3067\u3059\u306D\u3002",
  "id" : 499003195180318721,
  "created_at" : "2014-08-12 01:23:27 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "t_masuda",
      "screen_name" : "tmasda",
      "indices" : [ 0, 7 ],
      "id_str" : "91782180",
      "id" : 91782180
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498999619334860801",
  "geo" : { },
  "id_str" : "499000772730703873",
  "in_reply_to_user_id" : 91782180,
  "text" : "@tmasda \u3082\u3046C90\u306F\u5ACC\u3067\u3059\u30FB\u30FB\u30FB",
  "id" : 499000772730703873,
  "in_reply_to_status_id" : 498999619334860801,
  "created_at" : "2014-08-12 01:13:49 +0000",
  "in_reply_to_screen_name" : "tmasda",
  "in_reply_to_user_id_str" : "91782180",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498997896235069440",
  "geo" : { },
  "id_str" : "498999427822936064",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u30D2\u30F3\u30C8\uFF1A\u5FD9\u3057\u3055",
  "id" : 498999427822936064,
  "in_reply_to_status_id" : 498997896235069440,
  "created_at" : "2014-08-12 01:08:29 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u5C71\u7530\u3066\u308B\u307F",
      "screen_name" : "telmin_orca",
      "indices" : [ 0, 12 ],
      "id_str" : "72457512",
      "id" : 72457512
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498996787181404160",
  "geo" : { },
  "id_str" : "498997705893359616",
  "in_reply_to_user_id" : 72457512,
  "text" : "@telmin_orca \u6700\u8FD1\u3067\u3042\u3044\u3061\u3085\u30FC\u3057\u3066\u307E\u305B\u3093\u306D",
  "id" : 498997705893359616,
  "in_reply_to_status_id" : 498996787181404160,
  "created_at" : "2014-08-12 01:01:38 +0000",
  "in_reply_to_screen_name" : "telmin_orca",
  "in_reply_to_user_id_str" : "72457512",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498997480269156352",
  "text" : "\u30D8\u30C3\u30C0\u3067using std\u3068\u304B\u3084\u3089\u308C\u308B\u306E\u307B\u3093\u3068\u6EC5\u3073\u3066\u307B\u3057\u3044",
  "id" : 498997480269156352,
  "created_at" : "2014-08-12 01:00:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AB\u30CD\u30C0\u30C3\u30AF",
      "screen_name" : "kanedaq",
      "indices" : [ 0, 8 ],
      "id_str" : "266417808",
      "id" : 266417808
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498861650045202435",
  "geo" : { },
  "id_str" : "498997031612841984",
  "in_reply_to_user_id" : 266417808,
  "text" : "@kanedaq \u990A\u751F\u3057\u3066\u304F\u3060\u3055\u3044\u30FB\u30FB\u30FB",
  "id" : 498997031612841984,
  "in_reply_to_status_id" : 498861650045202435,
  "created_at" : "2014-08-12 00:58:57 +0000",
  "in_reply_to_screen_name" : "kanedaq",
  "in_reply_to_user_id_str" : "266417808",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498996353989484544",
  "text" : "\u52C9\u5F37\u4F1A\u53C2\u52A0\u8005\uFF1D\u6280\u8853\u7684\u51FA\u4F1A\u3044\u53A8\u3001\u3063\u3066\u306E\u306F\u78BA\u304B\u306B\u3042\u308B\u3068\u601D\u3046\u3002",
  "id" : 498996353989484544,
  "created_at" : "2014-08-12 00:56:16 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498995748181000193",
  "text" : "\u6628\u65E5\u306E\u5915\u98EF\u306E\u3056\u308B\u3046\u3069\u3093\u304C\u7F8E\u5473\u3057\u304B\u3063\u305F\u306E\u3067\u3001\u8FD1\u3044\u3046\u3061\u306B\u307E\u305F\u3084\u308D\u3046\u3002",
  "id" : 498995748181000193,
  "created_at" : "2014-08-12 00:53:51 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307E\u3044\u306F\u4E00\u5E74\u751F",
      "screen_name" : "mai_10",
      "indices" : [ 0, 7 ],
      "id_str" : "66628322",
      "id" : 66628322
    }, {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 8, 20 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498991559480590336",
  "geo" : { },
  "id_str" : "498994279906160640",
  "in_reply_to_user_id" : 66628322,
  "text" : "@mai_10 @torigomoku7 \u304A\u4E92\u3044\u304C\u304A\u4E92\u3044\u3092\u898B\u308C\u3070\u89E3\u6C7A",
  "id" : 498994279906160640,
  "in_reply_to_status_id" : 498991559480590336,
  "created_at" : "2014-08-12 00:48:01 +0000",
  "in_reply_to_screen_name" : "mai_10",
  "in_reply_to_user_id_str" : "66628322",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30D9\u30EB\u30AC\u30E2\u30C3\u30C8",
      "screen_name" : "bergamot_tone",
      "indices" : [ 0, 14 ],
      "id_str" : "780733957",
      "id" : 780733957
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498990188433920000",
  "geo" : { },
  "id_str" : "498994119000080384",
  "in_reply_to_user_id" : 780733957,
  "text" : "@bergamot_tone \u4F55\u5352\u3088\u308D\u3057\u304F\u304A\u9858\u3044\u7533\u3057\u4E0A\u3052\u307E\u3059\u3002",
  "id" : 498994119000080384,
  "in_reply_to_status_id" : 498990188433920000,
  "created_at" : "2014-08-12 00:47:23 +0000",
  "in_reply_to_screen_name" : "bergamot_tone",
  "in_reply_to_user_id_str" : "780733957",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    }, {
      "name" : "\u307E\u3044\u306F\u4E00\u5E74\u751F",
      "screen_name" : "mai_10",
      "indices" : [ 13, 20 ],
      "id_str" : "66628322",
      "id" : 66628322
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498988665830248448",
  "geo" : { },
  "id_str" : "498989763978735616",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 @mai_10 \u4E8C\u4EBA\u3057\u3066\u7740\u308C\u3070\u5E78\u305B\u4E8C\u500D\u306A\u306E\u3067\u306F",
  "id" : 498989763978735616,
  "in_reply_to_status_id" : 498988665830248448,
  "created_at" : "2014-08-12 00:30:05 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30D9\u30EB\u30AC\u30E2\u30C3\u30C8",
      "screen_name" : "bergamot_tone",
      "indices" : [ 0, 14 ],
      "id_str" : "780733957",
      "id" : 780733957
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498988216658038784",
  "geo" : { },
  "id_str" : "498989485766356993",
  "in_reply_to_user_id" : 780733957,
  "text" : "@bergamot_tone \u4EE3\u308F\u308A\u306B\u3046\u3061\u306E\u3082\u51FA\u3057\u3066\u304A\u3044\u3066\u304F\u308C\u307E\u305B\u3093\u304B\uFF01\uFF01",
  "id" : 498989485766356993,
  "in_reply_to_status_id" : 498988216658038784,
  "created_at" : "2014-08-12 00:28:58 +0000",
  "in_reply_to_screen_name" : "bergamot_tone",
  "in_reply_to_user_id_str" : "780733957",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3075\u307F@\u30CF\u30A4\u30AB\u30E9Haskeller",
      "screen_name" : "fumieval",
      "indices" : [ 0, 9 ],
      "id_str" : "75975397",
      "id" : 75975397
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498972107863126018",
  "geo" : { },
  "id_str" : "498989244770054144",
  "in_reply_to_user_id" : 75975397,
  "text" : "@fumieval \u610F\u5916\u3068\u6771\u4EAC\u4EE5\u5916\u306E\u653F\u4EE4\u6307\u5B9A\u90FD\u5E02\u3050\u3089\u3044\u306E\u65B9\u304C\u4FBF\u5229\u3063\u307D\u3044\u3067\u3059",
  "id" : 498989244770054144,
  "in_reply_to_status_id" : 498972107863126018,
  "created_at" : "2014-08-12 00:28:01 +0000",
  "in_reply_to_screen_name" : "fumieval",
  "in_reply_to_user_id_str" : "75975397",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498971115218165760",
  "geo" : { },
  "id_str" : "498988902296743936",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u307E\u3060\u3044\u3051\u307E\u3059\uFF01",
  "id" : 498988902296743936,
  "in_reply_to_status_id" : 498971115218165760,
  "created_at" : "2014-08-12 00:26:39 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307E\u3044\u306F\u4E00\u5E74\u751F",
      "screen_name" : "mai_10",
      "indices" : [ 0, 7 ],
      "id_str" : "66628322",
      "id" : 66628322
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498977799672045568",
  "geo" : { },
  "id_str" : "498988309503164418",
  "in_reply_to_user_id" : 66628322,
  "text" : "@mai_10 \u3064\u307E\u308A\u307E\u3044\u59C9\u3082\u30D2\u30E9\u30D2\u30E9\u7740\u3066\u7B11\u9854\u304C\u4F3C\u5408\u3046\u3068\u3044\u3046\u3053\u3068\uFF1F",
  "id" : 498988309503164418,
  "in_reply_to_status_id" : 498977799672045568,
  "created_at" : "2014-08-12 00:24:18 +0000",
  "in_reply_to_screen_name" : "mai_10",
  "in_reply_to_user_id_str" : "66628322",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498987338857328640",
  "text" : "\u3042\u3001\u30B4\u30DF\u307E\u305F\u51FA\u3057\u5FD8\u308C\u305F\u30FB\u30FB\u30FB",
  "id" : 498987338857328640,
  "created_at" : "2014-08-12 00:20:26 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498987256401522688",
  "text" : "\u3072\u307E\u3060",
  "id" : 498987256401522688,
  "created_at" : "2014-08-12 00:20:07 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498830726586851329",
  "text" : "\u304D\u88C2\u3084\u7834\u58CA\u3063\u3066\u3001\u30E9\u30F3\u30C0\u30E0\u306A\u8981\u7D20\u3082\u3068\u3066\u3082\u7D61\u3080\u73FE\u8C61\u306A\u306E\u3067\u3001\u8A08\u7B97\u3057\u3065\u3089\u3044\u3067\u3059\u3088\u306D\u3047\u3002",
  "id" : 498830726586851329,
  "created_at" : "2014-08-11 13:58:07 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498828240056623105",
  "text" : "\u5915\u98EF\u306F\u3046\u3069\u3093\u306B\u3057\u3088\u3046",
  "id" : 498828240056623105,
  "created_at" : "2014-08-11 13:48:14 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498816491064532992",
  "text" : "\u5BB6\u8A08\u7C3F\u3068\u53E3\u5EA7\u306E\u6B8B\u9AD8\u306B3\u4E07\u5186\u3050\u3089\u3044\u5265\u96E2\u304C\u3042\u308B\u306E\u3060\u3051\u3069\u306A\u3093\u3060\u308D\u3046\uFF1F",
  "id" : 498816491064532992,
  "created_at" : "2014-08-11 13:01:33 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498501736529534976",
  "text" : "\u3055\u3066\u306D\u3088\u304B\u30FC",
  "id" : 498501736529534976,
  "created_at" : "2014-08-10 16:10:50 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498500698716139520",
  "geo" : { },
  "id_str" : "498501700588548099",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u304C\u3093\u3070\u308C",
  "id" : 498501700588548099,
  "in_reply_to_status_id" : 498500698716139520,
  "created_at" : "2014-08-10 16:10:41 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498501482551853059",
  "geo" : { },
  "id_str" : "498501648126189568",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u3042\u30FC\u30FB\u30FB\u30FB\u3002\u306A\u304A\u3057\u3083\u30FC\u3088\uFF01\uFF01",
  "id" : 498501648126189568,
  "in_reply_to_status_id" : 498501482551853059,
  "created_at" : "2014-08-10 16:10:29 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498500333895561217",
  "geo" : { },
  "id_str" : "498501413488435203",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u7D2B\u8272\u306A\u306E\u306F\u30A8\u30D5\u30A7\u30AF\u30C8\uFF1F",
  "id" : 498501413488435203,
  "in_reply_to_status_id" : 498500333895561217,
  "created_at" : "2014-08-10 16:09:33 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498496736252284929",
  "geo" : { },
  "id_str" : "498497224133713920",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u307B\u308F\u307B\u308F\u3067\u3059\u3088\uFF01",
  "id" : 498497224133713920,
  "in_reply_to_status_id" : 498496736252284929,
  "created_at" : "2014-08-10 15:52:54 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498495362521251841",
  "geo" : { },
  "id_str" : "498495965737660416",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u30EA\u30D5\u30EC\u30AF\u306F\u3084\u308F\u3089\u304B\u97F3\u30B2\u30FC\u3060\u304B\u3089",
  "id" : 498495965737660416,
  "in_reply_to_status_id" : 498495362521251841,
  "created_at" : "2014-08-10 15:47:54 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "null",
      "screen_name" : "haruraruru",
      "indices" : [ 0, 11 ],
      "id_str" : "2914910132",
      "id" : 2914910132
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498495240865468416",
  "geo" : { },
  "id_str" : "498495327150698496",
  "in_reply_to_user_id" : 392819317,
  "text" : "@haruraruru \u30E6\u30D3\u306E\u65B9\u304C\u304A\u304B\u3057\u3044",
  "id" : 498495327150698496,
  "in_reply_to_status_id" : 498495240865468416,
  "created_at" : "2014-08-10 15:45:22 +0000",
  "in_reply_to_screen_name" : "kt2340",
  "in_reply_to_user_id_str" : "392819317",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498488122754076672",
  "text" : "@yu_ga0 \u304B\u307E\u3063\u3066\u30FC",
  "id" : 498488122754076672,
  "created_at" : "2014-08-10 15:16:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 0, 11 ],
      "id_str" : "115981748",
      "id" : 115981748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498449849243549696",
  "geo" : { },
  "id_str" : "498450204639510528",
  "in_reply_to_user_id" : 115981748,
  "text" : "@ttata_trit \u8A08\u7B97\u3057\u3066\u306A\u3044",
  "id" : 498450204639510528,
  "in_reply_to_status_id" : 498449849243549696,
  "created_at" : "2014-08-10 12:46:04 +0000",
  "in_reply_to_screen_name" : "ttata_trit",
  "in_reply_to_user_id_str" : "115981748",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 0, 11 ],
      "id_str" : "115981748",
      "id" : 115981748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498449077265108994",
  "geo" : { },
  "id_str" : "498449751986020352",
  "in_reply_to_user_id" : 115981748,
  "text" : "@ttata_trit \u308F\u308A\u304B\u3057",
  "id" : 498449751986020352,
  "in_reply_to_status_id" : 498449077265108994,
  "created_at" : "2014-08-10 12:44:16 +0000",
  "in_reply_to_screen_name" : "ttata_trit",
  "in_reply_to_user_id_str" : "115981748",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 0, 11 ],
      "id_str" : "115981748",
      "id" : 115981748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498434407678500864",
  "geo" : { },
  "id_str" : "498434683764371456",
  "in_reply_to_user_id" : 115981748,
  "text" : "@ttata_trit \u3053\u306E\u90E8\u5C4B\u6696\u304B\u3044",
  "id" : 498434683764371456,
  "in_reply_to_status_id" : 498434407678500864,
  "created_at" : "2014-08-10 11:44:23 +0000",
  "in_reply_to_screen_name" : "ttata_trit",
  "in_reply_to_user_id_str" : "115981748",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498427476121362432",
  "text" : "\u5915\u98EF\u306F\u30CF\u30F3\u30D0\u30FC\u30B0\u3067\u3044\u3044\u304B\u3002\u3067\u3063\u304B\u3044\u3084\u3064\u3002",
  "id" : 498427476121362432,
  "created_at" : "2014-08-10 11:15:45 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498420335130443776",
  "geo" : { },
  "id_str" : "498421419428958208",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u5BFF\u53F8\u3044\u3044\u306A\u30FC",
  "id" : 498421419428958208,
  "in_reply_to_status_id" : 498420335130443776,
  "created_at" : "2014-08-10 10:51:41 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498364651600490496",
  "text" : "\u30C9\u30E9\u3048\u3082\u3093\u3063\u3066\u30AE\u30E3\u30B0\u6F2B\u753B\u3060\u3063\u305F\u306F\u305A\u306A\u306E\u306B\u3001\u306A\u3093\u3067\u3044\u3064\u306E\u9593\u306B\u304B\u611F\u52D5\u30FB\u6559\u8A13\u3082\u306E\u306B\u306A\u3063\u305F\u306E\uFF1F",
  "id" : 498364651600490496,
  "created_at" : "2014-08-10 07:06:06 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498344714349731841",
  "geo" : { },
  "id_str" : "498344984353853440",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u78BA\u304B\u306B\uFF57",
  "id" : 498344984353853440,
  "in_reply_to_status_id" : 498344714349731841,
  "created_at" : "2014-08-10 05:47:57 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498336457040216064",
  "geo" : { },
  "id_str" : "498343392128946177",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u3084\u3063\u3071\u308A\u30A6\u30B3\u30F3\u8336\u3068\u713C\u914E\u3092\u983C\u3093\u3067\u5272\u308B\u306E\u304C\u30D9\u30B9\u30C8\u304B\u30FB\u30FB\u30FB",
  "id" : 498343392128946177,
  "in_reply_to_status_id" : 498336457040216064,
  "created_at" : "2014-08-10 05:41:38 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3057\u3057\u3083\u3082\uFF20\u30A6\u30ED\u30B3\u306E\u5973",
      "screen_name" : "shamo_6",
      "indices" : [ 0, 8 ],
      "id_str" : "1727272765",
      "id" : 1727272765
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498334000163729408",
  "geo" : { },
  "id_str" : "498334129851604993",
  "in_reply_to_user_id" : 1727272765,
  "text" : "@shamo_6 \u516C\u5171\u306E\u907F\u96E3\u5834\u6240",
  "id" : 498334129851604993,
  "in_reply_to_status_id" : 498334000163729408,
  "created_at" : "2014-08-10 05:04:49 +0000",
  "in_reply_to_screen_name" : "shamo_6",
  "in_reply_to_user_id_str" : "1727272765",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3057\u3057\u3083\u3082\uFF20\u30A6\u30ED\u30B3\u306E\u5973",
      "screen_name" : "shamo_6",
      "indices" : [ 0, 8 ],
      "id_str" : "1727272765",
      "id" : 1727272765
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498332072558092288",
  "geo" : { },
  "id_str" : "498332232033910785",
  "in_reply_to_user_id" : 1727272765,
  "text" : "@shamo_6 \u907F\u96E3\u3057\u3066\u304F\u3060\u3055\u3044\uFF57",
  "id" : 498332232033910785,
  "in_reply_to_status_id" : 498332072558092288,
  "created_at" : "2014-08-10 04:57:17 +0000",
  "in_reply_to_screen_name" : "shamo_6",
  "in_reply_to_user_id_str" : "1727272765",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304F\u3044\u306A\u3061\u3083\u3093",
      "screen_name" : "kuina_ch",
      "indices" : [ 0, 9 ],
      "id_str" : "886087615",
      "id" : 886087615
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498321436642009089",
  "geo" : { },
  "id_str" : "498321598584090628",
  "in_reply_to_user_id" : 886087615,
  "text" : "@kuina_ch \u304A\u5F79\u6240\u306F\u30AB\u30F3\u30DE\u30D4\u30EA\u30AA\u30C9\u4F7F\u3063\u3066\u305F\u308A\u3057\u307E\u3059\u3057\u306D\u30FC",
  "id" : 498321598584090628,
  "in_reply_to_status_id" : 498321436642009089,
  "created_at" : "2014-08-10 04:15:02 +0000",
  "in_reply_to_screen_name" : "kuina_ch",
  "in_reply_to_user_id_str" : "886087615",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304F\u3044\u306A\u3061\u3083\u3093",
      "screen_name" : "kuina_ch",
      "indices" : [ 0, 9 ],
      "id_str" : "886087615",
      "id" : 886087615
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498320771127595008",
  "geo" : { },
  "id_str" : "498320995178930179",
  "in_reply_to_user_id" : 886087615,
  "text" : "@kuina_ch \u3001\u3002\u304C\u65E5\u672C\u8A9E\u3068\u3044\u3046\u308F\u3051\u3067\u306F\u306A\u3044\uFF01\u3063\u3066\u8A00\u3063\u3066\u4F7F\u3063\u3066\u308B\u4EBA\u306F\u3044\u307E\u3057\u305F\u306D",
  "id" : 498320995178930179,
  "in_reply_to_status_id" : 498320771127595008,
  "created_at" : "2014-08-10 04:12:38 +0000",
  "in_reply_to_screen_name" : "kuina_ch",
  "in_reply_to_user_id_str" : "886087615",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u306F\u304A",
      "screen_name" : "smileyhao",
      "indices" : [ 0, 10 ],
      "id_str" : "163113678",
      "id" : 163113678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498314656943841281",
  "geo" : { },
  "id_str" : "498314882010189824",
  "in_reply_to_user_id" : 163113678,
  "text" : "@smileyhao \u3044\u3063\u305D\u3082\u3063\u3068\u6FE1\u308C\u3066\u30B7\u30E3\u30EF\u30FC\u3059\u308C\u3070\uFF01",
  "id" : 498314882010189824,
  "in_reply_to_status_id" : 498314656943841281,
  "created_at" : "2014-08-10 03:48:20 +0000",
  "in_reply_to_screen_name" : "smileyhao",
  "in_reply_to_user_id_str" : "163113678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498310321392476161",
  "geo" : { },
  "id_str" : "498310501265190914",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei \u3053\u3053\u306F\u5730\u65B9\u3058\u3083\u306A\u3044\uFF01\u9996\u90FD\u3060",
  "id" : 498310501265190914,
  "in_reply_to_status_id" : 498310321392476161,
  "created_at" : "2014-08-10 03:30:56 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "vigorous_action@wiki",
      "screen_name" : "vigorous_action",
      "indices" : [ 0, 16 ],
      "id_str" : "157933828",
      "id" : 157933828
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498302749558075392",
  "geo" : { },
  "id_str" : "498302837265158145",
  "in_reply_to_user_id" : 157933828,
  "text" : "@vigorous_action \uFF1F",
  "id" : 498302837265158145,
  "in_reply_to_status_id" : 498302749558075392,
  "created_at" : "2014-08-10 03:00:29 +0000",
  "in_reply_to_screen_name" : "vigorous_action",
  "in_reply_to_user_id_str" : "157933828",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498301678152478721",
  "geo" : { },
  "id_str" : "498301934965489664",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u554F\u984C\u306F\u30A6\u30B3\u30F3\u30CF\u30A4\u7F6E\u3044\u3066\u3042\u308B\u3068\u3053\u308D\u305D\u3093\u306A\u306B\u306A\u3044\u3063\u3066\u3044\u3046",
  "id" : 498301934965489664,
  "in_reply_to_status_id" : 498301678152478721,
  "created_at" : "2014-08-10 02:56:53 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498301384626692097",
  "geo" : { },
  "id_str" : "498301607709143041",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei \u304F\u308C\uFF01",
  "id" : 498301607709143041,
  "in_reply_to_status_id" : 498301384626692097,
  "created_at" : "2014-08-10 02:55:35 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307E\u3044\u306F\u4E00\u5E74\u751F",
      "screen_name" : "mai_10",
      "indices" : [ 0, 7 ],
      "id_str" : "66628322",
      "id" : 66628322
    }, {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 8, 20 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498300859986370560",
  "geo" : { },
  "id_str" : "498301320084738048",
  "in_reply_to_user_id" : 66628322,
  "text" : "@mai_10 @torigomoku7 \u3058\u3083\u3042\u63DA\u3052\u305F\u3066\u306E\u307B\u3046\u304F\u3060\u3055\u3044",
  "id" : 498301320084738048,
  "in_reply_to_status_id" : 498300859986370560,
  "created_at" : "2014-08-10 02:54:27 +0000",
  "in_reply_to_screen_name" : "mai_10",
  "in_reply_to_user_id_str" : "66628322",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498300782085554176",
  "text" : "\u30B3\u30ED\u30C3\u30B1\u98DF\u3079\u305F\u3044",
  "id" : 498300782085554176,
  "created_at" : "2014-08-10 02:52:19 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498236783176331265",
  "geo" : { },
  "id_str" : "498294888484438016",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u4ECA\u5EA6\u98F2\u307F\u4F1A\u306E\u6642\u304B\u3089\u30A6\u30B3\u30F3\u30CF\u30A4\u5FC5\u9808\u3067\u3059\u306D\uFF57",
  "id" : 498294888484438016,
  "in_reply_to_status_id" : 498236783176331265,
  "created_at" : "2014-08-10 02:28:53 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tommy6",
      "screen_name" : "Tommy6",
      "indices" : [ 0, 7 ],
      "id_str" : "11020582",
      "id" : 11020582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498185238988599297",
  "geo" : { },
  "id_str" : "498294814907969536",
  "in_reply_to_user_id" : 11020582,
  "text" : "@Tommy6 \u3060\u304B\u3089\u3084\u3081\u308D\uFF01\uFF01\u3084\u3081\u3066\u304F\u3060\u3055\u3044",
  "id" : 498294814907969536,
  "in_reply_to_status_id" : 498185238988599297,
  "created_at" : "2014-08-10 02:28:36 +0000",
  "in_reply_to_screen_name" : "Tommy6",
  "in_reply_to_user_id_str" : "11020582",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498294427933102080",
  "text" : "\u30CB\u30C8\u30EA\u306E\u4EBA\u304C\u6765\u308B\u76F4\u524D\u307E\u3067\u5BDD\u3066\u305F\u304B\u3089\u307E\u3060\u7720\u3044",
  "id" : 498294427933102080,
  "created_at" : "2014-08-10 02:27:04 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498294331480895489",
  "text" : "\u304A\u3001\u6025\u306B\u5927\u96E8\u3060",
  "id" : 498294331480895489,
  "created_at" : "2014-08-10 02:26:41 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Tommy6",
      "screen_name" : "Tommy6",
      "indices" : [ 0, 7 ],
      "id_str" : "11020582",
      "id" : 11020582
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498162133884874752",
  "geo" : { },
  "id_str" : "498167543631069184",
  "in_reply_to_user_id" : 11020582,
  "text" : "@Tommy6 \u30D5\u30E9\u30B0\u7ACB\u3066\u306A\u3044\u3067\uFF01\uFF01",
  "id" : 498167543631069184,
  "in_reply_to_status_id" : 498162133884874752,
  "created_at" : "2014-08-09 18:02:52 +0000",
  "in_reply_to_screen_name" : "Tommy6",
  "in_reply_to_user_id_str" : "11020582",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498167248498851840",
  "text" : "\u3061\u304F\u308F\u306E\u7A74\u306B\u3067\u3082\u5165\u3063\u3066\u308B\u3081\u3046",
  "id" : 498167248498851840,
  "created_at" : "2014-08-09 18:01:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 21, 43 ],
      "url" : "http:\/\/t.co\/AjhVAgEKhR",
      "expanded_url" : "http:\/\/twitter.com\/yu6ri8\/status\/498109063293440000",
      "display_url" : "twitter.com\/yu6ri8\/status\/\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "498160667455528960",
  "text" : "\u3046\u3061\u306E\u604B\u4EBA\u3055\u3093\u306E\u6700\u8FD1\u3060\u3044\u305F\u3044\u3053\u3093\u306A\u611F\u3058\u3060 http:\/\/t.co\/AjhVAgEKhR",
  "id" : 498160667455528960,
  "created_at" : "2014-08-09 17:35:33 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498160122472824833",
  "text" : "\u5272\u3068\u8B0E\u306E\u30B2\u30FC\u30E0\u3060\u3063\u305F\u3051\u3069\u8EFD\u5FEB\u3055\u306F\u826F\u3044",
  "id" : 498160122472824833,
  "created_at" : "2014-08-09 17:33:23 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/www.apple.com\" rel=\"nofollow\"\u003EiOS\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/498159773573844992\/photo\/1",
      "indices" : [ 70, 92 ],
      "url" : "http:\/\/t.co\/AmZBlWHiZo",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BunRrK1CAAA_GoI.jpg",
      "id_str" : "498159773372514304",
      "id" : 498159773372514304,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BunRrK1CAAA_GoI.jpg",
      "sizes" : [ {
        "h" : 680,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 361,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 637,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 680,
        "resize" : "fit",
        "w" : 640
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      } ],
      "display_url" : "pic.twitter.com\/AmZBlWHiZo"
    } ],
    "hashtags" : [ {
      "text" : "BrainWars",
      "indices" : [ 28, 38 ]
    } ],
    "urls" : [ {
      "indices" : [ 47, 69 ],
      "url" : "http:\/\/t.co\/WLoedvFOLf",
      "expanded_url" : "http:\/\/brainwarsapp.com\/b\/1433116",
      "display_url" : "brainwarsapp.com\/b\/1433116"
    } ]
  },
  "geo" : { },
  "id_str" : "498159773573844992",
  "text" : "\u3044\u307E\u306E\u8133\u529B\u30D1\u30E9\u30E1\u30FC\u30BF\u3067\u3059\uFF01\u307F\u3093\u306A\u3082\u4E00\u7DD2\u306B\u8133\u3092\u935B\u3048\u3088\u3046\uFF01 #BrainWars \u3053\u3093\u306A\u3093\u3089\u3057\u3044 http:\/\/t.co\/WLoedvFOLf http:\/\/t.co\/AmZBlWHiZo",
  "id" : 498159773573844992,
  "created_at" : "2014-08-09 17:32:00 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498151489378869251",
  "geo" : { },
  "id_str" : "498151533880418304",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u3068\u304A\u3044",
  "id" : 498151533880418304,
  "in_reply_to_status_id" : 498151489378869251,
  "created_at" : "2014-08-09 16:59:15 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498151451080671233",
  "text" : "\u4ECA\u5E74\u307E\u3060\u30B9\u30A4\u30AB\u98DF\u3079\u3066\u306A\u3044",
  "id" : 498151451080671233,
  "created_at" : "2014-08-09 16:58:55 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30AD\u30E9\u30AD\u30E9OL",
      "screen_name" : "nyanco15",
      "indices" : [ 0, 9 ],
      "id_str" : "963419196",
      "id" : 963419196
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498151294415028225",
  "geo" : { },
  "id_str" : "498151422567800832",
  "in_reply_to_user_id" : 963419196,
  "text" : "@nyanco15 \u304F\u3060\u3055\u3044",
  "id" : 498151422567800832,
  "in_reply_to_status_id" : 498151294415028225,
  "created_at" : "2014-08-09 16:58:49 +0000",
  "in_reply_to_screen_name" : "nyanco15",
  "in_reply_to_user_id_str" : "963419196",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498119324804980736",
  "geo" : { },
  "id_str" : "498119373962231809",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u5927\u6CB3\u30C9\u30E9\u30DE",
  "id" : 498119373962231809,
  "in_reply_to_status_id" : 498119324804980736,
  "created_at" : "2014-08-09 14:51:28 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498119295734280192",
  "text" : "\u300C\u9154\u3063\u3066\u308B\u3093\u3067\u3059\u3088\u30FC\uFF57\u300D\u3063\u3066\u8A00\u3063\u3066\u308B\u4EBA\u3092\u898B\u308B\u3068\u300C\u304A\u3001\u3088\u3057\u3001\u3058\u3083\u3042\u3082\u3063\u3068\u9154\u308F\u305B\u305F\u3089\u3069\u3046\u306A\u308B\u304B\u8A66\u3057\u3066\u307F\u3088\u3046\uFF01\u300D\u3063\u3066\u5B9F\u9A13\u3057\u305F\u304F\u306A\u308B\u4EBA\u3067\u3059\u3002",
  "id" : 498119295734280192,
  "created_at" : "2014-08-09 14:51:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498118609348984832",
  "text" : "\u5C0F\u8179\u304C\u7A7A\u3044\u305F\u306E\u3067\u3056\u308B\u305D\u3070\u98DF\u3079\u3066\u308B",
  "id" : 498118609348984832,
  "created_at" : "2014-08-09 14:48:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498116991694028800",
  "geo" : { },
  "id_str" : "498118557129904129",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u305D\u3053\u3067\u300E\u8475\u5FB3\u5DDD\u4E09\u4EE3\u300F\u89B3\u308B\u3068\u5370\u8C61\u5909\u308F\u308B\u3002",
  "id" : 498118557129904129,
  "in_reply_to_status_id" : 498116991694028800,
  "created_at" : "2014-08-09 14:48:13 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498116407075164162",
  "geo" : { },
  "id_str" : "498116475014504448",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u305D\u308C\u306F\u9B3C\u6B66\u8005\u3060\u304B\u3089\u304B\uFF57",
  "id" : 498116475014504448,
  "in_reply_to_status_id" : 498116407075164162,
  "created_at" : "2014-08-09 14:39:56 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498116126010650626",
  "geo" : { },
  "id_str" : "498116296714645504",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u79C1\u306F\u5FB3\u5DDD\u6D3E\u3067\u3059\u3051\u3069\u306D",
  "id" : 498116296714645504,
  "in_reply_to_status_id" : 498116126010650626,
  "created_at" : "2014-08-09 14:39:14 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498115415399481344",
  "geo" : { },
  "id_str" : "498115823408390144",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u3069\u3063\u3061\u3082\u53F2\u5B9F\u306B\u57FA\u3065\u304B\u306A\u3044\uFF57\uFF57",
  "id" : 498115823408390144,
  "in_reply_to_status_id" : 498115415399481344,
  "created_at" : "2014-08-09 14:37:21 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498113547058941952",
  "geo" : { },
  "id_str" : "498114528144400384",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u300E\u4FE1\u9577\u5354\u594F\u66F2\u300F\u3092\u8AAD\u3080\u3068\u5149\u79C0\u3044\u3044\u4EBA\u3060\u30FC\u3063\u3066\u601D\u3046",
  "id" : 498114528144400384,
  "in_reply_to_status_id" : 498113547058941952,
  "created_at" : "2014-08-09 14:32:12 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498114255330107392",
  "text" : "\u9014\u4E2D\u307E\u3067\u7D44\u307F\u7ACB\u3066\u3066\u3068\u3044\u3046\u304B\u3080\u3057\u308D\u3042\u3068\u6249\u3064\u3051\u305F\u3089\u5B8C\u4E86\u3050\u3089\u3044\u307E\u3067\u7D44\u307F\u3066\u305F\u6642\u306B\u6C17\u3065\u3044\u305F\u306E\u3067\u5206\u89E3\u3082\u3067\u304D\u306A\u3044\u72B6\u614B\u306A\u3093\u3067\u3059\u3051\u3069\u3001\u3063\u3066\u8A00\u3063\u305F\u3089\u3001\u3058\u3083\u3042\u7D44\u307F\u7ACB\u3066\u6E08\u307F\u3068\u4EA4\u63DB\u3057\u307E\u3059\u306D\u3063\u3066\u306A\u3063\u3066\u3001\u30CB\u30C8\u30EA\u3059\u3054\u3044\u306A\u30FC\u3063\u3066\u601D\u3044\u307E\u3057\u305F\u3002",
  "id" : 498114255330107392,
  "created_at" : "2014-08-09 14:31:07 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498114031660457985",
  "text" : "\u660E\u65E5\u7814\u7A76\u3059\u308B\u524D\u306B\u3001\u305D\u3046\u3044\u3048\u3070\u5348\u524D\u4E2D\u306B\u4E0D\u826F\u54C1\u4EA4\u63DB\u306E\u98DF\u5668\u68DA\u304C\u5C4A\u304F\u3093\u3060\u3063\u305F\u3002",
  "id" : 498114031660457985,
  "created_at" : "2014-08-09 14:30:14 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498113914693898240",
  "text" : "\u4ECA\u65E5\u305A\u3063\u3068\u30EA\u30D5\u30EC\u30AF\u3057\u3066\u305F\u304B\u3089\u4E45\u3057\u3076\u308A\u306B\u3064\u75B2\u308C\u305F",
  "id" : 498113914693898240,
  "created_at" : "2014-08-09 14:29:46 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 50, 63 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498113726264782848",
  "text" : "\u3068\u3044\u3046\u308F\u3051\u3067\u7121\u4E8B\u5E30\u5B85\u3057\u307E\u3057\u305F\u3002\u307F\u306A\u3055\u307E\u4ECA\u65E5\u306F\u697D\u3057\u304B\u3063\u305F\u3067\u3059\u3001\u307E\u305F\u3069\u3053\u304B\u3067\u304A\u4F1A\u3044\u3067\u304D\u308B\u3068\u5B09\u3057\u3044\u3067\u3059\uFF01 @caori_knight \u3055\u3093\u306F\u3001\u540D\u53E4\u5C4B\u3068\u304B\u3067\u3082\u304A\u4F1A\u3044\u3057\u307E\u3057\u3087\u3046\uFF01",
  "id" : 498113726264782848,
  "created_at" : "2014-08-09 14:29:01 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\" rel=\"nofollow\"\u003ETwitter Web Client\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4F53\u8102\u80AA\u738733.6%\u30B4\u30EA\u30E9(\u00B4\uFF1B\u03C9\uFF1B`)",
      "screen_name" : "rainbow410",
      "indices" : [ 0, 11 ],
      "id_str" : "88924631",
      "id" : 88924631
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498074754557816833",
  "geo" : { },
  "id_str" : "498113330712551425",
  "in_reply_to_user_id" : 88924631,
  "text" : "@rainbow410 \u3042\u308C\u9154\u3063\u3066\u305F\u3093\u3067\u3059\u304B\uFF57",
  "id" : 498113330712551425,
  "in_reply_to_status_id" : 498074754557816833,
  "created_at" : "2014-08-09 14:27:27 +0000",
  "in_reply_to_screen_name" : "rainbow410",
  "in_reply_to_user_id_str" : "88924631",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/498105930647478272\/photo\/1",
      "indices" : [ 9, 31 ],
      "url" : "http:\/\/t.co\/B245XNXiZs",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BumgtGPCUAAwf_W.jpg",
      "id_str" : "498105930429386752",
      "id" : 498105930429386752,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BumgtGPCUAAwf_W.jpg",
      "sizes" : [ {
        "h" : 451,
        "resize" : "fit",
        "w" : 370
      }, {
        "h" : 451,
        "resize" : "fit",
        "w" : 370
      }, {
        "h" : 451,
        "resize" : "fit",
        "w" : 370
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 414,
        "resize" : "fit",
        "w" : 340
      } ],
      "display_url" : "pic.twitter.com\/B245XNXiZs"
    } ],
    "hashtags" : [ {
      "text" : "\u30B7\u30DE\u30EA\u30B9",
      "indices" : [ 3, 8 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498105930647478272",
  "text" : "\u6BDB\u7389 #\u30B7\u30DE\u30EA\u30B9 http:\/\/t.co\/B245XNXiZs",
  "id" : 498105930647478272,
  "created_at" : "2014-08-09 13:58:02 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498101724607827968",
  "text" : "\u660E\u65E5\u3053\u305D\u7814\u7A76\u3057\u3088\u3046",
  "id" : 498101724607827968,
  "created_at" : "2014-08-09 13:41:20 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 18, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498101329286279168",
  "text" : "\u305F\u3068\u3048\u3070\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498101329286279168,
  "created_at" : "2014-08-09 13:39:45 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 17, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498100480753430528",
  "text" : "\u30D7\u30EC\u30FC\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498100480753430528,
  "created_at" : "2014-08-09 13:36:23 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 17, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498099542873497601",
  "text" : "\u79C1\u305F\u3061\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498099542873497601,
  "created_at" : "2014-08-09 13:32:39 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 20, 34 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498097547273981952",
  "text" : "\u30B0\u30EB\u30FC\u30F4\u30A3\u30F3\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498097547273981952,
  "created_at" : "2014-08-09 13:24:44 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498096619837878273",
  "text" : "\u5BBF\u984C\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498096619837878273,
  "created_at" : "2014-08-09 13:21:03 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498096223723610115",
  "text" : "\u30C8\u30EA\u30EB\u82E6\u624B\u306A\u3093\u3058\u3083\u30FC\uFF01\uFF01",
  "id" : 498096223723610115,
  "created_at" : "2014-08-09 13:19:28 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498095847347736576",
  "text" : "\u4E00\u66F2\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498095847347736576,
  "created_at" : "2014-08-09 13:17:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498095570494320641",
  "text" : "\u516D\u6BB5\u3082\u4E03\u6BB5\u30822\u66F2\u76EE\u304C\u82E6\u624B\u3067\u53D7\u304B\u3089\u306A\u3044\u304B\u3089\u30EA\u30D5\u30EC\u30AF\u30A8\u30F3\u30B8\u30E7\u30A4\u52E2",
  "id" : 498095570494320641,
  "created_at" : "2014-08-09 13:16:52 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498094839200632833",
  "text" : "\u4E88\u611F\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498094839200632833,
  "created_at" : "2014-08-09 13:13:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u307F",
      "screen_name" : "yu_minpg",
      "indices" : [ 0, 9 ],
      "id_str" : "40223746",
      "id" : 40223746
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498091645657559041",
  "geo" : { },
  "id_str" : "498093345751564288",
  "in_reply_to_user_id" : 40223746,
  "text" : "@yu_minpg \u307F\u306A\u3044\u3067\u3059\u3088\u3002\u3002\uFF08\u3046\u3061\u306F",
  "id" : 498093345751564288,
  "in_reply_to_status_id" : 498091645657559041,
  "created_at" : "2014-08-09 13:08:02 +0000",
  "in_reply_to_screen_name" : "yu_minpg",
  "in_reply_to_user_id_str" : "40223746",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498092975549730816",
  "text" : "\uFF18\u6708\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498092975549730816,
  "created_at" : "2014-08-09 13:06:34 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498092442373996546",
  "text" : "\u796D\u308A\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498092442373996546,
  "created_at" : "2014-08-09 13:04:27 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498091716809736192",
  "text" : "\u30EA\u30D5\u30EC\u30AF\u3082\u3046\u4E00\u53F0\u3069\u3053\u884C\u3063\u305F\u304B\u3068\u601D\u3063\u305F\u3089\u5F8C\u308D\u306B\u79FB\u52D5\u3057\u3066\u305F",
  "id" : 498091716809736192,
  "created_at" : "2014-08-09 13:01:34 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 15, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498085206855647233",
  "text" : "\u6D77\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498085206855647233,
  "created_at" : "2014-08-09 12:35:41 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3086\u304B\u305F\u305D",
      "screen_name" : "yucataso",
      "indices" : [ 0, 9 ],
      "id_str" : "67385537",
      "id" : 67385537
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "498081614568513536",
  "geo" : { },
  "id_str" : "498084355722334208",
  "in_reply_to_user_id" : 67385537,
  "text" : "@yucataso \u305D\u3046\u3044\u3046\u6642\u306F\u98F2\u3080\u306B\u9650\u308B\u306E\u3067\u306F",
  "id" : 498084355722334208,
  "in_reply_to_status_id" : 498081614568513536,
  "created_at" : "2014-08-09 12:32:19 +0000",
  "in_reply_to_screen_name" : "yucataso",
  "in_reply_to_user_id_str" : "67385537",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 17, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498083910740242432",
  "text" : "\u9762\u767D\u3044\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498083910740242432,
  "created_at" : "2014-08-09 12:30:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 19, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498083268894277634",
  "text" : "\u304A\u6C17\u306B\u5165\u308A\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498083268894277634,
  "created_at" : "2014-08-09 12:27:59 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 19, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498082359409774593",
  "text" : "\u307E\u305F\u30EA\u30D5\u30EC\u30AF\uFF01 \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498082359409774593,
  "created_at" : "2014-08-09 12:24:23 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 50, 73 ],
      "url" : "https:\/\/t.co\/5k6FBZWRAv",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53e610d5498eb560515c318a?s=-CFZ3iWMxq9QWwtsAOkT4-f2scs&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "498080072923095041",
  "text" : "\u3072\u307E\u3064\u3076\u3057\u30FC (@ \u30BF\u30A4\u30C8\u30FC\u30B9\u30C6\u30FC\u30B7\u30E7\u30F3 \u6E0B\u8C37\u5E97 in \u6E0B\u8C37\u533A, \u6771\u4EAC\u90FD w\/ 2 others) https:\/\/t.co\/5k6FBZWRAv",
  "id" : 498080072923095041,
  "created_at" : "2014-08-09 12:15:17 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498079604779667458",
  "text" : "\u3059\u305A\u3057\u3044\u306D\u30FC",
  "id" : 498079604779667458,
  "created_at" : "2014-08-09 12:13:26 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498075679959707648",
  "text" : "\u5065\u5EB7\u7684\u306A\u6642\u9593\u306B\u89E3\u6563\u3057\u305F\u306E\u3067\u3001\u6E0B\u8C37\u3067\u3082\u884C\u304F\u304B\u3002",
  "id" : 498075679959707648,
  "created_at" : "2014-08-09 11:57:50 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498028386908782592",
  "text" : "\u308A\u306A\uFF08\u3063\u304F\u3059\uFF09\u4F1A",
  "id" : 498028386908782592,
  "created_at" : "2014-08-09 08:49:55 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498025842694311937",
  "text" : "\u30BB\u30DF\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498025842694311937,
  "created_at" : "2014-08-09 08:39:48 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 17, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498024932119298048",
  "text" : "\u30E9\u30A4\u30C8\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498024932119298048,
  "created_at" : "2014-08-09 08:36:11 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 18, 32 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498023124873379840",
  "text" : "\u306A\u3093\u3068\u304B\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498023124873379840,
  "created_at" : "2014-08-09 08:29:00 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498022131167293440",
  "text" : "\u524D\u306E\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498022131167293440,
  "created_at" : "2014-08-09 08:25:03 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498021275181142017",
  "text" : "\u52C7\u6C17\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498021275181142017,
  "created_at" : "2014-08-09 08:21:39 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 17, 31 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498021013834047488",
  "text" : "\u5915\u66AE\u308C\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498021013834047488,
  "created_at" : "2014-08-09 08:20:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 15, 29 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498018699513585664",
  "text" : "\u6D77\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498018699513585664,
  "created_at" : "2014-08-09 08:11:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498017805292142593",
  "text" : "\u6691\u3044\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498017805292142593,
  "created_at" : "2014-08-09 08:07:52 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 16, 30 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498017033120804864",
  "text" : "\u71B1\u3044\u30BB\u30DF \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498017033120804864,
  "created_at" : "2014-08-09 08:04:48 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 19, 33 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498016775414358016",
  "text" : "\u4ECA\u65E5\u306F\u30BB\u30DF\u6311\u6226 \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498016775414358016,
  "created_at" : "2014-08-09 08:03:46 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498012460276645889",
  "text" : "\u6885\u96EA\u591C\u30B9\u30DA\u304C\u843D\u3061\u305F",
  "id" : 498012460276645889,
  "created_at" : "2014-08-09 07:46:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/eagate.573.jp\/game\/reflec\/groovin\/\" rel=\"nofollow\"\u003EREFLEC  BEAT groovin'!! AC\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ {
      "text" : "REFLECBEAT_AC",
      "indices" : [ 22, 36 ]
    } ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "498011790182068224",
  "text" : "\u4ECA\u65E5\u306F\u304A\u76C6\u3067\u3059\u306D\uFF1F\uFF1F \u3010\uFF21\uFF2F\u2212\uFF2B\uFF2D\uFF32\uFF35\uFF34\u3011 #REFLECBEAT_AC",
  "id" : 498011790182068224,
  "created_at" : "2014-08-09 07:43:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 62, 85 ],
      "url" : "https:\/\/t.co\/7XEC4aRwlG",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53e5c1ad498e71e3b00189e3?s=hVmY9x00wjj44UkcrP-tWk1Nu1Q&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "497995080150835200",
  "text" : "I've (@ \u30BF\u30A4\u30C8\u30FC\u30B9\u30C6\u30FC\u30B7\u30E7\u30F3 \u65B0\u5BBF\u5357\u53E3\u30B2\u30FC\u30E0\u30EF\u30FC\u30EB\u30C9\u5E97 in Shinjuku, \u6771\u4EAC\u90FD w\/ 6 others) https:\/\/t.co\/7XEC4aRwlG",
  "id" : 497995080150835200,
  "created_at" : "2014-08-09 06:37:34 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 35, 58 ],
      "url" : "https:\/\/t.co\/PApjLfM3jm",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53e5bd5c11d2587e16023700?s=LonlbKoooKeHizpkbJXz6OJmCJQ&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "497990442185936896",
  "text" : "I'm at \u67FF\u5B89 \u4E09\u5C3A\u4E09\u5BF8\u7BB8 \u30EB\u30DF\u30CD\u65B0\u5BBF\u5E97 in \u65B0\u5BBF\u533A, \u6771\u4EAC\u90FD https:\/\/t.co\/PApjLfM3jm",
  "id" : 497990442185936896,
  "created_at" : "2014-08-09 06:19:08 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 58, 81 ],
      "url" : "https:\/\/t.co\/6YfVc2uJIL",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53e5a597498e6d624508bbac?s=vBQpUPp_TFxgmXFQYbGfZEujP9s&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "497964920580108289",
  "text" : "\u3068\u308A\u3042\u3048\u305A\u3059\u308F\u30FC (@ \u65B0\u5BBF\u99C5 (Shinjuku Sta.) in \u65B0\u5BBF\u533A, \u6771\u4EAC\u90FD w\/ 47 others) https:\/\/t.co\/6YfVc2uJIL",
  "id" : 497964920580108289,
  "created_at" : "2014-08-09 04:37:43 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304B\u304A\u308A\u306A",
      "screen_name" : "caori_knight",
      "indices" : [ 8, 21 ],
      "id_str" : "140781133",
      "id" : 140781133
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497964027826286593",
  "text" : "\u897F\u53E3\u6539\u672D\u524D\u306B\u306F @caori_knight \u3055\u3093\u306E\u59FF\u304C\u306A\u3044",
  "id" : 497964027826286593,
  "created_at" : "2014-08-09 04:34:10 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497959625304453122",
  "text" : "25\u5206\u65B0\u5BBF\u7740\u4E88\u5B9A\u3067\u3059\u3002",
  "id" : 497959625304453122,
  "created_at" : "2014-08-09 04:16:40 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E0D\u60D1\u306E\u72E9\u4EBA@\u9154\u3044\u3069\u308C\u8266\u968A",
      "screen_name" : "tosca_fuwaku",
      "indices" : [ 0, 13 ],
      "id_str" : "325487678",
      "id" : 325487678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497834774178693120",
  "geo" : { },
  "id_str" : "497941196782567424",
  "in_reply_to_user_id" : 325487678,
  "text" : "@tosca_fuwaku \u3067\u3059\u3088\u306D\u3047\u3002",
  "id" : 497941196782567424,
  "in_reply_to_status_id" : 497834774178693120,
  "created_at" : "2014-08-09 03:03:27 +0000",
  "in_reply_to_screen_name" : "tosca_fuwaku",
  "in_reply_to_user_id_str" : "325487678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497819683039371266",
  "text" : "\u30A6\u30A3\u30AD\u30E1\u30C7\u30A3\u30A2\u304C\u56DE\u3063\u3066\u3044\u308B\u306E\u3092\u8003\u3048\u308C\u3070\u3001\u7269\u7406\u7684\u306B\u30B5\u30FC\u30D0\u30FC\u6A5F\u5668\u304C\u4F55\u53F0\u3042\u3063\u3066\u3069\u3046\u3053\u3046\u3068\u3044\u3046\u306E\u3068\u3001\u5B9F\u969B\u306B\u4F5C\u696D\u3067\u304D\u308B\u4EBA\u304C\u4F55\u4EBA\u3044\u308B\u304B\u3063\u3066\u3001\u3042\u307E\u308A\u95A2\u4FC2\u306A\u3044\u3093\u3060\u308D\u3046\u306A\u3068\u601D\u3046\u3002",
  "id" : 497819683039371266,
  "created_at" : "2014-08-08 19:00:36 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307D\u3093\u3053\u3064@Lv13",
      "screen_name" : "ponkotuy",
      "indices" : [ 0, 9 ],
      "id_str" : "84240596",
      "id" : 84240596
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497817857875382272",
  "geo" : { },
  "id_str" : "497819019890528256",
  "in_reply_to_user_id" : 84240596,
  "text" : "@ponkotuy \u30A6\u30A3\u30AD\u30E1\u30C7\u30A3\u30A2\u3082\u7269\u7406\u30B5\u30FC\u30D0\u30FC\u3068\u3066\u3082\u3064\u3082\u306A\u3044\u3067\u3059\u304C\u5B9F\u969B\u306E\u4F5C\u696D\u54E1\u306F20\u4EBA\u3044\u306A\u3044\u3050\u3089\u3044\u3067\u3059\u3088\u3002\u534A\u5206\u4EE5\u4E0A\u30DC\u30E9\u30F3\u30C6\u30A3\u30A2\u3067\u3059\u3057\u3002",
  "id" : 497819019890528256,
  "in_reply_to_status_id" : 497817857875382272,
  "created_at" : "2014-08-08 18:57:58 +0000",
  "in_reply_to_screen_name" : "ponkotuy",
  "in_reply_to_user_id_str" : "84240596",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497769789519978496",
  "text" : "\u30AA\u30D5\u4F1A\u3063\u3066\u9298\u6253\u305F\u308C\u3066\u308B\u30AA\u30D5\u4F1A\u3068\u304B\u4E45\u3057\u3076\u308A\u306A\u306E\u3067\u308F\u304F\u308F\u304F\u3057\u3066\u3044\u308B\u3002",
  "id" : 497769789519978496,
  "created_at" : "2014-08-08 15:42:20 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497769539912749056",
  "text" : "\u660E\u65E5\u306F\u306A\u3093\u3082\u306A\u3044\u6C17\u3067\u3044\u305F\u3051\u3069\u666E\u901A\u306B\u30AA\u30D5\u4F1A\u3060\u3063\u305F\u3002",
  "id" : 497769539912749056,
  "created_at" : "2014-08-08 15:41:21 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497768616058552320",
  "text" : "x\u2192\u221E\u306E\u6642\u306Bf(x)\u304C\u767A\u6563\u3059\u308B\u3051\u3069\u3001\u7A4D\u5206\u304C\u53CE\u675F\u3059\u308B\u3088\u3046\u306A\u95A2\u6570\u3063\u3066\u3001\u5B58\u5728\u3059\u308B\u306E\u3060\u3063\u3051\uFF1F",
  "id" : 497768616058552320,
  "created_at" : "2014-08-08 15:37:40 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497691476235517952",
  "text" : "\u30D1\u30FC\u30C6\u30A3\u30FC\u306B\u884C\u304F\u306E\u306F\u8AE6\u3081\u305F\u306E\u3067\u3053\u306E\u8FBA\u3067\u98F2\u3080\u3057\u304B\u306A\u3044",
  "id" : 497691476235517952,
  "created_at" : "2014-08-08 10:31:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497566349468520448",
  "text" : "\u4E09\u91CD\u6F01\u9023\u4F1A\u9577\u306E\u767A\u8A00\u3068\u304B\u3001\u305D\u3046\u3044\u3046\u306E\u898B\u308B\u305F\u3073\u306B\u3001\u307B\u3093\u3068\u306B\u8A00\u8449\u306E\u4E00\u90E8\u3092\u53D6\u308A\u51FA\u3057\u3066\u305D\u3053\u3057\u304B\u898B\u306A\u3044\u4EBA\u305F\u3061\u591A\u3044\u3088\u306A\u3041\u3063\u3066\u843D\u80C6\u3059\u308B\u3002\u305D\u308C\u3067\u8AB0\u3082\u5E78\u305B\u306B\u306A\u3093\u3066\u306A\u308C\u306A\u3044\u306E\u306B\u3001\u306A\u3093\u3067\u305D\u3093\u306A\u3053\u3068\u3057\u305F\u3044\u3093\u3060\u308D\u3046\u3002",
  "id" : 497566349468520448,
  "created_at" : "2014-08-08 02:13:56 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u307B\u3089\u3075\u304D",
      "screen_name" : "n0ixe",
      "indices" : [ 0, 6 ],
      "id_str" : "140231677",
      "id" : 140231677
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497558761255350272",
  "geo" : { },
  "id_str" : "497560612243640320",
  "in_reply_to_user_id" : 140231677,
  "text" : "@n0ixe \u65C5\u884C\u884C\u3053\u3046\uFF01",
  "id" : 497560612243640320,
  "in_reply_to_status_id" : 497558761255350272,
  "created_at" : "2014-08-08 01:51:08 +0000",
  "in_reply_to_screen_name" : "n0ixe",
  "in_reply_to_user_id_str" : "140231677",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497557883798560768",
  "text" : "\u6700\u8FD1\u305A\u3063\u3068\u671D\u8ABF\u5B50\u60AA\u3044\u306A\u30FC\u3002\u3002\u30AF\u30FC\u30E9\u30FC\u306E\u305B\u3044\u304B\u306A\u3002",
  "id" : 497557883798560768,
  "created_at" : "2014-08-08 01:40:18 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497404046127284224",
  "geo" : { },
  "id_str" : "497404114523787267",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u3044\u307E\u306B\u3050\u3089\u3044",
  "id" : 497404114523787267,
  "in_reply_to_status_id" : 497404046127284224,
  "created_at" : "2014-08-07 15:29:16 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u10E0\u10DD\u10D1\u10D4\u10E0\u10E2\u10D0\u10E1\u10D9 ",
      "screen_name" : "robertask",
      "indices" : [ 0, 10 ],
      "id_str" : "86671709",
      "id" : 86671709
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497403281061064704",
  "geo" : { },
  "id_str" : "497403370148081664",
  "in_reply_to_user_id" : 86671709,
  "text" : "@robertask \u3093\u30FC\u3001\u306A\u3093\u3067\u3067\u3057\u3087\u3046\u306D\u3002\u9ED2\u3044\u304B\u3089\uFF1F",
  "id" : 497403370148081664,
  "in_reply_to_status_id" : 497403281061064704,
  "created_at" : "2014-08-07 15:26:19 +0000",
  "in_reply_to_screen_name" : "robertask",
  "in_reply_to_user_id_str" : "86671709",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u10E0\u10DD\u10D1\u10D4\u10E0\u10E2\u10D0\u10E1\u10D9 ",
      "screen_name" : "robertask",
      "indices" : [ 0, 10 ],
      "id_str" : "86671709",
      "id" : 86671709
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497401862530682881",
  "geo" : { },
  "id_str" : "497402924142583809",
  "in_reply_to_user_id" : 86671709,
  "text" : "@robertask \u3044\u307E\u3044\u3061\u30FB\u30FB\u30FB",
  "id" : 497402924142583809,
  "in_reply_to_status_id" : 497401862530682881,
  "created_at" : "2014-08-07 15:24:33 +0000",
  "in_reply_to_screen_name" : "robertask",
  "in_reply_to_user_id_str" : "86671709",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4E0D\u60D1\u306E\u72E9\u4EBA@\u9154\u3044\u3069\u308C\u8266\u968A",
      "screen_name" : "tosca_fuwaku",
      "indices" : [ 0, 13 ],
      "id_str" : "325487678",
      "id" : 325487678
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497380089470779392",
  "geo" : { },
  "id_str" : "497387716212822019",
  "in_reply_to_user_id" : 325487678,
  "text" : "@tosca_fuwaku \u3060\u304B\u3089\u3061\u3083\u3093\u3068\u3059\u308C\u3070\u3044\u3044\u3063\u3066\u8A00\u3063\u305F\u3068\u304A\u308A\u3067\u3059\u304C\uFF1F",
  "id" : 497387716212822019,
  "in_reply_to_status_id" : 497380089470779392,
  "created_at" : "2014-08-07 14:24:07 +0000",
  "in_reply_to_screen_name" : "tosca_fuwaku",
  "in_reply_to_user_id_str" : "325487678",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30CF\u30EB",
      "screen_name" : "_haru_",
      "indices" : [ 13, 20 ],
      "id_str" : "15616888",
      "id" : 15616888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497017542670426112",
  "geo" : { },
  "id_str" : "497387622155575297",
  "in_reply_to_user_id" : 445958218,
  "text" : "@hiochuu \u3058\u3083\u3042 @_haru_ \u30683\u4EBA\u3067\u3059\u306D\uFF01",
  "id" : 497387622155575297,
  "in_reply_to_status_id" : 497017542670426112,
  "created_at" : "2014-08-07 14:23:44 +0000",
  "in_reply_to_screen_name" : "hio87bi",
  "in_reply_to_user_id_str" : "445958218",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497378256241168387",
  "text" : "\u6587\u7CFBSE\uFF08\uFF09\u306E\u554F\u984C\u70B9\u306F\u6280\u8853\u529B\u306E\u306A\u3055\u3092\u8A8D\u3081\u305A\u306B\u52C9\u5F37\u3082\u3057\u3088\u3046\u3068\u3057\u306A\u3044\u3068\u3053\u308D\u306A\u306E\u3067\u3001\u3061\u3083\u3093\u3068\u4ECA\u304B\u3089\u3067\u3082\u6280\u8853\u529B\u8EAB\u306B\u4ED8\u3051\u308C\u3070\u3044\u3044\u3093\u3058\u3083\u306A\u3044\u3067\u3057\u3087\u3046\u304B\u3002",
  "id" : 497378256241168387,
  "created_at" : "2014-08-07 13:46:31 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30CF\u30EB",
      "screen_name" : "_haru_",
      "indices" : [ 0, 7 ],
      "id_str" : "15616888",
      "id" : 15616888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497373746823303168",
  "geo" : { },
  "id_str" : "497374202190516225",
  "in_reply_to_user_id" : 15616888,
  "text" : "@_haru_ \u30C0\u30F3\u30EC\u30DC\u52E2\u304A\u3081\u3067\u3068\u3046",
  "id" : 497374202190516225,
  "in_reply_to_status_id" : 497373746823303168,
  "created_at" : "2014-08-07 13:30:25 +0000",
  "in_reply_to_screen_name" : "_haru_",
  "in_reply_to_user_id_str" : "15616888",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497341286689431552",
  "text" : "\u98DF\u5668\u68DA\u7D44\u307F\u7ACB\u3066\u3066\u305F\u3089\u3001\u5FC5\u8981\u306A\u3068\u3053\u308D\u306B\u7A74\u304C\u7A7A\u3044\u3066\u306A\u3044\u4E0D\u826F\u54C1\u3060\u3063\u305F\u305F\u3081\u3001\u4F5C\u696D\u4E2D\u65AD\u3057\u307E\u3059\u3002\u4EBA\u751F2\u56DE\u3081\u306E\u7D44\u307F\u7ACB\u3066\u5BB6\u5177\u306E\u4E0D\u826F\u54C1\uFF081\u56DE\u3081\u306F\u7A74\u306E\u4F4D\u7F6E\u304C\u660E\u3089\u304B\u306B\u305A\u308C\u3066\u305F\uFF09\u3002",
  "id" : 497341286689431552,
  "created_at" : "2014-08-07 11:19:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497325855782096896",
  "text" : "\u98DF\u5668\u68DA\u3050\u3089\u3044\u4F5C\u3063\u3066\u304A\u3053\u3046",
  "id" : 497325855782096896,
  "created_at" : "2014-08-07 10:18:18 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497325816158511104",
  "text" : "\u306A\u3093\u3082\u3057\u3066\u306A\u30FC",
  "id" : 497325816158511104,
  "created_at" : "2014-08-07 10:18:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497325794947891200",
  "text" : "\u5B8C\u5168\u306B\u5BDD\u3066\u305F",
  "id" : 497325794947891200,
  "created_at" : "2014-08-07 10:18:04 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497276182589480961",
  "text" : "\u6691\u304F\u3066\u3060\u308C\u3066\u308B",
  "id" : 497276182589480961,
  "created_at" : "2014-08-07 07:00:55 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497269892777648130",
  "text" : "\u3080\u3057\u308D\u5272\u3063\u3066\u304F\u308C\u308B\u3068\u8AE6\u3081\u3066\u65B0\u3057\u3044\u306E\u3092\u8CB7\u304A\u3046\u3068\u3059\u308B\u306E\u3067\u8AB0\u304B\u5272\u3063\u3066\u304F\u3060\u3055\u3044\u3002",
  "id" : 497269892777648130,
  "created_at" : "2014-08-07 06:35:55 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497269563960999936",
  "text" : "\u3053\u306E\u30B1\u30A4\u30BF\u30A4\u3001\u3059\u3050\u306B\u767A\u71B1\u3057\u3066\u51E6\u7406\u80FD\u529B\u843D\u3061\u308B\u304B\u3089\u3001\u3068\u3066\u3082\u30EC\u30B9\u30DD\u30F3\u30B9\u60AA\u304F\u306A\u308B\u3057\u3001\u305D\u308C\u3067\u6F22\u5B57\u5909\u63DB\u3068\u304B\u304C\u8FFD\u3044\u3064\u304B\u305A\u306B\u3059\u3054\u3044typo\u3059\u308B\u306E\u3084\u3081\u3066\u307B\u3057\u3044\u3057\u3001\u5272\u308A\u305F\u3044\u3002",
  "id" : 497269563960999936,
  "created_at" : "2014-08-07 06:34:37 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497047192364797952",
  "geo" : { },
  "id_str" : "497047609979072512",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei \u30E1\u30AC\u5358\u4F4D\u306E\u304A\u91D1\u306F\u4F7F\u3063\u305F\u3053\u3068\u306A\u3044\u306E\u3067",
  "id" : 497047609979072512,
  "in_reply_to_status_id" : 497047192364797952,
  "created_at" : "2014-08-06 15:52:39 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    }, {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 9, 20 ],
      "id_str" : "115981748",
      "id" : 115981748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497046689547419648",
  "geo" : { },
  "id_str" : "497046798377033728",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei @ttata_trit \u3061\u3087\u3063\u3068\u306E\u3088\u3055\u3093\u30FB\u30FB\u30FB",
  "id" : 497046798377033728,
  "in_reply_to_status_id" : 497046689547419648,
  "created_at" : "2014-08-06 15:49:26 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497046572593471488",
  "text" : "\u7DD1\u5316\u3057\u3066\u30BB\u30EB\u30ED\u30DF\u30F3\u56DE\u53CE\u3057\u3088\u3046",
  "id" : 497046572593471488,
  "created_at" : "2014-08-06 15:48:32 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497046163959185409",
  "text" : "\u5510\u7A81\u306B\u5DFB\u304D\u8FBC\u307E\u308C\u308B\u3086\u30FC\u3068\u304F\u3093",
  "id" : 497046163959185409,
  "created_at" : "2014-08-06 15:46:54 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3076\u3093\u3061\u3087\u3046",
      "screen_name" : "yutopp",
      "indices" : [ 4, 11 ],
      "id_str" : "51032597",
      "id" : 51032597
    }, {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 28, 39 ],
      "id_str" : "115981748",
      "id" : 115981748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497045601855348736",
  "geo" : { },
  "id_str" : "497045991678164992",
  "in_reply_to_user_id" : 115981748,
  "text" : "\u80A5\u6599\u306B @yutopp \u304C\u306A\u3044\u3068\u80B2\u305F\u306A\u3044\u306E\u3067\u6CE8\u610F QT @ttata_trit: \u3042\u304A\u3055\u3093\u57CB\u3081\u3066\u305D\u3053\u306Bgumonji\u306E\u6728\u3092\u690D\u3048\u3088\u3046",
  "id" : 497045991678164992,
  "in_reply_to_status_id" : 497045601855348736,
  "created_at" : "2014-08-06 15:46:13 +0000",
  "in_reply_to_screen_name" : "ttata_trit",
  "in_reply_to_user_id_str" : "115981748",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    }, {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 9, 20 ],
      "id_str" : "115981748",
      "id" : 115981748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497045222837080064",
  "geo" : { },
  "id_str" : "497045390684737537",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei @ttata_trit \u8ECA\u6301\u3061\u306E\u4EBA\u304C\u306A\u306B\u3092\u304A\u3063\u3057\u3083\u308B\u30FB\u30FB\u30FB",
  "id" : 497045390684737537,
  "in_reply_to_status_id" : 497045222837080064,
  "created_at" : "2014-08-06 15:43:50 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497044558526439425",
  "geo" : { },
  "id_str" : "497045065827487744",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei \u30EA\u30A2\u5145\u5EA6\u306F\u3001\u9AD8\u3044\u9806\u306B\u3063\u305F\u305F\uFF1EKei-Kei\uFF1E\uFF1E\u79C1\u306A\u306E\u3067",
  "id" : 497045065827487744,
  "in_reply_to_status_id" : 497044558526439425,
  "created_at" : "2014-08-06 15:42:33 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 0, 11 ],
      "id_str" : "115981748",
      "id" : 115981748
    }, {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 12, 20 ],
      "id_str" : "6529602",
      "id" : 6529602
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497043923714322432",
  "geo" : { },
  "id_str" : "497044492499709952",
  "in_reply_to_user_id" : 115981748,
  "text" : "@ttata_trit @Kei_Kei \u3063\u305F\u305F\u304F\u3093\u30EA\u30A2\u5145\u306A\u306E\u306F\u8AB0\u3082\u304C\u8A8D\u3081\u308B",
  "id" : 497044492499709952,
  "in_reply_to_status_id" : 497043923714322432,
  "created_at" : "2014-08-06 15:40:16 +0000",
  "in_reply_to_screen_name" : "ttata_trit",
  "in_reply_to_user_id_str" : "115981748",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "Kei-Kei",
      "screen_name" : "Kei_Kei",
      "indices" : [ 0, 8 ],
      "id_str" : "6529602",
      "id" : 6529602
    }, {
      "name" : "\u305F\u306B\u3063\u305F\u3055\u3093",
      "screen_name" : "ttata_trit",
      "indices" : [ 9, 20 ],
      "id_str" : "115981748",
      "id" : 115981748
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497032565916585984",
  "geo" : { },
  "id_str" : "497041804428976128",
  "in_reply_to_user_id" : 6529602,
  "text" : "@Kei_Kei @ttata_trit \u30C4\u30FC\u30B7\u30E7\u30C3\u30C8\u671F\u5F85\u3057\u3066\u308B",
  "id" : 497041804428976128,
  "in_reply_to_status_id" : 497032565916585984,
  "created_at" : "2014-08-06 15:29:35 +0000",
  "in_reply_to_screen_name" : "Kei_Kei",
  "in_reply_to_user_id_str" : "6529602",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497027556080488448",
  "text" : "\u3059\u3044\u307E\u305B\u3093\u3059\u3044\u307E\u305B\u3093ParaView\u3067\u304D\u308B\u3060\u306A\u3093\u3066\u8A00\u308F\u306A\u3044\u306E\u3067\u77F3\u6295\u3052\u306A\u3044\u3067\u304F\u3060\u3055\u3044",
  "id" : 497027556080488448,
  "created_at" : "2014-08-06 14:32:58 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3042\u3059\u307F\u3093",
      "screen_name" : "an_asumin",
      "indices" : [ 0, 10 ],
      "id_str" : "202087794",
      "id" : 202087794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497026885851701250",
  "geo" : { },
  "id_str" : "497026944240611328",
  "in_reply_to_user_id" : 202087794,
  "text" : "@an_asumin ParaView\u306A\u3089\uFF01\uFF08\u65B9\u5411\u6027\u304C\u305A\u308C\u3066\u3044\u308B\uFF09",
  "id" : 497026944240611328,
  "in_reply_to_status_id" : 497026885851701250,
  "created_at" : "2014-08-06 14:30:32 +0000",
  "in_reply_to_screen_name" : "an_asumin",
  "in_reply_to_user_id_str" : "202087794",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497026853253558272",
  "text" : "Blender\u3001\u4E00\u6642\u671F\u3001\u30D7\u30EA\u30D7\u30ED\u30BB\u30C3\u30B5\u4EE3\u308F\u308A\u306B\u4F7F\u3063\u3066\u305F\u3051\u3069\u3001\u7D50\u5C40\u3001\u3082\u3046\u624B\u3067for\u56DE\u3057\u3066\u7C92\u5B50\u914D\u7F6E\u3059\u308B\u307B\u3046\u304C\u697D\u3060\u3063\u3066\u6C17\u3065\u3044\u305F\u3002",
  "id" : 497026853253558272,
  "created_at" : "2014-08-06 14:30:10 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3042\u3059\u307F\u3093",
      "screen_name" : "an_asumin",
      "indices" : [ 0, 10 ],
      "id_str" : "202087794",
      "id" : 202087794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497026611909111808",
  "geo" : { },
  "id_str" : "497026680695689216",
  "in_reply_to_user_id" : 202087794,
  "text" : "@an_asumin Blender\u306F\u305D\u3093\u306A\u306B\u4F7F\u3048\u306A\u3044\u30FB\u30FB\u30FB",
  "id" : 497026680695689216,
  "in_reply_to_status_id" : 497026611909111808,
  "created_at" : "2014-08-06 14:29:29 +0000",
  "in_reply_to_screen_name" : "an_asumin",
  "in_reply_to_user_id_str" : "202087794",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3042\u3059\u307F\u3093",
      "screen_name" : "an_asumin",
      "indices" : [ 0, 10 ],
      "id_str" : "202087794",
      "id" : 202087794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497026361341378560",
  "geo" : { },
  "id_str" : "497026432459997186",
  "in_reply_to_user_id" : 202087794,
  "text" : "@an_asumin \u6700\u521D\u306F\u57FA\u672C\u6A5F\u80FD\u304B\u3089\u4F7F\u3046\uFF01",
  "id" : 497026432459997186,
  "in_reply_to_status_id" : 497026361341378560,
  "created_at" : "2014-08-06 14:28:30 +0000",
  "in_reply_to_screen_name" : "an_asumin",
  "in_reply_to_user_id_str" : "202087794",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3042\u3059\u307F\u3093",
      "screen_name" : "an_asumin",
      "indices" : [ 0, 10 ],
      "id_str" : "202087794",
      "id" : 202087794
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497025862311505920",
  "geo" : { },
  "id_str" : "497026102171140096",
  "in_reply_to_user_id" : 202087794,
  "text" : "@an_asumin \u6700\u521D\u304B\u3089Blender\u4F7F\u3044\u6163\u308C\u3066\u304A\u3044\u305F\u307B\u3046\u304C\u697D\u3060\u3063\u305F\u308A\u3059\u308B\u304B\u3082\u30FB\u30FB\u30FB",
  "id" : 497026102171140096,
  "in_reply_to_status_id" : 497025862311505920,
  "created_at" : "2014-08-06 14:27:11 +0000",
  "in_reply_to_screen_name" : "an_asumin",
  "in_reply_to_user_id_str" : "202087794",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497012169632780289",
  "geo" : { },
  "id_str" : "497012653689036800",
  "in_reply_to_user_id" : 445958218,
  "text" : "@hiochuu \u305D\u3046\u3067\u3059\u305D\u3046\u3067\u3059\uFF01\u5408\u3063\u3066\u307E\u3059\uFF01",
  "id" : 497012653689036800,
  "in_reply_to_status_id" : 497012169632780289,
  "created_at" : "2014-08-06 13:33:45 +0000",
  "in_reply_to_screen_name" : "hio87bi",
  "in_reply_to_user_id_str" : "445958218",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497008684526882817",
  "geo" : { },
  "id_str" : "497009163315068928",
  "in_reply_to_user_id" : 445958218,
  "text" : "@hiochuu \u5BDD\u3066\u305F\u304B\u3089\u75B2\u308C\u3066\u306A\u3044\u3093\u3067\u3059\u3051\u3069\u306D\uFF01\u305D\u3046\u3044\u3048\u3070\u7D50\u5C40\u3001\u4F55\u65E5\u306B\u306A\u3063\u305F\u3093\u3067\u3057\u305F\u3063\u3051\u30FB\u30FB\u30FB\uFF1F",
  "id" : 497009163315068928,
  "in_reply_to_status_id" : 497008684526882817,
  "created_at" : "2014-08-06 13:19:53 +0000",
  "in_reply_to_screen_name" : "hio87bi",
  "in_reply_to_user_id_str" : "445958218",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497008488795475969",
  "text" : "\u604B\u4EBA\u3055\u3093\u7528\u306B\u673A\u3068\u6905\u5B50\u304C\u8FFD\u52A0\u3055\u308C\u305F\u306E\u3067\u3001\u6B21\u306F\u3061\u3083\u3063\u3061\u3083\u3068\u3082\u3046\u5C11\u3057\u9577\u3044LAN\u30B1\u30FC\u30D6\u30EB\u8CB7\u3063\u3066\u3001\u3042\u3068\u306F\u3082\u3046\u5C11\u3057\u3044\u3044\u30B0\u30E9\u30DC\u304C\u307B\u3057\u3044\u3068\u3053\u308D\u306D\u3002",
  "id" : 497008488795475969,
  "created_at" : "2014-08-06 13:17:12 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497007931854831616",
  "text" : "\u3061\u3087\u3063\u3068\u5143\u6C17\u306B\u306A\u3063\u305F\u304B\u3089\u30B2\u30FC\u30E0\u3057\u3088\u3046",
  "id" : 497007931854831616,
  "created_at" : "2014-08-06 13:14:59 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u767D",
      "screen_name" : "parodyer",
      "indices" : [ 0, 9 ],
      "id_str" : "82635103",
      "id" : 82635103
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497006328942841856",
  "geo" : { },
  "id_str" : "497006405656649728",
  "in_reply_to_user_id" : 82635103,
  "text" : "@parodyer \u307E\u3041\u81EA\u5206\u3067\u30B2\u30FC\u30E0\u3059\u308B\u3088\u308A\u306F\u6642\u9593\u3068\u3089\u308C\u306A\u3044\u306E\u3067\u306F\uFF57",
  "id" : 497006405656649728,
  "in_reply_to_status_id" : 497006328942841856,
  "created_at" : "2014-08-06 13:08:55 +0000",
  "in_reply_to_screen_name" : "parodyer",
  "in_reply_to_user_id_str" : "82635103",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u767D",
      "screen_name" : "parodyer",
      "indices" : [ 0, 9 ],
      "id_str" : "82635103",
      "id" : 82635103
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "497003635977363457",
  "geo" : { },
  "id_str" : "497006214484459520",
  "in_reply_to_user_id" : 82635103,
  "text" : "@parodyer \u305D\u3046\u3044\u3046\u5C64\u304C\u4ECA\u306F\u5B9F\u6CC1\u52D5\u753B\u3092\u898B\u3066\u308B\u307F\u305F\u3044\u3067\u3059\u306D",
  "id" : 497006214484459520,
  "in_reply_to_status_id" : 497003635977363457,
  "created_at" : "2014-08-06 13:08:10 +0000",
  "in_reply_to_screen_name" : "parodyer",
  "in_reply_to_user_id_str" : "82635103",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "497005294556151809",
  "text" : "\u3068\u308A\u3042\u3048\u305A\u30A2\u30A4\u30B9\u3060\u3051\u98DF\u3079\u305F",
  "id" : 497005294556151809,
  "created_at" : "2014-08-06 13:04:30 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "496998371555434498",
  "text" : "\u4F53\u8ABF\u306F\u5272\u3068\u623B\u3063\u305F\u304B\u306A\u30FB\u30FB\u30FB\uFF1F",
  "id" : 496998371555434498,
  "created_at" : "2014-08-06 12:37:00 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "496997217702404097",
  "text" : "\u8D77\u304D\u305F\u3001\u6D17\u6FEF\u307E\u308F\u3057\u3063\u3071\u306A\u3057\u3060\u3063\u305F\u3002",
  "id" : 496997217702404097,
  "created_at" : "2014-08-06 12:32:25 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "uiu",
      "screen_name" : "uiureo",
      "indices" : [ 0, 7 ],
      "id_str" : "13812652",
      "id" : 13812652
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "496905716079144960",
  "geo" : { },
  "id_str" : "496905796488138752",
  "in_reply_to_user_id" : 13812652,
  "text" : "@uiureo \u3044\u3084\u3001\u672A\u6210\u5E74\u306A\u3093\u3066\u6240\u8A6E\u306F\u7DDA\u5F15\u304D\u3067\u3057\u304B\u306A\u3044\u3067\u3059\u306D\u3063\u3066",
  "id" : 496905796488138752,
  "in_reply_to_status_id" : 496905716079144960,
  "created_at" : "2014-08-06 06:29:08 +0000",
  "in_reply_to_screen_name" : "uiureo",
  "in_reply_to_user_id_str" : "13812652",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "uiu",
      "screen_name" : "uiureo",
      "indices" : [ 0, 7 ],
      "id_str" : "13812652",
      "id" : 13812652
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "496905172413472768",
  "geo" : { },
  "id_str" : "496905571832840194",
  "in_reply_to_user_id" : 13812652,
  "text" : "@uiureo 20\u304B\u3089\u3059\u308B\u306819\u3068\u304B\u666E\u901A\u3067\u306F",
  "id" : 496905571832840194,
  "in_reply_to_status_id" : 496905172413472768,
  "created_at" : "2014-08-06 06:28:15 +0000",
  "in_reply_to_screen_name" : "uiureo",
  "in_reply_to_user_id_str" : "13812652",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "496866203638321153",
  "geo" : { },
  "id_str" : "496866306893684736",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u3046\u3054\u304D\u305F\u304F\u306A\u3044",
  "id" : 496866306893684736,
  "in_reply_to_status_id" : 496866203638321153,
  "created_at" : "2014-08-06 03:52:13 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "496865397723762691",
  "geo" : { },
  "id_str" : "496866054698573824",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u3093\u30FC\u30FB\u30FB\u30FB\u3044\u3044\u3084",
  "id" : 496866054698573824,
  "in_reply_to_status_id" : 496865397723762691,
  "created_at" : "2014-08-06 03:51:13 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "496865004721688578",
  "geo" : { },
  "id_str" : "496865230765297664",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 \u308F\u304B\u3089\u3093\u30FB\u30FB\u30FB\u4F53\u304C\u3048\u3089\u3044",
  "id" : 496865230765297664,
  "in_reply_to_status_id" : 496865004721688578,
  "created_at" : "2014-08-06 03:47:56 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "496864853202440192",
  "text" : "\u4E45\u3057\u3076\u308A\u306B\u4F53\u8ABF\u308F\u308B",
  "id" : 496864853202440192,
  "created_at" : "2014-08-06 03:46:26 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u4FDD\u5065\u5BA4\u3067\u4F1A\u3063\u305F\u3055\u304F\u3089\u3093\u307C\u2740\u273F",
      "screen_name" : "xxsakxuraxx",
      "indices" : [ 0, 12 ],
      "id_str" : "120724458",
      "id" : 120724458
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "496666290635808769",
  "geo" : { },
  "id_str" : "496666717259436032",
  "in_reply_to_user_id" : 120724458,
  "text" : "@xxsakxuraxx \u96C6\u5408\u4F4F\u5B85\u306F\u3057\u304B\u305F\u306A\u3044\u3067\u3059\u3088\u306D\u3047",
  "id" : 496666717259436032,
  "in_reply_to_status_id" : 496666290635808769,
  "created_at" : "2014-08-05 14:39:07 +0000",
  "in_reply_to_screen_name" : "xxsakxuraxx",
  "in_reply_to_user_id_str" : "120724458",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 84, 106 ],
      "url" : "http:\/\/t.co\/9rAoCSLMen",
      "expanded_url" : "http:\/\/codezine.jp\/article\/detail\/6464",
      "display_url" : "codezine.jp\/article\/detail\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "496592176864714753",
  "text" : "Visual Studio 11 : C++ Unit Test Framework\u3000\u2500\u2500 C++\u5358\u4F53\u30C6\u30B9\u30C8\u306E\u6C7A\u5B9A\u7248\uFF08\u304B\u3082\u3057\u308C\u306A\u3044\uFF09 \uFF081\/4\uFF09\uFF1ACodeZine - http:\/\/t.co\/9rAoCSLMen \u3053\u308C\u304B",
  "id" : 496592176864714753,
  "created_at" : "2014-08-05 09:42:55 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "496590443165921283",
  "text" : "VisualStudio\u74B0\u5883\u3067\u6C17\u8EFD\u306B\u3067\u304D\u308BC++\u306EUnitTest\u74B0\u5883\u306F\u306A\u3044\u3082\u306E\u304B",
  "id" : 496590443165921283,
  "created_at" : "2014-08-05 09:36:02 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "496455462603063296",
  "text" : "\u306A\u3093\u304B\u8B66\u544A\u97F3\u805E\u3053\u3048\u308B\u3093\u3060\u304C\u5927\u4E08\u592B\u304B\u3053\u308C",
  "id" : 496455462603063296,
  "created_at" : "2014-08-05 00:39:40 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DuffyCAE",
      "screen_name" : "DuffyCAE",
      "indices" : [ 0, 9 ],
      "id_str" : "2583377618",
      "id" : 2583377618
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "496116016254955520",
  "geo" : { },
  "id_str" : "496126810879307776",
  "in_reply_to_user_id" : 2583377618,
  "text" : "@DuffyCAE \u306F\u3044\u3001\u3060\u304B\u3089\u306A\u304A\u306E\u3053\u3068\u306A\u3093\u3067\u6BD4\u8F03\u3068\u3044\u3046\u304B\u7D39\u4ECB\u306B\u3059\u3089\u306A\u3044\u306E\u304B\u3063\u3066\u306E\u304C\u4E0D\u601D\u8B70\u3067\u3059\u3002\u3042\u3068\u3001Intr.Jour.\u306B\u307B\u3068\u3093\u3069SMAC-SPH\u304C\u898B\u5F53\u305F\u3089\u306A\u3044\u3068\u3053\u3068\u304B\u3002",
  "id" : 496126810879307776,
  "in_reply_to_status_id" : 496116016254955520,
  "created_at" : "2014-08-04 02:53:43 +0000",
  "in_reply_to_screen_name" : "DuffyCAE",
  "in_reply_to_user_id_str" : "2583377618",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "496115668844941312",
  "text" : "\u3059\u3063\u3054\u3044\u61D0\u304B\u3057\u3044\u4EBA\u305F\u3061\u306B\u4F1A\u3046\u5922\u3060\u3063\u305F\u3002\u4E2D\u5B66\u6821\u306E\u6642\u306E\u89AA\u53CB\u3068\u304B\u3002",
  "id" : 496115668844941312,
  "created_at" : "2014-08-04 02:09:27 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u3084\u304D\u3068\u308A",
      "screen_name" : "torigomoku7",
      "indices" : [ 0, 12 ],
      "id_str" : "709119536",
      "id" : 709119536
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495959453124943874",
  "geo" : { },
  "id_str" : "495987906775756800",
  "in_reply_to_user_id" : 133385291,
  "text" : "@torigomoku7 you are my angel falling hopeless for me",
  "id" : 495987906775756800,
  "in_reply_to_status_id" : 495959453124943874,
  "created_at" : "2014-08-03 17:41:46 +0000",
  "in_reply_to_screen_name" : "p1yop1yo7",
  "in_reply_to_user_id_str" : "133385291",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495984097949270016",
  "text" : "\u30AF\u30FC\u30E9\u30FC\u306E\u5411\u304D\u5909\u3048\u305F\u3089\u3059\u3054\u3044\u6DBC\u3057\u3044",
  "id" : 495984097949270016,
  "created_at" : "2014-08-03 17:26:38 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495808013412745216",
  "text" : "\u30B3\u30D4\u30FC\u30B3\u30F3\u30B9\u30C8\u30E9\u30AF\u30BF\u3092\u8D70\u3089\u305B\u305F\u304F\u306A\u3044\u304B\u3089\u30DD\u30A4\u30F3\u30BF\u306E\u914D\u5217\u306B\u3057\u3066\u305F\u3051\u3069\u305D\u308C\u306F\u305D\u308C\u3067\u3064\u3089\u3044\u304B\u3089\u3001\u53D6\u3063\u3066\u304F\u308B\u5074\u3067\u3061\u3083\u3093\u3068\u53C2\u7167\u306B\u3059\u308B\u304B\u3002",
  "id" : 495808013412745216,
  "created_at" : "2014-08-03 05:46:56 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495805483349843968",
  "text" : "func(int val) vs func(const int&amp; func)\n\u307F\u306A\u3055\u3093\u306F\u3069\u3063\u3061\u3092\u4F7F\u3044\u307E\u3059\uFF1F",
  "id" : 495805483349843968,
  "created_at" : "2014-08-03 05:36:53 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495784040297795584",
  "text" : "constexpr\u306F\u3044\u3064\u306B\u306A\u3063\u305F\u3089VisualStudio\u3067\u4F7F\u3048\u308B\u3088\u3046\u306B\u306A\u308A\u307E\u3059\u304B\uFF1F",
  "id" : 495784040297795584,
  "created_at" : "2014-08-03 04:11:41 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495775071240081408",
  "text" : "boost.MPI\u304C\u58CA\u308C\u3066\u305F\u30FB\u30FB\u30FB\u3042\u308C\uFF1F",
  "id" : 495775071240081408,
  "created_at" : "2014-08-03 03:36:02 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495760345479118848",
  "text" : "SMAC-SPH\u4F5C\u3063\u305F\u4EBA\u306F\u3001\u306A\u3093\u3067ISPH\u3092\u898B\u306A\u304B\u3063\u305F\u3093\u3060\u308D\u3046\u306A\u3041\u3068\u3044\u3046\u4E0D\u601D\u8B70\u3057\u304B\u306A\u3044\u3002\u898B\u843D\u3068\u3057\u3066\u305F\u3068\u306F\u601D\u3048\u306A\u3044\u3051\u3069\u3002",
  "id" : 495760345479118848,
  "created_at" : "2014-08-03 02:37:31 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DuffyCAE",
      "screen_name" : "DuffyCAE",
      "indices" : [ 0, 9 ],
      "id_str" : "2583377618",
      "id" : 2583377618
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495709548439564288",
  "geo" : { },
  "id_str" : "495758970565304320",
  "in_reply_to_user_id" : 2583377618,
  "text" : "@DuffyCAE SMAC-SPH\u3067\u3059\u304B\u30FB\u30FB\u30FB\u30FB\u30FB",
  "id" : 495758970565304320,
  "in_reply_to_status_id" : 495709548439564288,
  "created_at" : "2014-08-03 02:32:04 +0000",
  "in_reply_to_screen_name" : "DuffyCAE",
  "in_reply_to_user_id_str" : "2583377618",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/tweetlogix.com\" rel=\"nofollow\"\u003ETweetlogix\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DuffyCAE",
      "screen_name" : "DuffyCAE",
      "indices" : [ 0, 9 ],
      "id_str" : "2583377618",
      "id" : 2583377618
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495489436516245505",
  "geo" : { },
  "id_str" : "495623678923923456",
  "in_reply_to_user_id" : 2583377618,
  "text" : "@DuffyCAE \u305D\u3046\u3067\u3059\u306D\u3002\u3068\u3044\u3046\u304B\u3001\u305D\u308C\u3092\u8AAD\u307E\u305A\u306B\u3069\u308C\u3092\u898B\u3066ISPH\u3092\u5B9F\u88C5\u3057\u3088\u3046\u3068\u3055\u308C\u3066\u305F\u3093\u3067\u3059\u304B\u30FB\u30FB\u30FB\uFF1F",
  "id" : 495623678923923456,
  "in_reply_to_status_id" : 495489436516245505,
  "created_at" : "2014-08-02 17:34:27 +0000",
  "in_reply_to_screen_name" : "DuffyCAE",
  "in_reply_to_user_id_str" : "2583377618",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495483598107971585",
  "text" : "MPS\u6CD5\u304C\u7406\u89E3\u3067\u304D\u305F\u3089SPH\u3082\u7406\u89E3\u3067\u304D\u308B\u3063\u3066\u8A00\u3063\u3066\u308B\u4EBA\u3001\u6709\u9650\u8981\u7D20\u6CD5\u3092\u7406\u89E3\u3067\u304D\u305F\u3089\u6709\u9650\u4F53\u7A4D\u6CD5\u3082\u7406\u89E3\u3067\u304D\u305D\u3046\u3060\u3057\u3001\u3059\u3054\u3044\u5929\u624D\u305D\u3046\u3002",
  "id" : 495483598107971585,
  "created_at" : "2014-08-02 08:17:50 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\"\u003ETwitter for Android\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DuffyCAE",
      "screen_name" : "DuffyCAE",
      "indices" : [ 0, 9 ],
      "id_str" : "2583377618",
      "id" : 2583377618
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495468144912134145",
  "geo" : { },
  "id_str" : "495482994098855937",
  "in_reply_to_user_id" : 2583377618,
  "text" : "@DuffyCAE PPE=Pressure Poisson Equation\u3067\u3059\uFF08\u203B\u3069\u3063\u3061\u304C\u3069\u3063\u3061\u306EP\u304B\u306F\u8868\u8A18\u63FA\u308C\u304C\u3042\u308B\u3088\u3046\uFF09\u3002\u3042\u308C\u306B\u66F8\u3044\u3066\u3042\u308B\u306E\u306FMPS\uFF08\u5BC6\u5EA6\u306E\u7269\u8CEA\u5FAE\u5206\u3092\u4F7F\u3046\u65B9\u5F0F\uFF09\u306B\u7279\u5316\u3057\u3066\u308B\u306E\u3067ISPH\u306F\u9055\u3044\u307E\u3059\u3088\uFF1F\u8A73\u3057\u304F\u306FShao\u5148\u751F\u305F\u3061\u306E\u8AD6\u6587\u8AAD\u3081\u3070\u308F\u304B\u308B\u3068\u601D\u3044\u307E\u3059\u3051\u3069\u3002",
  "id" : 495482994098855937,
  "in_reply_to_status_id" : 495468144912134145,
  "created_at" : "2014-08-02 08:15:26 +0000",
  "in_reply_to_screen_name" : "DuffyCAE",
  "in_reply_to_user_id_str" : "2583377618",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495477099147177985",
  "geo" : { },
  "id_str" : "495478103641038848",
  "in_reply_to_user_id" : 445958218,
  "text" : "@hiochuu \u6765\u6708\u308A\u3087\u30FC\u304B\u3044\u3067\u3059\uFF011\u5468\u76EE\u306E\u571F\u65E5\u306F\u5E30\u7701\u4E88\u5B9A\u306A\u306E\u3067\u96E3\u3057\u3044\u3067\u3059\u3051\u3069\u3001\u305D\u306E\u6B21\u306A\u3089\uFF01",
  "id" : 495478103641038848,
  "in_reply_to_status_id" : 495477099147177985,
  "created_at" : "2014-08-02 07:56:00 +0000",
  "in_reply_to_screen_name" : "hio87bi",
  "in_reply_to_user_id_str" : "445958218",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495477614690050048",
  "text" : "\u3067\u3082\u7DCF\u5408\u7684\u306B\u3075\u3049\u30FC\u308C\u3093\u30B9\u30DA\u306F\u697D\u3057\u3044\u66F2\u3067\u3057\u305F\u3002",
  "id" : 495477614690050048,
  "created_at" : "2014-08-02 07:54:03 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495477387673362432",
  "text" : "\u3075\u3049\u30FC\u308C\u3093\u30B9\u30DA\u304C\u82E5\u5E72\u76EE\u3067\u8FFD\u3048\u3066\u306A\u304B\u3063\u305F\u3068\u3053\u308D\u3042\u3063\u305F\u304B\u3089\u3001\u30EA\u30CF\u30D3\u30EA\u305B\u306D\u3070\u3002",
  "id" : 495477387673362432,
  "created_at" : "2014-08-02 07:53:09 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495476795051765762",
  "text" : "\u9752\u9F8D\u3055\u3093\u3068\u76F8\u6027\u826F\u304F\u3066\u3001S-C-U\u3055\u3093\u3068\u76F8\u6027\u60AA\u3044\u30EA\u30D5\u30EC\u30AF\u7247\u624Ber\u3068\u306F\u79C1\u306E\u3053\u3068\u3067\u3059\u3002",
  "id" : 495476795051765762,
  "created_at" : "2014-08-02 07:50:48 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495476230632660992",
  "text" : "\u300C\u3075\u3049\u30FC\u308C\u3093\u3055\u3093Special\u304C\u51FA\u305F\u3051\u3069\u8D64\u30C8\u30EA\u4F59\u88D5\u3084\u3057\u521D\u898B\u30C0\u30D6\u30EB\u3050\u3089\u3044\u3044\u3051\u308B\u3084\u308D\u30FC\uFF57\uFF57\u300D\u3063\u3066\u306A\u3081\u3066\u304B\u304B\u3063\u305F\u7D50\u679C\u3001\u521D\u624B\u3067\u7247\u624B\u6BBA\u3057\uFF086\u540C\u6642\uFF09\u304F\u3089\u3063\u3066\u7206\u767A\u3057\u305F",
  "id" : 495476230632660992,
  "created_at" : "2014-08-02 07:48:33 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ {
      "expanded_url" : "http:\/\/twitter.com\/aokomoriuta\/status\/495470870471516160\/photo\/1",
      "indices" : [ 18, 40 ],
      "url" : "http:\/\/t.co\/sx547iVtFd",
      "media_url" : "http:\/\/pbs.twimg.com\/media\/BuBEIZhCUAARGgM.jpg",
      "id_str" : "495470870089846784",
      "id" : 495470870089846784,
      "media_url_https" : "https:\/\/pbs.twimg.com\/media\/BuBEIZhCUAARGgM.jpg",
      "sizes" : [ {
        "h" : 604,
        "resize" : "fit",
        "w" : 340
      }, {
        "h" : 150,
        "resize" : "crop",
        "w" : 150
      }, {
        "h" : 1920,
        "resize" : "fit",
        "w" : 1080
      }, {
        "h" : 1066,
        "resize" : "fit",
        "w" : 600
      }, {
        "h" : 1820,
        "resize" : "fit",
        "w" : 1024
      } ],
      "display_url" : "pic.twitter.com\/sx547iVtFd"
    } ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495470870471516160",
  "text" : "\u666E\u901A\u306B\u521D\u898B\u30C0\u30D6\u30EB\u3068\u308C\u3066\u3073\u3063\u304F\u308A\u3057\u305F http:\/\/t.co\/sx547iVtFd",
  "id" : 495470870471516160,
  "created_at" : "2014-08-02 07:27:15 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 74, 97 ],
      "url" : "https:\/\/t.co\/sisEEA4A5O",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53dc8ea1498e2c4d27ad3fc5?s=0QrIKGt9HfiSPulQtXTjw5uHfxI&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "495466365349003264",
  "text" : "\u306A\u308B\u307B\u3069\u3053\u3053\u304C\u4F8B\u306E\u30C0\u30F3\u30A8\u30DC\u6652\u3057\u53F0\u304B (@ \u30E9\u30A6\u30F3\u30C9\u30EF\u30F3\u30B9\u30BF\u30B8\u30A2\u30E0 \u30C0\u30A4\u30D0\u30FC\u30B7\u30C6\u30A3\u6771\u4EAC \u30D7\u30E9\u30B6\u5E97 in Koto, \u6771\u4EAC\u90FD w\/ 3 others) https:\/\/t.co\/sisEEA4A5O",
  "id" : 495466365349003264,
  "created_at" : "2014-08-02 07:09:21 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DuffyCAE",
      "screen_name" : "DuffyCAE",
      "indices" : [ 0, 9 ],
      "id_str" : "2583377618",
      "id" : 2583377618
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495461660060942336",
  "geo" : { },
  "id_str" : "495465499359068162",
  "in_reply_to_user_id" : 2583377618,
  "text" : "@DuffyCAE ISPH\u4F7F\u3046\u3093\u3067\u3059\u306D\u3001\u305D\u308C\u306A\u3089PPE\u4F5C\u308B\u3068\u3053\u308D\u306F\u540C\u3058\u3067\u3059\u304C\u3001\u539F\u7406\u306F\u305D\u3082\u305D\u3082MAC\u6CD5\u306B\u3042\u308B\u306E\u3067\u3001\u8A73\u3057\u304F\u306F\u305D\u3063\u3061\u3092\u898B\u308B\u307B\u3046\u304C\u3044\u3044\u3067\u3059\u3002",
  "id" : 495465499359068162,
  "in_reply_to_status_id" : 495461660060942336,
  "created_at" : "2014-08-02 07:05:55 +0000",
  "in_reply_to_screen_name" : "DuffyCAE",
  "in_reply_to_user_id_str" : "2583377618",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DuffyCAE",
      "screen_name" : "DuffyCAE",
      "indices" : [ 0, 9 ],
      "id_str" : "2583377618",
      "id" : 2583377618
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495449843620274176",
  "geo" : { },
  "id_str" : "495451444326051841",
  "in_reply_to_user_id" : 2583377618,
  "text" : "@DuffyCAE \u3048\u3048\u3068\u3001\u8ABF\u3079\u305F\u3089\u5206\u304B\u308B\u3053\u3068\u3068\u306F\u601D\u3044\u307E\u3059\u304C\u3001\u305D\u3082\u305D\u3082\u30AB\u30FC\u30CD\u30EB\u95A2\u6570\u3092\u5FAE\u5206\u3059\u308B\u3068\u3044\u3046\u8003\u3048\u306ESPH\u3068\u5FAE\u5206\u6F14\u7B97\u5B50\u30E2\u30C7\u30EB\u306EMPS\u3067\u306F\u5168\u304F\u8003\u3048\u65B9\u306F\u9055\u3044\u307E\u3059\u3057\u3001\u305D\u3082\u305D\u3082\u57FA\u672CSPH\u306F\u967D\u89E3\u6CD5\u306A\u3093\u3067\u3059\u3088\u30FC\uFF1F",
  "id" : 495451444326051841,
  "in_reply_to_status_id" : 495449843620274176,
  "created_at" : "2014-08-02 06:10:04 +0000",
  "in_reply_to_screen_name" : "DuffyCAE",
  "in_reply_to_user_id_str" : "2583377618",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 40, 63 ],
      "url" : "https:\/\/t.co\/3RYwNr3Xky",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53dc7f17498e463a12b2ca64?s=E3LvsmeNeYBdCLaX0eHNgqJpA0I&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "495449682165121024",
  "text" : "\u98DF\u5668\u68DA\u3068\u93E1\u304B\u3063\u305F (@ \u30CB\u30C8\u30EA \u30F4\u30A3\u30FC\u30CA\u30B9\u30D5\u30A9\u30FC\u30C8\u5E97 in \u6C5F\u6771\u533A, \u6771\u4EAC\u90FD) https:\/\/t.co\/3RYwNr3Xky",
  "id" : 495449682165121024,
  "created_at" : "2014-08-02 06:03:03 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 55, 78 ],
      "url" : "https:\/\/t.co\/mIfJDOkUe5",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53dc6fba498eaee222c41b3e?s=qtsVLlKLXKYYlpLircMweY4GRgw&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "495433184755322882",
  "text" : "\u601D\u3063\u305F\u3088\u308A\u3067\u304B\u304F\u3066\u3073\u3063\u304F\u308A\u3057\u305F (@ VenusFort (\u30F4\u30A3\u30FC\u30CA\u30B9\u30D5\u30A9\u30FC\u30C8) in Koto, \u6771\u4EAC\u90FD) https:\/\/t.co\/mIfJDOkUe5",
  "id" : 495433184755322882,
  "created_at" : "2014-08-02 04:57:30 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u30CF\u30EB",
      "screen_name" : "_haru_",
      "indices" : [ 59, 66 ],
      "id_str" : "15616888",
      "id" : 15616888
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 68, 91 ],
      "url" : "https:\/\/t.co\/mzCcl4e9OY",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53dc6bc9498e47a5bf77b2df?s=k93v-91ckNtrcvLkMWgTW0rU5Jo&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "495428957144383488",
  "text" : "\u635C\u67FB\u7DDA\u304C\u8E0A\u3063\u3066\u305F (@ \u6771\u4EAC\u30C6\u30EC\u30DD\u30FC\u30C8\u99C5 (Tokyo Teleport Sta.) in Koto, \u6771\u4EAC\u90FD w\/ @_haru_) https:\/\/t.co\/mzCcl4e9OY",
  "id" : 495428957144383488,
  "created_at" : "2014-08-02 04:40:42 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495427143615000576",
  "text" : "\u308A\u3093\u304B\u3044\u7DDA\u306E\u4E2D\u3067\u3089\u6DBC\u3057\u3044",
  "id" : 495427143615000576,
  "created_at" : "2014-08-02 04:33:30 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495425357273505793",
  "text" : "\u3067\u3082\u3059\u3054\u3044\u9EBA\u304C\u592A\u304F\u3066\u3046\u3069\u3093\u307F\u305F\u3044\u3060\u3063\u305F",
  "id" : 495425357273505793,
  "created_at" : "2014-08-02 04:26:24 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495424118901051394",
  "text" : "\u305F\u307E\u305F\u307E\u898B\u304B\u3051\u305F\u3089\u7A7A\u3044\u3066\u305F\u304B\u3089\u3075\u3089\u3063\u3068\u5165\u3063\u305F\u3089\u524D\u306B\u884C\u5217\u3067\u304D\u3059\u304E\u3066\u8FD1\u6240\u8FF7\u60D1\u306B\u306A\u3063\u3066\u9589\u5E97\u3057\u305F\u307B\u3069\u6709\u540D\u306A\u3064\u3051\u9EBA\u5C4B\u3055\u3093\u3060\u3063\u305F\u3089\u3057\u3044\u3002",
  "id" : 495424118901051394,
  "created_at" : "2014-08-02 04:21:29 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"http:\/\/foursquare.com\" rel=\"nofollow\"\u003EFoursquare\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ {
      "indices" : [ 27, 50 ],
      "url" : "https:\/\/t.co\/YMVech02yX",
      "expanded_url" : "https:\/\/www.swarmapp.com\/aokomoriuta\/checkin\/53dc6215498eaee222c38e6f?s=1VlfV-e9pQ7ofXiXqr9y6SGIt3Y&ref=tw",
      "display_url" : "swarmapp.com\/aokomoriuta\/ch\u2026"
    } ]
  },
  "geo" : { },
  "id_str" : "495418535116800000",
  "text" : "I'm at \u516D\u5398\u820E \u5927\u5D0E\u5E97 in \u54C1\u5DDD\u533A, \u6771\u4EAC\u90FD https:\/\/t.co\/YMVech02yX",
  "id" : 495418535116800000,
  "created_at" : "2014-08-02 03:59:17 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495410773196152832",
  "geo" : { },
  "id_str" : "495412815226621954",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u6CE3\u3044\u3066\u308B\u30FB\u30FB\u30FB",
  "id" : 495412815226621954,
  "in_reply_to_status_id" : 495410773196152832,
  "created_at" : "2014-08-02 03:36:34 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495409609461350402",
  "geo" : { },
  "id_str" : "495410088098541568",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u308F\u3089\u3048\u3070\u3044\u3044\u3068\u304A\u3082\u3046\u3088",
  "id" : 495410088098541568,
  "in_reply_to_status_id" : 495409609461350402,
  "created_at" : "2014-08-02 03:25:43 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495408949365972993",
  "geo" : { },
  "id_str" : "495409105972908032",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u3046\u305D\u3060\u3063\uFF01\uFF01",
  "id" : 495409105972908032,
  "in_reply_to_status_id" : 495408949365972993,
  "created_at" : "2014-08-02 03:21:49 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "\u304A\u30FC\u304B\u4F9D\u5B58\u75C7",
      "screen_name" : "rofi",
      "indices" : [ 0, 5 ],
      "id_str" : "14889894",
      "id" : 14889894
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495407817042317312",
  "geo" : { },
  "id_str" : "495408080230707200",
  "in_reply_to_user_id" : 14889894,
  "text" : "@rofi \u7F8E\u5BB9\u9662\u3058\u3083\u306A\u304F\u3066\u30FB\u30FB\u30FB\uFF1F",
  "id" : 495408080230707200,
  "in_reply_to_status_id" : 495407817042317312,
  "created_at" : "2014-08-02 03:17:45 +0000",
  "in_reply_to_screen_name" : "rofi",
  "in_reply_to_user_id_str" : "14889894",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ {
      "name" : "DuffyCAE",
      "screen_name" : "DuffyCAE",
      "indices" : [ 0, 9 ],
      "id_str" : "2583377618",
      "id" : 2583377618
    } ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "in_reply_to_status_id_str" : "495385978828095488",
  "geo" : { },
  "id_str" : "495406874120814592",
  "in_reply_to_user_id" : 2583377618,
  "text" : "@DuffyCAE SPH\u306E\u3069\u306E\u8FBA\u306B\u5F79\u7ACB\u3064\u3093\u3067\u3059\u304B\u30FB\u30FB\u30FB\uFF1F",
  "id" : 495406874120814592,
  "in_reply_to_status_id" : 495385978828095488,
  "created_at" : "2014-08-02 03:12:57 +0000",
  "in_reply_to_screen_name" : "DuffyCAE",
  "in_reply_to_user_id_str" : "2583377618",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495406606473887745",
  "text" : "\u5148\u6708\u306E7\u6708\u306Etweet\u6570\u3001400[tpm]\u3068\u304B\u3067\u3001Twilog\u306B\u8A18\u9332\u304C\u6B8B\u3063\u3066\u308B\u9650\u308A\u30012009\u5E749\u6708\u3068\u3044\u3046\u5927\u6614\u4EE5\u6765\u306E\u53F2\u4E0A2\u756A\u3081\u306E\u4F4E\u3055\u3092\u8A18\u9332\u3057\u3066\u3044\u305F",
  "id" : 495406606473887745,
  "created_at" : "2014-08-02 03:11:53 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/sourceforge.jp\/projects\/opentween\/\" rel=\"nofollow\"\u003EOpenTween\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495240475909947392",
  "text" : "\u3042\u3063\u3064\u3044\u306D\u30FC",
  "id" : 495240475909947392,
  "created_at" : "2014-08-01 16:11:45 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
}, {
  "source" : "\u003Ca href=\"https:\/\/play.google.com\/store\/apps\/details?id=org.mariotaku.twidere\" rel=\"nofollow\"\u003ETwidere for Android #2\u003C\/a\u003E",
  "entities" : {
    "user_mentions" : [ ],
    "media" : [ ],
    "hashtags" : [ ],
    "urls" : [ ]
  },
  "geo" : { },
  "id_str" : "495205760775626752",
  "text" : "\u6708\u304C\u5909\u308F\u3063\u305F\u9014\u7AEF\u306B\u9045\u304F\u307E\u3067\u6B8B\u308B\u3084\u3064\u306D\u3001\u77E5\u3063\u3066\u308B",
  "id" : 495205760775626752,
  "created_at" : "2014-08-01 13:53:48 +0000",
  "user" : {
    "name" : "\u9752\u5B50\u5B88\u6B4C",
    "screen_name" : "aokomoriuta",
    "protected" : false,
    "id_str" : "10902082",
    "profile_image_url_https" : "https:\/\/pbs.twimg.com\/profile_images\/617634272933851136\/JSsWKFTz_normal.png",
    "id" : 10902082,
    "verified" : false
  }
} ]